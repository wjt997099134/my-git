package com.cskaoyan.controller.spread;

import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.bean.vo.spread.WxCouponData;
import com.cskaoyan.bean.vo.spread.WxCouponSelectData;
import com.cskaoyan.bean.vo.spread.WxMyCouponData;
import com.cskaoyan.service.WxCouponService;
import com.cskaoyan.util.ConstantProperties;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * 小程序优惠券管理
 * @author CR
 * @since 2022/06/07 14:16
 */
@RestController
@RequestMapping("wx/coupon")
public class WxCouponController {
    @Autowired
    WxCouponService couponService;
    @Autowired
    ConstantProperties CONSTANT;

    /**
     *  首页点击优惠券标题，获取所有优惠券信息列表
     */
    @GetMapping("list")
    public BaseRespVo list(Integer page,Integer limit) {
        WxCouponData couponData = couponService.queryAllCoupons(page,limit);
        return BaseRespVo.ok(couponData);
    }

    /**
     *  点击某优惠券，领取该优惠券到登录账户
     */
    @PostMapping("receive")
    public BaseResp receive(@RequestBody Map map) {
        Integer couponId = (Integer) map.get("couponId");
        User user = (User) SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
        int code = couponService.receive(couponId,user.getId());
        if (code == CONSTANT.getFailCode4()) {
            return BaseResp.invalidRequest("优惠券已经领取过");
        }
        return BaseResp.ok();
    }

    /**
     *  个人中心点击优惠券，获取当前登录用户的优惠券列表
     */
    @GetMapping("mylist")
    public BaseRespVo mylist(Integer status,Integer page,Integer limit) {
        User user = (User) SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
        // User user1 = (User) SecurityUtils.getSubject().getPrincipal();
        WxMyCouponData myCouponData = couponService.queryMyCoupons(status,page,limit,user.getId());
        return BaseRespVo.ok(myCouponData);
    }

    /**
     *  我的优惠券列表输入优惠码，兑换优惠券
     */
    @PostMapping("exchange")
    public BaseResp exchange(@RequestBody Map map) {
        String couponCode = (String) map.get("code");
        User user = (User) SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
        int code = couponService.exchange(couponCode,user.getId());
        if (code == CONSTANT.getFailCode4()) {
            return BaseResp.invalidRequest("优惠券已兑换");
        }
        if (code == CONSTANT.getFailCode5()) {
            return BaseResp.invalidRequest1("优惠券不正确");
        }
        return BaseResp.ok();
    }

    /**
     *  下单页面选择可以使用的优惠券
     */
    @GetMapping("selectlist")
    public BaseRespVo selectlist(Integer cartId,Integer grouponRulesId) {
        User user = (User) SecurityUtils.getSubject().getPrincipals().getPrimaryPrincipal();
        WxCouponSelectData couponSelectData = couponService.queryCouponsOfCart(cartId,grouponRulesId,user.getId());
        return BaseRespVo.ok(couponSelectData);
    }
}
