package com.cskaoyan.controller.spread;

import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.spread.WxBrandData;
import com.cskaoyan.mapper.BrandMapper;
import com.cskaoyan.service.WxBrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 小程序品牌管理
 * @author CR
 * @since 2022/06/07 10:50
 */
@RestController
@RequestMapping("wx/brand")
public class WxBrandController {
    @Autowired
    WxBrandService brandService;

    /**
     *  首页点击品牌制造商直供标题，获取品牌信息列表
     */
    @GetMapping("list")
    public BaseRespVo list(Integer page,Integer limit) {
        WxBrandData brandData = brandService.queryBrands(page,limit);
        return BaseRespVo.ok(brandData);
    }

    /**
     *  点击品牌制造商图片，获取品牌制造商详情
     *  此页面会调用 wx/goods/list 接口
     */
    @GetMapping("detail")
    public BaseRespVo detail(Integer id) {
        Brand brand = brandService.queryBrandById(id);
        return BaseRespVo.ok(brand);
    }
}
