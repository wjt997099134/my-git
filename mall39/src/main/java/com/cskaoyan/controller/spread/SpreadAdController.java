package com.cskaoyan.controller.spread;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.spread.AdCreateBo;
import com.cskaoyan.bean.bo.spread.AdDeleteBo;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.spread.AdCreate;
import com.cskaoyan.bean.vo.spread.AdData;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.service.SpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 广告管理
 * @author CR
 * @since 2022/06/04 21:40
 */
@RestController
@RequestMapping("admin/ad")
public class SpreadAdController {
    @Autowired
    SpreadService spreadService;

    /**
     *  1.获取广告列表（只显示deleted标志位为0的信息）；
     *  2.根据标题、内容查询广告信息
     */
    @GetMapping("list")
    public BaseRespVo list(BaseParam param, String name, String content){
        AdData adData = spreadService.queryAd(param,name,content);
        return BaseRespVo.ok(adData);
    }

    /**
     *  添加广告信息，会调用图片上传接口
     */
    @PostMapping("create")
    public BaseRespVo create(@RequestBody AdCreateBo adCreateBo) {
        AdCreate adCreate = spreadService.createAd(adCreateBo);
        return BaseRespVo.ok(adCreate);
    }

    /**
     *  删除广告信息，使用逻辑删除，删除后修改数据库deleted标志位为1
     */
    @PostMapping("delete")
    public BaseResp delete(@RequestBody AdDeleteBo adDeleteBo) {
        spreadService.deleteAd(adDeleteBo.getId());
        return BaseResp.ok();
    }

    /**
     *  编辑修改广告信息
     */
    @PostMapping("update")
    public BaseRespVo update(@RequestBody AdCreate adCreate) {
        AdCreate adUpdate = spreadService.updateAd(adCreate);
        return BaseRespVo.ok(adUpdate);
    }
}
