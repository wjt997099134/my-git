package com.cskaoyan.controller.spread;

import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.spread.WxCategoryData;
import com.cskaoyan.bean.vo.spread.WxCurrentCategoryData;
import com.cskaoyan.service.WxCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 小程序类目管理
 * 前端接口为Catalog，实际类目单词为category
 * @author CR
 * @since 2022/06/07 16:08
 */
@RestController
@RequestMapping("wx/catalog")
public class WxCatalogController {
    @Autowired
    WxCategoryService categoryService;

    /**
     *  点击分类页面显示类目信息
     */
    @GetMapping("index")
    public BaseRespVo list() {
        WxCategoryData categoryData = categoryService.queryCategories();
        return BaseRespVo.ok(categoryData);
    }

    /**
     *  分类页面点击一级类目显示当前类目信息
     */
    @GetMapping("current")
    public BaseRespVo current(Integer id) {
        WxCurrentCategoryData currentCategoryData = categoryService.queryCurrentCategories(id);
        return BaseRespVo.ok(currentCategoryData);
    }
}
