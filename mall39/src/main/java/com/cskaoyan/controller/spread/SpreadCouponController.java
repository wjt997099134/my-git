package com.cskaoyan.controller.spread;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.spread.CouponCreateBo;
import com.cskaoyan.bean.bo.spread.CouponDeleteBo;
import com.cskaoyan.bean.bo.spread.CouponUpdateBo;
import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.spread.*;
import com.cskaoyan.service.SpreadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 优惠券管理
 * @author CR
 * @since 2022/06/05 20:01
 */
@RestController
@RequestMapping("admin/coupon")
public class SpreadCouponController {
    @Autowired
    SpreadService spreadService;

    /**
     *  1.获取优惠券列表（只显示deleted标志位为0的信息）；
     *  2.根据标题、类型、状态查询优惠券信息
     */
    @GetMapping("list")
    public BaseRespVo list(BaseParam param, String name, Integer type, Integer status) {
        CouponData couponData = spreadService.queryCoupon(param,name,type,status);
        return BaseRespVo.ok(couponData);
    }

    /**
     *  添加优惠券信息
     *  添加类型如果为兑换码优惠券，会随机生成一个8位兑换码(大写字母 + 数字)
     */
    @PostMapping("create")
    public BaseRespVo createCoupon(@RequestBody CouponCreateBo couponCreateBo) {
        if (couponCreateBo.getType() == 2) {
            CouponCreateType2 couponCreateType2 = spreadService.createCoupon2(couponCreateBo);
            return BaseRespVo.ok(couponCreateType2);
        }
        CouponCreate couponCreate = spreadService.createCoupon(couponCreateBo);
        return BaseRespVo.ok(couponCreate);
    }

    /**
     *  编辑修改优惠券信息
     */
    @PostMapping("update")
    public BaseRespVo updateCoupon(@RequestBody CouponUpdateBo couponUpdateBo) {
        CouponUpdate couponUpdate = spreadService.updateCoupon(couponUpdateBo);
        return BaseRespVo.ok(couponUpdate);
    }

    /**
     *  删除优惠券信息
     */
    @PostMapping("delete")
    public BaseResp deleteCoupon(@RequestBody CouponDeleteBo couponDeleteBo) {
        spreadService.deleteCoupon(couponDeleteBo.getId());
        return BaseResp.ok();
    }

    /**
     *  获取所选优惠券详情信息
     */
    @GetMapping("read")
    public BaseRespVo detailCoupon(Integer id) {
        // CouponDataListBean couponDetail = spreadService.queryCouponDetail(id);
        Coupon couponDetail = spreadService.queryCouponDetail(id);
        return BaseRespVo.ok(couponDetail);
    }

    /**
     *  在优惠券详情页获取coupon_user列表（此列表数据库中默认未维护数据）；
     *  根据ID、状态查询coupon_user
     */
    @GetMapping("listuser")
    public BaseRespVo listuserCoupon(BaseParam param,Integer couponId,Integer userId,Integer status) {
        ListuserData listuserData = spreadService.queryListuser(param,couponId,userId,status);
        return BaseRespVo.ok(listuserData);
    }
}
