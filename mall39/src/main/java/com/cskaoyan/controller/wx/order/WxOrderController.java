package com.cskaoyan.controller.wx.order;

import com.cskaoyan.bean.bo.WxBaseParam;
import com.cskaoyan.bean.bo.WxOrderCommentBO;
import com.cskaoyan.bean.bo.WxOrderIdBO;
import com.cskaoyan.bean.bo.WxOrderSubmitBO;
import com.cskaoyan.bean.po.OrderGoods;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxOrderDetailVO;
import com.cskaoyan.bean.vo.WxOrderListVO;
import com.cskaoyan.bean.vo.WxOrderSubmitVO;
import com.cskaoyan.service.MarketService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 微信小程序订单模块
 *
 * @author WuHuaguo
 * @since 2022/06/07 15:12
 */

@RestController
@RequestMapping("wx/order")
public class WxOrderController {

    @Autowired
    MarketService marketService;

    @RequestMapping("list")
    public BaseRespVo list(WxBaseParam baseParam) {
        WxOrderListVO wxOrderListVO = marketService.wxOrderList(baseParam);
        return BaseRespVo.ok(wxOrderListVO);
    }

    @RequestMapping("detail")
    public BaseRespVo detail(Integer orderId) {
        WxOrderDetailVO wxOrderDetailVO = marketService.wxOrderDetail(orderId);
        return BaseRespVo.ok(wxOrderDetailVO);
    }

    /**
     * 申请退款
     *
     * @param wxOrderIdBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 10:57
     */
    @RequestMapping("refund")
    public BaseRespVo refund(@RequestBody WxOrderIdBO wxOrderIdBO) {
        marketService.wxOrderRefund(wxOrderIdBO);
        return BaseRespVo.ok();
    }

    /**
     * 删除订单
     *
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 11:37
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody WxOrderIdBO wxOrderIdBO) {
        marketService.wxOrderDelete(wxOrderIdBO);
        return BaseRespVo.ok();
    }

    /**
     * 确认收货
     *
     * @param wxOrderIdBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 14:29
     */
    @RequestMapping("confirm")
    public BaseRespVo confirm(@RequestBody WxOrderIdBO wxOrderIdBO) {
        marketService.wxOrderConfirm(wxOrderIdBO);
        return BaseRespVo.ok();
    }

    /**
     * 取消订单
     *
     * @param wxOrderIdBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 15:11
     */
    @RequestMapping("cancel")
    public BaseRespVo cancel(@RequestBody WxOrderIdBO wxOrderIdBO) {
        marketService.wxOrderCancel(wxOrderIdBO);
        return BaseRespVo.ok();
    }

    /**
     * 评论中的显示商品信息
     * admin/wx/goods
     *
     * @param orderId
     * @param goodsId
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 15:46
     */
    @RequestMapping("goods")
    public BaseRespVo goods(Integer orderId, Integer goodsId) {
        OrderGoods orderGoods = marketService.wxOrderGoods(orderId, goodsId);
        return BaseRespVo.ok(orderGoods);
    }

    /**
     * 评论
     *
     * @param wxOrderCommentBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 16:40
     */
    @RequestMapping("comment")
    public BaseRespVo comment(@RequestBody WxOrderCommentBO wxOrderCommentBO) {
        marketService.wxOrderComment(wxOrderCommentBO);
        return BaseRespVo.ok();
    }

    /**
     * 下单
     *
     * @param wxOrderSubmitBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/08 21:15
     */
    @RequestMapping("submit")
    public BaseRespVo submit(@RequestBody WxOrderSubmitBO wxOrderSubmitBO) {
        WxOrderSubmitVO wxOrderSubmitVO = marketService.wxOrderSubmit(wxOrderSubmitBO);

        if (wxOrderSubmitBO == null) {
            return BaseRespVo.invalidParameter("库存不足");
        }
        return BaseRespVo.ok(wxOrderSubmitVO);
    }

    /**
     * 付款
     *
     * @param wxOrderIdBO
     * @return com.cskaoyan.bean.vo.BaseRespVo
     * @author WuHuaguo
     * @since 2022/06/09 14:41
     */
    @RequestMapping("prepay")
    public BaseRespVo prepay(@RequestBody WxOrderIdBO wxOrderIdBO) {
        marketService.wxOrderPrepay(wxOrderIdBO);
        return BaseRespVo.ok();
    }
}
