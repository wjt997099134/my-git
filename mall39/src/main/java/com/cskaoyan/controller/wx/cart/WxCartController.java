package com.cskaoyan.controller.wx.cart;

import com.cskaoyan.bean.bo.cartbo.CartCheckedBO;
import com.cskaoyan.bean.bo.cartbo.CartCreateBO;
import com.cskaoyan.bean.bo.cartbo.CartIdsBO;
import com.cskaoyan.bean.bo.cartbo.CartUpdateBO;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.cartvo.CartIndexVO;
import com.cskaoyan.bean.vo.cartvo.CartOrderVO;
import com.cskaoyan.service.CartServiceImpl;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;

/**
 * @param
 * @Date: 2022/6/7 9:09
 * @Description:微信购物车功能
 * @Author: 李宇浩
 * @return
 */
@RequestMapping("wx/cart")
@RestController
public class WxCartController {

    @Autowired
    CartServiceImpl cartService;

    /**
     * 用户名获取
     */
    private String getUsername() {
        Subject subject = SecurityUtils.getSubject();
        User primaryPrincipal = ((User) subject.getPrincipals().getPrimaryPrincipal());
        String username = primaryPrincipal.getUsername();
        return username;
    }

    /**
     * 商品详情页面的购物车图标显示购物车数量红点goodscount
     */
    @RequestMapping("goodscount")
    public BaseRespVo goodsCount() {
        String username = getUsername();
        int count = cartService.goodsCount(username);
        return BaseRespVo.ok(count);
    }

    /**
     * 添加购物车add 待优化
     */
    @RequestMapping("add")
    public BaseRespVo add(@RequestBody CartCreateBO cartCreateBO) {
        String username = getUsername();
        int count = cartService.addCart(cartCreateBO, username);
        if (count == 404) {
            return BaseRespVo.invalidData();
        }
        return BaseRespVo.ok(count);
    }

    /**
     * 购物车详情显示index
     */
    @RequestMapping("index")
    public BaseRespVo index() {
        String username = getUsername();
        CartIndexVO cartIndexVO = cartService.getIndex(username);
        return BaseRespVo.ok(cartIndexVO);
    }

    /**
     * 购物车更新update
     */
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody CartUpdateBO cartUpdateBO) {

        int code = cartService.updateCart(cartUpdateBO);
        if (code == 404) {
            return BaseRespVo.invalidData();
        }
        return BaseRespVo.ok();
    }

    /**
     * 购物车选中状态更新checked
     */
    @RequestMapping("checked")
    public BaseRespVo checked(@RequestBody CartCheckedBO checkedBO) {
        String username = getUsername();
        CartIndexVO cartIndexVO = cartService.checked(checkedBO, username);

        return BaseRespVo.ok(cartIndexVO);
    }

    /**
     * 在商品详情页面直接点击下单直接添加商品至购物车表fastadd
     */
    @RequestMapping("fastadd")
    public BaseRespVo fastAdd(@RequestBody CartCreateBO cartCreateBO) {
        String username = getUsername();
        int code = cartService.fastAdd(cartCreateBO, username);

        if (code == 404) {
            return BaseRespVo.invalidData();
        }
        
        return BaseRespVo.ok(code);
    }

    /**
     * 下单付款前界面参数回显checkout
     */
    @RequestMapping("checkout")
    public BaseRespVo checkout(Integer cartId, Integer addressId, Integer couponId, Integer userCouponId, Integer grouponRulesId) {

        CartOrderVO cartOrderVO = cartService.getCartOrder(cartId, addressId, couponId, userCouponId, grouponRulesId);

        return BaseRespVo.ok(cartOrderVO);
    }

    /**
     * 删除购物车商品delete
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody CartIdsBO cartIdsBO) {
        String username = getUsername();
        CartIndexVO cartIndexVO = cartService.delete(cartIdsBO, username);
        return BaseRespVo.ok(cartIndexVO);
    }

}
