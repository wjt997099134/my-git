package com.cskaoyan.controller.wx.user;

import com.cskaoyan.bean.po.Order;
import com.cskaoyan.bean.po.OrderExample;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxUserIndexInnerVO;
import com.cskaoyan.bean.vo.WxUserIndexVO;
import com.cskaoyan.mapper.OrderMapper;
import com.cskaoyan.util.ConstantProperties;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @ClassName: UserIndexController
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 11:30
 */

@RestController
@RequestMapping("wx/user")
public class UserIndexController {

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    ConstantProperties CONSTANT;

    @RequestMapping("index")
    public BaseRespVo index() {
        // http://182.92.235.201:8083/wx/user/index

        // 待评价
        Integer unComment = 0;
        // 待付款
        Integer unPaid = 0;
        // 待收货
        Integer unRecv = 0;
        // 待发货
        Integer unShip = 0;

        Subject subject = SecurityUtils.getSubject();
        if (!subject.isAuthenticated()) {
            return BaseRespVo.invalidData("请先登陆！！");
        } else {
            User primaryPrincipal = (User) subject.getPrincipals().getPrimaryPrincipal();

            OrderExample example = new OrderExample();
            OrderExample.Criteria criteria = example.createCriteria();
            // 未被删除
            criteria.andDeletedEqualTo(CONSTANT.getNotDeleted());
            criteria.andUserIdEqualTo(primaryPrincipal.getId());
            List<Order> orders = orderMapper.selectByExample(example);

            for (Order order : orders) {
                if ((short) order.getOrderStatus() == CONSTANT.getOrderNotPay()) {
                    unPaid++;
                    continue;
                }

                if ((short) order.getOrderStatus() == CONSTANT.getOrderPay()) {
                    unShip++;
                    continue;
                }

                if ((short) order.getOrderStatus() == CONSTANT.getOrderShip()) {
                    unRecv++;
                    continue;
                }

                // if ((short) order.getOrderStatus() == CONSTANT.getOrderReceive() || order.getOrderStatus() == 402) {
                if (((short) order.getOrderStatus() == CONSTANT.getOrderReceive() || order.getOrderStatus() == 402) && order.getComments() != 0) {
                    unComment++;
                }
            }
        }

        WxUserIndexInnerVO wxUserIndexInnerVO = new WxUserIndexInnerVO(unComment, unPaid, unRecv, unShip);
        WxUserIndexVO wxUserIndexVO = new WxUserIndexVO(wxUserIndexInnerVO);
        return BaseRespVo.ok(wxUserIndexVO);
    }
}