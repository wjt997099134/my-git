package com.cskaoyan.controller.wx.footprint;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.FootprintListVO;
import com.cskaoyan.service.FootprintService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author karlfu
 * @since 2022/06/08 11:28
 */

@RestController
@RequestMapping("wx/footprint")
public class WxFootprintController {
    @Autowired
    FootprintService footprintService;

    @GetMapping("list")
    public BaseRespVo getFootprintDetailList(BaseParam baseParam) {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }
        User user = (User) subject.getPrincipals().getPrimaryPrincipal();
        FootprintListVO footprintListVO = footprintService.footprintList(baseParam, user.getId());
        return BaseRespVo.ok(footprintListVO);
    }

    // 长按单条记录三秒删除足迹
    @PostMapping("delete")
    public BaseRespVo delFootPrint(@RequestBody Map map) {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }
        int toDelFootprintId = (Integer) map.get("id");
        footprintService.delFootPrint(toDelFootprintId);
        return BaseRespVo.ok();
    }
}
