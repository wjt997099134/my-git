package com.cskaoyan.controller.wx.collect;

import com.cskaoyan.bean.bo.WxCollectAddordeleteBo;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxCollectListData;
import com.cskaoyan.service.WxCollectService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 小程序-收藏模块
 *
 * @author yemingfei
 * @since 2022/06/08 20:05
 */
@RestController
@RequestMapping("/wx/collect")
public class WxCollectController {

    @Autowired
    WxCollectService wxCollectService;
    @RequestMapping("list")
    public BaseRespVo getCollectList(Integer type, Integer page, Integer limit){
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }
        WxCollectListData wxCollectListData = wxCollectService.collectList(type,page,limit);
        return BaseRespVo.ok(wxCollectListData);
    }

    @RequestMapping("addordelete")
    public BaseRespVo getCollectAddordelete(@RequestBody WxCollectAddordeleteBo wxCollectAddordeleteBo){
        // 加入收藏或取消收藏
        wxCollectService.addordelete(wxCollectAddordeleteBo);
        return BaseRespVo.ok();

    }
}
