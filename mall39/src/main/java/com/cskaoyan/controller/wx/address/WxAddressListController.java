package com.cskaoyan.controller.wx.address;

import com.cskaoyan.bean.bo.AddressDeleteData;
import com.cskaoyan.bean.bo.WxAddressSaveBo;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxAddressListVO;
import com.cskaoyan.bean.vo.user.AddressDataListBean;
import com.cskaoyan.service.AddressService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author karlfu
 * @since 2022/06/07 20:44
 */

@RestController
@RequestMapping("wx/address")
public class WxAddressListController {
    @Autowired
    AddressService addressService;

    @GetMapping("list")
    public BaseRespVo getAddressList() {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }
        User user = (User)subject.getPrincipals().getPrimaryPrincipal();
        WxAddressListVO wxAddressListVO = addressService.getAddressList(user.getId());
        return BaseRespVo.ok(wxAddressListVO);
    }

    @RequestMapping("detail")
    public BaseRespVo getAddressDetail(Integer id){
        AddressDataListBean addressDataListBean = addressService.addressDetail(id);
        return BaseRespVo.ok(addressDataListBean);
    }

    @RequestMapping("save")
    public BaseRespVo getAddressSave(@RequestBody WxAddressSaveBo wxAddressSaveBo){
        // 返回地址id 新建地址和保存修改后的地址
        Integer addressId = addressService.addressSave(wxAddressSaveBo);
        return BaseRespVo.ok(addressId);
    }

    @RequestMapping("delete")
    public BaseRespVo getAddressDelete(@RequestBody AddressDeleteData addressDeleteData){
        // 删除地址
        addressService.addressDelete(addressDeleteData.getId());
        return BaseRespVo.ok();
    }
}
