package com.cskaoyan.controller.wx.search;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.po.Keyword;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.SearchIndexVO;
import com.cskaoyan.service.SearchService;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * @author karlfu
 * @since 2022/06/09 10:00
 */

@RestController
@RequestMapping("wx/search")
public class WxSearchController {
    @Autowired
    SearchService searchService;

    @GetMapping("index")
    public BaseRespVo getSearchIndexList(BaseParam baseParam) {

        SearchIndexVO searchIndexVO = new SearchIndexVO();
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (authenticated) {
            User user = (User) subject.getPrincipals().getPrimaryPrincipal();
            List<SearchIndexVO.HistoryKeyword> historyKeywordList = searchService.getHistoryKeywordList(user.getId());
            searchIndexVO.setHistoryKeywordList(historyKeywordList);
        }

        List<Keyword> defaultKeywordList = searchService.getDefaultKeywordList();
        if (defaultKeywordList != null && defaultKeywordList.size() > 0) {
            Random random = new Random();
            searchIndexVO.setDefaultKeyword(defaultKeywordList.get(random.nextInt(defaultKeywordList.size())));
        }

        List<Keyword> hotKeywordList = searchService.getHotKeywordList();
        searchIndexVO.setHotKeywordList(hotKeywordList);

        return BaseRespVo.ok(searchIndexVO);
    }

    // 前端发的是空POST
    @PostMapping("clearhistory")
    public BaseRespVo clearHistory() {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (authenticated) {
            User user = (User) subject.getPrincipals().getPrimaryPrincipal();
            Integer userId = user.getId();
            searchService.clearHistoryByUserId(userId);
            return BaseRespVo.ok();
        }

        return BaseRespVo.invalidParameter("未登录不可以清除足迹");
    }

    @GetMapping("helper")
    public BaseRespVo searchHelper(String keyword) {
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (authenticated && StringUtils.isNotEmpty(keyword)) {
            User user = (User) subject.getPrincipals().getPrimaryPrincipal();
            Integer userId = user.getId();
            List<String> historyKeywordHelperList = searchService.getHistoryKeywordHelperList(user.getId(), keyword);
            return BaseRespVo.ok(historyKeywordHelperList);
        }
        return BaseRespVo.ok();
    }
}
