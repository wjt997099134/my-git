package com.cskaoyan.controller.wx.feedback;

import com.cskaoyan.bean.bo.WxFeedbackBO;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.service.FeedbackService;
import com.cskaoyan.util.ValidationUtils;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

/**
 * @author karlfu
 * @since 2022/06/07 17:30
 */

@RestController
@RequestMapping("wx/feedback")
public class WxFeedbackSubmitController {
    @Autowired
    FeedbackService feedbackService;

    @PostMapping("submit")
    public BaseRespVo feedbackSubmit(@RequestBody @Valid WxFeedbackBO wxFeedbackBO, BindingResult bindingResult) {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }

        BaseRespVo valid = ValidationUtils.valid(bindingResult);

        if (valid != null) {
            return valid;
        };

        User user = (User)subject.getPrincipals().getPrimaryPrincipal();
        feedbackService.feedbackSubmit(wxFeedbackBO, user.getId(), user.getUsername());
        return BaseRespVo.ok();
    }
}
