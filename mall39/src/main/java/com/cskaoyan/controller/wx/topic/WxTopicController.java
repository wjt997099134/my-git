package com.cskaoyan.controller.wx.topic;

import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.TopicListVO;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.bean.vo.topic.WxTopicDetailVO;
import com.cskaoyan.bean.vo.topic.WxTopicListVO;
import com.cskaoyan.mapper.TopicMapper;
import com.cskaoyan.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: WxTopicController
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 15:14
 */

@RestController
@RequestMapping("wx/topic")
public class WxTopicController {

    @Autowired
    TopicService topicService;

    @RequestMapping("list")
    public BaseRespVo list(@RequestParam Map requestMap) {
        // http://localhost:8083/wx/topic/list?page=1&limit=10
        Integer page = Integer.parseInt((String) requestMap.get("page"));
        Integer limit = Integer.parseInt((String) requestMap.get("limit"));

        WxTopicListVO wxTopicListVO = topicService.getWxTopicList(page, limit);
        return BaseRespVo.ok(wxTopicListVO);
    }

    @RequestMapping("detail")
    public BaseRespVo detail(@RequestParam Map requestMap) {
        // http://localhost:8083/wx/topic/detail?id=317
        Integer id = Integer.parseInt((String) requestMap.get("id"));

        // 查询专题信息
        WxTopicDetailVO wxTopicDetailVO = topicService.getTopicDetail(id);
        return BaseRespVo.ok(wxTopicDetailVO);
    }

    @RequestMapping("related")
    public BaseRespVo related(@RequestParam Map requestMap) {
        // http://localhost:8083/wx/topic/related?id=317
        Integer id = Integer.parseInt((String) requestMap.get("id"));

        // 获取相似专题
        TopicListVO topicListVO = topicService.getRelateTopic(id);
        return BaseRespVo.ok(topicListVO);
    }
}