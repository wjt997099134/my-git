package com.cskaoyan.controller.wx.login;

import com.cskaoyan.bean.bo.AdminInfoBean;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.po.UserExample;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.LoginUserData;
import com.cskaoyan.bean.vo.WxLoginUserInfoVO;
import com.cskaoyan.bean.vo.WxLoginVO;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.config.shiro.MallToken;
import com.cskaoyan.mapper.UserMapper;
import com.cskaoyan.service.UserService;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * @ClassName: WxLoginController
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/7 16:20
 */

@RestController
@RequestMapping("wx/auth")
public class WxLoginController {

    // @Autowired
    // UserService userService;

    @Autowired
    UserMapper userMapper;

    // @RequiresPermissions(value = {"wx"}, logical = Logical.OR)
    @RequestMapping("login")
    public BaseRespVo login(@RequestBody Map map) {
        String username = (String) map.get("username");
        String password = (String) map.get("password");

        // LoginUserData loginUserData = new LoginUserData();
        // AdminInfoBean adminInfo = new AdminInfoBean();
        // adminInfo.setAvatar("https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif");
        // adminInfo.setNickName("admin123");
        // loginUserData.setAdminInfo(adminInfo);
        // loginUserData.setToken("c4d988f3-e4c5-4e46-b6ce-13b9e008ea20");
        // return BaseRespVo.ok(loginUserData);
        Subject subject = SecurityUtils.getSubject();

        subject.login(new MallToken(username, Md5Util.getMD5(password), "wx"));

        String token = (String) subject.getSession().getId();
        User primaryPrincipal = (User) subject.getPrincipals().getPrimaryPrincipal();

        WxLoginVO wxLoginVO = new WxLoginVO();
        if (primaryPrincipal.getPassword().equals(Md5Util.getMD5(password))) {

            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andUsernameEqualTo(username);

            List<User> users = userMapper.selectByExample(example);
            for (User user : users) {
                String avatar = user.getAvatar();
                String nickname = user.getNickname();
                WxLoginUserInfoVO wxLoginUserInfoVO = new WxLoginUserInfoVO(avatar, nickname);

                wxLoginVO.setToken(token);
                wxLoginVO.setUserInfo(wxLoginUserInfoVO);
                return BaseRespVo.ok(wxLoginVO);
            }
        }
        return BaseRespVo.ok(null);
    }

    @RequestMapping("logout")
    public BaseRespVo logout() {
        Subject subject = SecurityUtils.getSubject();
        subject.logout();
        return BaseRespVo.ok();
    }
}