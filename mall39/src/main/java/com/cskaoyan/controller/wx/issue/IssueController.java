package com.cskaoyan.controller.wx.issue;

import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.MarketIssueVO;
import com.cskaoyan.service.MarketIssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wx/issue")
public class IssueController {

    @Autowired
    MarketIssueService issueService;

    @RequestMapping("list")
    public BaseRespVo list(Integer page, Integer limit) {
        MarketIssueVO marketIssueVO = issueService.querryList(page, limit, null, null, null);
        return BaseRespVo.ok(marketIssueVO);
    }
}
