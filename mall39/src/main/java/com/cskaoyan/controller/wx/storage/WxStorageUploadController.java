package com.cskaoyan.controller.wx.storage;

import com.cskaoyan.bean.po.Storage;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.StorageVO;
import com.cskaoyan.service.StorageService;
import com.cskaoyan.util.StorageProperties;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author karlfu
 * @since 2022/06/07 15:59
 */

@RestController
@RequestMapping("wx/storage")
public class WxStorageUploadController {

    /*// 正常的应该是调用service层 这里偷了懒
    @Autowired
    StorageController storageController;

    @RequestMapping("upload")
    public BaseRespVo uploadFile(MultipartFile file) throws IOException {
        return storageController.uploadFile(file);
    }*/

    @Autowired
    StorageService storageService;
    @Autowired
    StorageProperties STORAGE_CONSTANT;

    @PostMapping("upload")
    public BaseRespVo uploadFile(@RequestBody MultipartFile file) throws IOException {

        // 这个请求前端Header不发送token故不可验证
        // 判断是否已经登录
//        Subject subject = SecurityUtils.getSubject();
//        boolean authenticated = subject.isAuthenticated();
//        if (!authenticated) {
//            return BaseRespVo.invalidData("未登录或登录凭据已失效");
//        }

        String contentType = file.getContentType(); //正文类型，通过正文类型可以做不同类型的文件的区分
        if (!contentType.startsWith("image")) {
            return BaseRespVo.invalidParameter("上传的不是图片！");
        }

        Storage storage = new Storage();
        storage.setType(contentType);

        String originalFilename = file.getOriginalFilename(); // 上传时的文件名
        storage.setName(originalFilename);

        long size = file.getSize(); //文件大小（单位：字节）
        storage.setSize((int) size);

        Date addTime = new Date();
        storage.setAddTime(addTime);
        storage.setUpdateTime(addTime);

        String suffix = null;
        if (originalFilename != null) {
            suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        }

        String key = UUID.randomUUID().toString();
        storage.setKey(key + suffix);

        // d:/mall/spring/
        String basePath = STORAGE_CONSTANT.getStoragePath();

        File filePath = new File(basePath, key + suffix);
        if (!filePath.getParentFile().exists()) {
            filePath.getParentFile().mkdirs();
        }
        // 将接收到的文件保存在指定位置
        file.transferTo(filePath);

        // http://localhost:8083/pic/
        String netPath = STORAGE_CONSTANT.getNetPath();
        storage.setUrl(netPath + key + suffix);

        //deleted不传 而数据库设置了默认为0
        storageService.storageCreate(storage);

        StorageVO storageVO = new StorageVO(storage.getId(), storage.getKey(), storage.getName(), storage.getType(), storage.getSize(), storage.getUrl(), storage.getAddTime(), storage.getUpdateTime());

        return BaseRespVo.ok(storageVO);
    }
}
