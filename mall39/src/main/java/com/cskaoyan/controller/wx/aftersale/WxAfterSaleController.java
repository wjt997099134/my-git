package com.cskaoyan.controller.wx.aftersale;

import com.cskaoyan.bean.bo.WxAddressSaveBo;
import com.cskaoyan.bean.bo.WxAfterSaleBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxAfterSaleDetailVO;
import com.cskaoyan.service.AfterSaleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: WxAfterSaleController
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 9:23
 */

@RestController
@RequestMapping("wx/aftersale")
public class WxAfterSaleController {

    @Autowired
    AfterSaleService afterSaleService;

    @RequestMapping("submit")
    public BaseRespVo afterSale(@RequestBody WxAfterSaleBO wxAfterSaleBO) {
        Integer affectedRows = afterSaleService.submitAfterSale(wxAfterSaleBO);
        if (affectedRows == 1) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidParameter("申请失败！！");
        }
    }

    @RequestMapping("detail")
    public BaseRespVo detail(Integer orderId) {
        // Integer orderId = Integer.parseInt((String) requestMap.get("orderId"));

        WxAfterSaleDetailVO wxAfterSaleDetailVO = afterSaleService.afterSaleDetail(orderId);
        return BaseRespVo.ok(wxAfterSaleDetailVO);
    }
}