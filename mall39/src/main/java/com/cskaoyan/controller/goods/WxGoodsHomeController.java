package com.cskaoyan.controller.goods;


import com.cskaoyan.bean.vo.AboutVO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxHomeListVo;
import com.cskaoyan.config.AboutUsConfiguration;
import com.cskaoyan.service.WxGoodsService;
import com.cskaoyan.util.AboutUsProperties;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wx/home")
public class WxGoodsHomeController {

    @Autowired
    WxGoodsService wxGoodsService;
    @Autowired
    ConstantProperties Constant;
    @Autowired
    AboutUsProperties ABOUT;

    @RequestMapping("index")
    public BaseRespVo index() {
        WxHomeListVo wxHomeListVo = wxGoodsService.homeList();
        return BaseRespVo.ok(wxHomeListVo);
    }

    /**
     * 必备工具 -> 关于我们
     */
    @RequestMapping("about")
    public BaseRespVo aboutUs() {
        AboutVO aboutVO = new AboutVO();
        aboutVO.setQq(ABOUT.getQq());
        aboutVO.setAddress(ABOUT.getAddress());
        aboutVO.setName(ABOUT.getName());
        aboutVO.setPhone(ABOUT.getPhone());
        aboutVO.setLatitude(ABOUT.getLatitude());
        aboutVO.setLongitude(ABOUT.getLongitude());
        return BaseRespVo.ok(aboutVO);
    }
}
