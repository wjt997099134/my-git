package com.cskaoyan.controller.goods;

import com.cskaoyan.bean.bo.WxBaseParam;
import com.cskaoyan.bean.bo.WxCommentPostBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxCommentCountData;
import com.cskaoyan.bean.vo.WxCommentData;
import com.cskaoyan.bean.vo.WxCommentPostVO;
import com.cskaoyan.service.CommentService;
import com.cskaoyan.service.WxGoodsService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wx/comment")
public class WxCommentController {
    @Autowired
    WxGoodsService wxGoodsService;
    @Autowired
    ConstantProperties CONSTANT;

    @Autowired
    CommentService commentService;

    @RequestMapping("list")
    public BaseRespVo list(WxBaseParam wxBaseParam, Integer valueId, Integer type) {
        WxCommentData wxCommentData = wxGoodsService.commentList(wxBaseParam, valueId, type);
        return BaseRespVo.ok(wxCommentData);
    }

    @RequestMapping("count")
    public BaseRespVo count(Integer valueId, Integer type) {
        WxCommentCountData wxCommentCountData = wxGoodsService.commentCount(valueId, type);
        return BaseRespVo.ok(wxCommentCountData);
    }

    @RequestMapping("post")
    public BaseRespVo postComment(@RequestBody WxCommentPostBO wxCommentPostBO) {
        WxCommentPostVO wxCommentPostVO = commentService.postComment(wxCommentPostBO);
        return BaseRespVo.ok(wxCommentPostVO);
    }
}
