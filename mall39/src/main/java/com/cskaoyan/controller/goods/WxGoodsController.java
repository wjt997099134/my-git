package com.cskaoyan.controller.goods;


import com.cskaoyan.bean.GoodsListBaseParam;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.service.WxGoodsService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("wx/goods")
public class WxGoodsController {

    @Autowired
    WxGoodsService wxGoodsService;
    @Autowired
    ConstantProperties CONSTANT;

    @RequestMapping("count")
    public BaseRespVo count() {
        int count = wxGoodsService.countGoods();
        return BaseRespVo.ok(count);
    }

    @RequestMapping("category")
    public BaseRespVo category(Integer id) {
        WxCategoryInfoVo wxCategoryInfovo = wxGoodsService.categoryInfo(id);
        return BaseRespVo.ok(wxCategoryInfovo);
    }
    @RequestMapping("list")
    public BaseRespVo goodsList(GoodsListBaseParam goodsListBaseParam) {
        WxGoodsListVo wxGoodsListVo = wxGoodsService.goodsList(goodsListBaseParam);
        return BaseRespVo.ok(wxGoodsListVo);
    }
    @RequestMapping("detail")
    public BaseRespVo goodsDetail(Integer id) {
        WxGoodsDetailVo wxGoodsDetailVo = wxGoodsService.goodsDetail(id);
        return BaseRespVo.ok(wxGoodsDetailVo);
    }
    @RequestMapping("related")
    public BaseRespVo goodsRelated(Integer id) {
        WxGoodsRelatedData wxGoodsRelatedData = wxGoodsService.goodsRelated(id);
        return BaseRespVo.ok(wxGoodsRelatedData);
    }
}
