package com.cskaoyan.controller.goods;


import com.cskaoyan.bean.vo.*;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.service.WxGrouponService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("wx/groupon")
public class WxGrouponController {

    @Autowired
    WxGrouponService wxGrouponService;
    @Autowired
    ConstantProperties CONSTANT;

    @RequestMapping("list")
    public BaseRespVo list(Integer page,Integer limit){
        GrouponData grouponData = wxGrouponService.grouponList(page,limit);
        return BaseRespVo.ok(grouponData);
    }
    @RequestMapping("detail")
    public BaseRespVo detail(Integer grouponId){
        GrouponDetailData grouponDetailData = wxGrouponService.grouponDetail(grouponId);
        return BaseRespVo.ok(grouponDetailData);
    }
    @RequestMapping("my")
    public BaseRespVo my(Integer showType){
        MyGrouponData myGrouponData = wxGrouponService.myGroupon(showType);
        return BaseRespVo.ok(myGrouponData);
    }
    @RequestMapping("join")
    public BaseRespVo join(Integer grouponId){
        WxGoodsDetailVo wxGoodsDetailVo = wxGrouponService.joinGroupon(grouponId);
        return BaseRespVo.ok(wxGoodsDetailVo);
    }
}
