package com.cskaoyan.controller.storage;

import com.cskaoyan.bean.bo.StorageListBaseParam;
import com.cskaoyan.bean.po.Storage;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.StorageDataVO;
import com.cskaoyan.bean.vo.StorageVO;
import com.cskaoyan.service.StorageService;
import com.cskaoyan.util.StorageProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import java.util.UUID;

/**
 * @author karlfu
 * @since 2022/06/05 14:45
 */

@RestController
@RequestMapping("admin/storage")
public class StorageController {

    @Autowired
    StorageService storageService;
    @Autowired
    StorageProperties STORAGE_CONSTANT;

    /* {"errno":0,"data":{"id":946,"key":"c18pj1e82emdqx37zv1p.png",
     "name":"111.png","type":"image/png","size":178697,
     "url":"http://182.92.235.201:8083/wx/storage/fetch/c18pj1e82emdqx37zv1p.png",
     "addTime":"2022-06-05 14:44:27","updateTime":"2022-06-05 14:44:27"},"errmsg":"成功"}*/

    @RequestMapping("create")
    public BaseRespVo uploadFile(@RequestBody MultipartFile file) throws IOException {

        String contentType = file.getContentType(); //正文类型，通过正文类型可以做不同类型的文件的区分
        if (!contentType.startsWith("image")) {
            return BaseRespVo.invalidParameter("上传的不是图片！");
        }

        Storage storage = new Storage();
        storage.setType(contentType);

        String originalFilename = file.getOriginalFilename(); // 上传时的文件名
        storage.setName(originalFilename);

        long size = file.getSize(); //文件大小（单位：字节）
        storage.setSize((int) size);

        Date addTime = new Date();
        storage.setAddTime(addTime);
        storage.setUpdateTime(addTime);

        String suffix = null;
        if (originalFilename != null) {
            suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        }

        String key = UUID.randomUUID().toString();
        storage.setKey(key + suffix);

        // d:/mall/spring/
        String basePath = STORAGE_CONSTANT.getStoragePath();

        File filePath = new File(basePath, key + suffix);
        if (!filePath.getParentFile().exists()) {
            filePath.getParentFile().mkdirs();
        }
        // 将接收到的文件保存在指定位置
        file.transferTo(filePath);

        // http://localhost:8083/pic/
        String netPath = STORAGE_CONSTANT.getNetPath();
        storage.setUrl(netPath + key + suffix);

        //deleted不传 而数据库设置了默认为0
        storageService.storageCreate(storage);

        StorageVO storageVO = new StorageVO(storage.getId(), storage.getKey(), storage.getName(), storage.getType(), storage.getSize(), storage.getUrl(), storage.getAddTime(), storage.getUpdateTime());

        return BaseRespVo.ok(storageVO);
    }

    @RequestMapping("list")
    public BaseRespVo listFile(StorageListBaseParam param) {
        StorageDataVO storageDataVO = storageService.queryAll(param);
        return BaseRespVo.ok(storageDataVO);
    }

    @RequestMapping("delete")
    public BaseRespVo deleteFile(@RequestBody Storage marketStorage) {
        // 假删除
        Storage storage = new Storage();
        storage.setId(marketStorage.getId());
        storage.setDeleted(true);
        storageService.deleteById(storage);
        return BaseRespVo.ok();
    }

    @RequestMapping("update")
    public BaseRespVo updateFile(@RequestBody Storage marketStorage) {
        // 假删除
        Storage storage = new Storage();
        storage.setId(marketStorage.getId());
        storage.setName(marketStorage.getName());

        Date updateTime = new Date();
        storage.setUpdateTime(updateTime);

        storageService.updateById(storage);
        return BaseRespVo.ok(storage);
    }
}
