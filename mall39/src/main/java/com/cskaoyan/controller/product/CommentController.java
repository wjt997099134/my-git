package com.cskaoyan.controller.product;

import com.cskaoyan.bean.po.Comment;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.CommentListVO;
import com.cskaoyan.service.CommentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: CommentController
 * @Description: 商品评论管理模块
 * @Author: zhanghaiyang
 * @Date: 2022/6/5 22:00
 */

@RestController
@RequestMapping("admin/comment")
public class CommentController {

    @Autowired
    CommentService commentService;

    @RequestMapping("list")
    public BaseRespVo list(@RequestParam Map requestMap) {
        // http://localhost:8083/admin/comment/list?page=1&limit=20&userId=1&valueId=1011&sort=add_time&order=desc

        Integer page = Integer.parseInt((String) requestMap.get("page"));
        Integer limit = Integer.parseInt((String) requestMap.get("limit"));

        Integer userId = null;
        Integer valueId = null;

        if (requestMap.containsKey("userId")) {
            if (!"".equals(requestMap.get("userId"))) {
                userId = Integer.parseInt((String) requestMap.get("userId"));
            }
        }

        if (requestMap.containsKey("valueId")) {
            if (!"".equals(requestMap.get("valueId"))) {
                valueId = Integer.parseInt((String) requestMap.get("valueId"));
            }
        }

        // 排序的规则：按照时间排序
        String sort = (String) requestMap.get("sort");
        // 排序的顺序：降序
        String order = (String) requestMap.get("order");

        CommentListVO commentListVO = commentService.getCommentList(page, limit, sort, order, userId, valueId);
        return BaseRespVo.ok(commentListVO);
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Comment comment) {
        // http://182.92.235.201:8083/admin/comment/delete
        // {"id":1068,"valueId":274,"type":1,"content":"hao","adminContent":"111","userId":1,"hasPicture":false,"picUrls":[],"star":5,"addTime":"2022-05-10 20:43:33","updateTime":"2022-05-10 20:43:33","deleted":false}
        // 根据用户id和更新时间来假删除评论
        commentService.deleteComment(comment);
        return BaseRespVo.ok(null);
    }
}