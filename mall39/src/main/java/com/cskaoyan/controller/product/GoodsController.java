package com.cskaoyan.controller.product;


import com.cskaoyan.bean.bo.GoodsCreateBO;
import com.cskaoyan.bean.bo.GoodsDetail;
import com.cskaoyan.bean.bo.GoodsDetailBO;
import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.CatAndBrandVO;
import com.cskaoyan.bean.vo.GoodsData;
import com.cskaoyan.bean.vo.GoodsDetailVO;
import com.cskaoyan.service.GoodsService;
import com.cskaoyan.service.GoodsServiceImpl;
import com.fasterxml.jackson.databind.ser.Serializers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @param
 * @Date: 2022/6/4 16:31
 * @Description:商品管理 DOING
 * @Author: 李宇浩
 * @return
 */
@RestController
@RequestMapping("admin/goods")
public class GoodsController {

    @Autowired
    GoodsServiceImpl goodsService;

    /**
     * 获取管理界面商品信息
     */
    @RequestMapping("list")
    public BaseRespVo goodsList(String page, String limit, String goodsSn, String name, String sort, String order, Integer goodsId) {

        GoodsData goodsList = goodsService.querryAll(page, limit, goodsSn, name, sort, order, goodsId);
        BaseRespVo ok = BaseRespVo.ok(goodsList);

        return ok;
    }

    /**
     * 新增商品
     * 参数
     * 0：正常
     * 1：名字商品编号不能为空
     * 2：规格不能为空
     * 3：参数不能相同
     */
    @RequestMapping("create")
    public BaseRespVo addGoods(@RequestBody GoodsCreateBO goodsCreateBO) {
        int code = goodsService.addGoods(goodsCreateBO);
        if (code == 404) {
            return BaseRespVo.invalidData();
        }
        return BaseRespVo.ok("");
    }

    /**
     * 商品更新 TODO
     */
    @RequestMapping("update")
    public BaseRespVo updateGoods(@RequestBody GoodsDetailBO goodsDetailBO) {

        goodsService.updateGoods(goodsDetailBO);

        return BaseRespVo.ok("");
    }

    /**
     * 商品详情
     */
    @RequestMapping("detail")
    public BaseRespVo getDetail(Integer id) {

        GoodsDetailVO goodsDetailVO = goodsService.selectDetail(id);

        return BaseRespVo.ok(goodsDetailVO);
    }

    /**
     * 商品删除
     */

    @RequestMapping("delete")
    public BaseRespVo deleteGoods(@RequestBody Goods goods) {

        goodsService.deleteGoods(goods);

        return BaseRespVo.ok("");
    }

    /**
     * 显示商品类目品牌列表
     */
    @RequestMapping("catAndBrand")
    public BaseRespVo catAndBrand() {

        CatAndBrandVO catAndBrandVO = goodsService.catAndBrand();

        return BaseRespVo.ok(catAndBrandVO);
    }
}
