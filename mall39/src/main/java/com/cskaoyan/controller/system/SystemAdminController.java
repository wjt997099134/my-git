package com.cskaoyan.controller.system;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.SystemCreateAdminBO;
import com.cskaoyan.bean.bo.SystemDeleteAdminBO;
import com.cskaoyan.bean.bo.SystemUpdateAdminBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.SystemAdminVO;
import com.cskaoyan.bean.vo.SystemCreateAdminVO;
import com.cskaoyan.bean.vo.SystemUpdateAdminVO;
import com.cskaoyan.service.SystemService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统管理
 * 管理员管理模块
 *
 * @author WuHuaguo
 * @since 2022/06/04 16:52
 */

@RestController
@RequestMapping("admin/admin")
public class SystemAdminController {

    @Autowired
    SystemService systemService;

    @Autowired
    ConstantProperties CONSTANT;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam baseParam, String username) {
        SystemAdminVO systemAdminVO = systemService.adminList(baseParam, username);
        return BaseRespVo.ok(systemAdminVO);
    }

    @RequestMapping("create")
    public BaseRespVo create(@RequestBody SystemCreateAdminBO createAdminBO) {
        SystemCreateAdminVO createAdminVO = systemService.adminCreate(createAdminBO);

        if (createAdminVO == null) {
            // 管理员名字已存在，新增管理员失败
            return BaseRespVo.invalidParameter("管理员名字已存在！");
        }

        // 新增管理员成功
        return BaseRespVo.ok(createAdminVO);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody SystemUpdateAdminBO updateAdminBO) {
        SystemUpdateAdminVO updateAdminVO = systemService.adminUpdate(updateAdminBO);

        if (updateAdminVO == null) {
            // 管理员名字已存在，编辑管理员失败
            return BaseRespVo.invalidParameter("管理员名字已存在！");
        }

        // 编辑管理员成功
        return BaseRespVo.ok(updateAdminVO);
    }


    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody SystemDeleteAdminBO deleteAdminBO) {
        int code = systemService.adminDelete(deleteAdminBO);

        if (code == CONSTANT.getSuccessCode()) {
            // 删除管理员成功
            return BaseRespVo.ok();
        }
        // 删除管理员失败
        return BaseRespVo.invalidParameter("管理员不能删除自己账号");
    }

}
