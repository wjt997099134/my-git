package com.cskaoyan.controller.system;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.SystemCreateRoleBO;
import com.cskaoyan.bean.bo.SystemPermissionsBO;
import com.cskaoyan.bean.po.Role;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.service.SystemService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * 系统管理
 * 角色管理模块
 *
 * @author WuHuaguo
 * @since 2022/06/05 15:11
 */

@RestController
@RequestMapping("admin/role")
public class SystemRoleController {

    @Autowired
    ConstantProperties CONSTANT;

    @Autowired
    SystemService systemService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam baseParam, String name) {
        SystemRoleVO systemRoleVO = systemService.roleList(baseParam, name);
        return BaseRespVo.ok(systemRoleVO);
    }

    @RequestMapping("options")
    public BaseRespVo options() {
        SystemRoleOptionsVO optionsVO = systemService.roleOptions();
        return BaseRespVo.ok(optionsVO);
    }

    @RequestMapping("create")
    public BaseRespVo create(@RequestBody SystemCreateRoleBO createRoleBO) {
        RoleOfSystemRoleVO roleVO = systemService.roleCreate(createRoleBO);
        return BaseRespVo.ok(roleVO);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody Role role) {
        int code = systemService.roleUpdate(role);
        if (code == CONSTANT.getFailCode3()) {
            // 角色名称重复，修改失败
            return BaseRespVo.invalidParameter("角色名称已存在！");
        }
        // 修改成功
        return BaseRespVo.ok();
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Role role) {
        systemService.roleDelete(role);
        return BaseRespVo.ok();
    }

    @GetMapping("permissions")
    public BaseRespVo permissions(Integer roleId) {
        SystemPermissionsVO permissionsVO = systemService.rolePermissionsShow(roleId);
        return BaseRespVo.ok(permissionsVO);
    }

    @PostMapping("permissions")
    public BaseRespVo permissions(@RequestBody SystemPermissionsBO permissionsBO) {
        systemService.rolePermissionsAdd(permissionsBO);
        return BaseRespVo.ok();
    }

}
