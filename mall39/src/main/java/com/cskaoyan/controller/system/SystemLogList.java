package com.cskaoyan.controller.system;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.SystemLogListVO;
import com.cskaoyan.service.SystemService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 系统管理
 * 操作日志模块
 *
 * @author WuHuaguo
 * @since 2022/06/06 14:16
 */

@RestController
@RequestMapping("admin/log")
public class SystemLogList {

    @Autowired
    SystemService systemService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam baseParam) {
        SystemLogListVO logListVO = systemService.logList(baseParam);
        return BaseRespVo.ok(logListVO);
    }
}
