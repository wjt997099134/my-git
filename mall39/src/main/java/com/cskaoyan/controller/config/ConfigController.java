package com.cskaoyan.controller.config;

import com.cskaoyan.bean.vo.*;
import com.cskaoyan.service.ConfigService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @author karlfu
 * @since 2022/06/06 10:44
 */

@RestController
@RequestMapping("admin/config")
public class ConfigController {
    @Autowired
    ConfigService configService;

    @GetMapping("mall")
    public BaseRespVo showMallConfig() {
        MallConfigVO mallConfigVO = configService.queryMallConfig();

        return BaseRespVo.ok(mallConfigVO);
    }

    @GetMapping("express")
    public BaseRespVo showExpressConfig() {
        ExpressConfigVO expressConfigVO = configService.queryExpressConfig();

        return BaseRespVo.ok(expressConfigVO);
    }

    @GetMapping("order")
    public BaseRespVo showOrderConfig() {
        OrderConfigVO orderConfigVO = configService.queryOrderConfig();

        return BaseRespVo.ok(orderConfigVO);
    }

    @GetMapping("wx")
    public BaseRespVo showWxConfig() {
        WxConfigVO wxConfigVO = configService.queryWxConfig();

        return BaseRespVo.ok(wxConfigVO);
    }

    // 由于VO与BO的成员相同 以下均复用
    // Attention!!!
    @PostMapping("mall")
    public BaseRespVo updateMallConfig(@RequestBody MallConfigVO mallConfigBO) {

        try {
            Integer.parseInt(mallConfigBO.getMarket_mall_latitude());
            Integer.parseInt(mallConfigBO.getMarket_mall_longitude());
            Integer.parseInt(mallConfigBO.getMarket_mall_qq());
            Integer.parseInt(mallConfigBO.getMarket_mall_phone());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return BaseRespVo.invalidParameter("参数非法");
        }

        configService.updateMallConfig(mallConfigBO);

        return BaseRespVo.ok();
    }

    @PostMapping("express")
    public BaseRespVo updateExpressConfig(@RequestBody ExpressConfigVO expressConfigBO) {

        try {
            Integer.parseInt(expressConfigBO.getMarket_express_freight_min());
            Integer.parseInt(expressConfigBO.getMarket_express_freight_value());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return BaseRespVo.invalidParameter("参数非法");
        }

        configService.updateExpressConfig(expressConfigBO);

        return BaseRespVo.ok();
    }

    @PostMapping("order")
    public BaseRespVo updateOrderConfig(@RequestBody OrderConfigVO orderConfigBO) {

        try {
            Integer.parseInt(orderConfigBO.getMarket_order_comment());
            Integer.parseInt(orderConfigBO.getMarket_order_unconfirm());
            Integer.parseInt(orderConfigBO.getMarket_order_unpaid());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return BaseRespVo.invalidParameter("参数非法");
        }

        configService.updateOrderConfig(orderConfigBO);

        return BaseRespVo.ok();
    }

    @PostMapping("wx")
    public BaseRespVo updateWxConfig(@RequestBody WxConfigVO wxConfigBO) {

        try {
            Integer.parseInt(wxConfigBO.getMarket_wx_catlog_goods());
            Integer.parseInt(wxConfigBO.getMarket_wx_catlog_list());
            Integer.parseInt(wxConfigBO.getMarket_wx_index_brand());
            Integer.parseInt(wxConfigBO.getMarket_wx_index_hot());
            Integer.parseInt(wxConfigBO.getMarket_wx_index_new());
            Integer.parseInt(wxConfigBO.getMarket_wx_index_topic());
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return BaseRespVo.invalidParameter("参数非法");
        }

        configService.updateWxConfig(wxConfigBO);

        return BaseRespVo.ok();
    }
}
