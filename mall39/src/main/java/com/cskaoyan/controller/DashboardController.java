package com.cskaoyan.controller;

import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.DashboardData;
import com.cskaoyan.service.DashboardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 首页模块
 * 显示用户数量，商品数量，货品数量，订单数量
 * @author yemingfei
 * @since 2022/06/05 21:05
 */
@RestController
@RequestMapping("admin/dashboard")
public class DashboardController {

    @Autowired
    DashboardService dashboardService;
    @RequestMapping
    public BaseRespVo dashboard(){
        DashboardData dashboardData = dashboardService.dashboard();
        return BaseRespVo.ok(dashboardData);
    }
}
