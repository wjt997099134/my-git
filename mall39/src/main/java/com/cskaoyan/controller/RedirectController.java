package com.cskaoyan.controller;

import com.cskaoyan.bean.vo.BaseRespVo;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author yemingfei
 * @since 2022/06/07 20:38
 */
@RestController
public class RedirectController {
    @RequestMapping("redirect")
    public BaseRespVo redirect() {
        return BaseRespVo.unAuthc();
    }
}
