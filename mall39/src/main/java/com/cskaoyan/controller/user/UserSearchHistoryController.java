package com.cskaoyan.controller.user;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.user.AddressData;
import com.cskaoyan.bean.vo.user.SearchHistoryData;
import com.cskaoyan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户管理-搜索历史模块
 *
 * @author yemingfei
 * @since 2022/06/05 15:05
 */
@RestController
@RequestMapping("admin/history")
public class UserSearchHistoryController {

    @Autowired
    UserService userService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,Integer userId,String keyword){
        SearchHistoryData searchHistoryData = userService.searchHistoryList(param,userId,keyword);
        return BaseRespVo.ok(searchHistoryData);
    }
}
