package com.cskaoyan.controller.user;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.user.MemberData;
import com.cskaoyan.bean.vo.user.MemberDataListBean;
import com.cskaoyan.service.UserService;
import com.cskaoyan.util.ConstantProperties;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户管理-会员管理模块
 * @author yemingfei
 * @since 2022/06/05 15:05
 */
@RestController
@RequestMapping("admin/user")
public class UserMemberController {

    @Autowired
    UserService userService;
    @Autowired
    ConstantProperties CONSTANT;

    // @RequiresPermissions("admin:user:list")
    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,String username,Integer userId,String mobile){
        MemberData memberData = userService.memberList(param,username,userId,mobile);
        return BaseRespVo.ok(memberData);
    }


    @RequestMapping("detail")
    public BaseRespVo detail(Integer id){
        // id是否存在
        int code = userService.memberIdIsExist(id);
        // id不存在
        if (code == CONSTANT.getFailCode1()){
            return BaseRespVo.ok(null);
        }
        MemberDataListBean memberDataListBean = userService.memberDetail(id);
        return BaseRespVo.ok(memberDataListBean);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody MemberDataListBean memberDataListBean){
        userService.update(memberDataListBean);
        return BaseRespVo.ok(null);
    }
}
