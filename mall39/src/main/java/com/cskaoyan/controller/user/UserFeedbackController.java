package com.cskaoyan.controller.user;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.user.AddressData;
import com.cskaoyan.bean.vo.user.FeedbackData;
import com.cskaoyan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户管理-意见反馈模块
 *
 * @author yemingfei
 * @since 2022/06/05 15:05
 */
@RestController
@RequestMapping("admin/feedback")
public class UserFeedbackController {

    @Autowired
    UserService userService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,String username,Integer id){
        FeedbackData feedbackData = userService.feedbackList(param,username,id);
        return BaseRespVo.ok(feedbackData);
    }
}
