package com.cskaoyan.controller.user;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.user.AddressData;
import com.cskaoyan.bean.vo.user.MemberData;
import com.cskaoyan.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 用户管理-收货地址模块
 *
 * @author yemingfei
 * @since 2022/06/05 15:05
 */
@RestController
@RequestMapping("admin/address")
public class UserAddressController {

    @Autowired
    UserService userService;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,String name,Integer userId){
        AddressData addressData = userService.addressList(param,name,userId);
        return BaseRespVo.ok(addressData);
    }
}
