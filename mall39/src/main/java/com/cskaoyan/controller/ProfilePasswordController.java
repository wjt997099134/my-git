package com.cskaoyan.controller;

import com.cskaoyan.bean.bo.ProfilePasswordBo;
import com.cskaoyan.bean.po.Admin;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.config.shiro.MallToken;
import com.cskaoyan.service.SystemService;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * 首页-修改密码模块
 *
 * @author yemingfei
 * @since 2022/06/08 13:39
 */
@RestController
@RequestMapping("/admin/profile")
public class ProfilePasswordController {

    @Autowired
    SystemService systemService;
    @RequestMapping("password")
    public BaseRespVo profilePassword(@RequestBody ProfilePasswordBo profilePasswordBo) {
        Subject subject = SecurityUtils.getSubject();
        Admin primaryPrincipal = (Admin) subject.getPrincipals().getPrimaryPrincipal();
        String realPassword  = primaryPrincipal.getPassword();
        if(!realPassword.equals(Md5Util.getMD5(profilePasswordBo.getOldPassword()))){
            // 原密码输入错误
            return BaseRespVo.profilePassword();
        }
        // 两次密码输入不一样，前端不发请求
        // 修改密码 ，新密码和原密码相同也能修改成功
        if (profilePasswordBo.getNewPassword().equals(profilePasswordBo.getNewPassword2())){
            // 修改密码
            systemService.profilePassword(realPassword, Md5Util.getMD5(profilePasswordBo.getNewPassword()),primaryPrincipal.getId());
            // 修改密码后更新认证信息
            subject.login(new MallToken(primaryPrincipal.getUsername(), Md5Util.getMD5(profilePasswordBo.getNewPassword()), "admin"));
            return BaseRespVo.ok();
        }
        return BaseRespVo.invalidData("新密码两次输入不一致");

    }

    @RequestMapping("nnotice")
    public BaseRespVo nnotice(){
        return BaseRespVo.ok();
    }
}
