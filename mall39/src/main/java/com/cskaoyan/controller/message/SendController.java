package com.cskaoyan.controller.message;

import com.cskaoyan.bean.bo.PhoneBO;
import com.cskaoyan.bean.bo.RegisterBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.service.*;
import com.cskaoyan.util.CodeTimer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * @author tll
 * @create 2020/8/12 8:26
 * @Description:短信发送业务和注册业务
 * @CrossOrigin 跨域支持
 */
@RestController
@CrossOrigin
@RequestMapping("wx/auth")
public class SendController {

    @Autowired
    private SendService sendService;

    @Autowired
    UserService userService;
    @Autowired
    CodeTimer codeTimer;
    @Autowired
    CodeService codeService;

    /**
     * 短信发送业务
     */
    @RequestMapping("regCaptcha")
    public BaseRespVo SendMsg(@RequestBody PhoneBO phoneBO) {


        // 获取手机号
        String phone = phoneBO.getMobile();
        //生成随机二维码
        String code = UUID.randomUUID().toString().substring(0, 6);
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", code);

        // 将code处理为数组存入数据库
        codeService.codeHandle(phone, code);

        codeTimer.setPhone(phone);
        // Timer定时删除订单 不能手动
//        codeTimer.removeCode();

        // 短信发送
        boolean flag = sendService.sendMsg(phone, "SMS_173765187", map);
        if (flag) {
            return BaseRespVo.ok();
        } else {
            return BaseRespVo.invalidData("发送失败");
        }
    }

    /**
     * 注册用户
     */
    @RequestMapping("register")
    public BaseRespVo register(@RequestBody RegisterBO registerBO) {

        BaseRespVo respVO = userService.register(registerBO);

        return respVO;
    }
}