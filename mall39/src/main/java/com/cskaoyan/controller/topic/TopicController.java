package com.cskaoyan.controller.topic;

import com.cskaoyan.bean.bo.TopicBachDeleteBO;
import com.cskaoyan.bean.bo.TopicCreateBO;
import com.cskaoyan.bean.bo.TopicUpdateBO;
import com.cskaoyan.bean.po.Topic;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.TopicCreateVO;
import com.cskaoyan.bean.vo.TopicListVO;
import com.cskaoyan.bean.vo.TopicReadVO;
import com.cskaoyan.bean.vo.spread.BaseResp;
import com.cskaoyan.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * @ClassName: TopicController
 * @Description: 专题管理模块
 * @Author: zhanghaiyang
 * @Date: 2022/6/6 9:16
 */

@RestController
@RequestMapping("admin/topic")
public class TopicController {

    @Autowired
    TopicService topicService;

    @RequestMapping("list")
    public BaseRespVo list(@RequestParam Map requestMap) {
        // http://localhost:8083/admin/topic/list?page=1&limit=20&title=12&subtitle=123&sort=id&order=desc

        Integer page = Integer.parseInt((String) requestMap.get("page"));
        Integer limit = Integer.parseInt((String) requestMap.get("limit"));

        String title = null;
        String subtitle = null;

        if (requestMap.containsKey("title")) {
            if (!"".equals(requestMap.get("title"))) {
                title = (String) requestMap.get("title");
            }
        }

        if (requestMap.containsKey("subtitle")) {
            if (!"".equals(requestMap.get("subtitle"))) {
                subtitle = (String) requestMap.get("subtitle");
            }
        }

        // 排序的规则
        String sort = (String) requestMap.get("sort");
        // 排序的顺序：降序
        String order = (String) requestMap.get("order");

        TopicListVO topicListVO = topicService.getTopicList(page, limit, sort, order, title, subtitle);
        return BaseRespVo.ok(topicListVO);
    }

    @RequestMapping("create")
    public BaseRespVo create(@RequestBody TopicCreateBO topicCreateBO) {
        // 请求参数:{"goods":[],"title":"大概是点分公司发的","subtitle":"换个卡该回家了捡垃圾","picUrl":"http://localhost:8083/pic/d6ea00a7-2c74-45ed-b78a-eeb61ce45fd9.jpg","content":"<p>ldsml;agmdl;gdfl;mald;mvl;dmvkdjogjreoigjqe[rkg,l;fdamblarb616515631</p>","price":"20","readCount":"0"}
        Topic topic = topicService.createTopic(topicCreateBO);

        TopicCreateVO topicCreateVO = new TopicCreateVO(topic.getPicUrl(), topic.getAddTime(), topic.getPrice(), topic.getSubtitle(), topic.getGoods(), topic.getUpdateTime(), topic.getId(), topic.getTitle(), topic.getContent());
        // 响应：{"errno":0,"data":{"id":370,"title":"水电费撒地方","subtitle":"256165156","price":20,"picUrl":"http://182.92.235.201:8083/wx/storage/fetch/ahgyhi8fshddscke5rgb.jpg","goods":[1181012],"addTime":"2022-06-06 15:45:41","updateTime":"2022-06-06 15:45:41","content":"<p>是的发生的发生的发生的</p>"},"errmsg":"成功"}
        return BaseRespVo.ok(topicCreateVO);
    }

    @RequestMapping("read")
    public BaseRespVo topic(@RequestParam Map requestMap) {
        // 请求参数：http://182.92.235.201:8083/admin/topic/read?id=368
        Integer id = Integer.parseInt((String) requestMap.get("id"));

        // 响应参数：{"errno":0,"data":{"goodsList":[],"topic":{"id":369,"title":"阿达瓦","subtitle":"覆盖","price":213.00,"readCount":"1k","picUrl":"http://182.92.235.201:8083/wx/storage/fetch/4my6nrddmja6p77xkqwy.jpg","sortOrder":100,"goods":[1181181],"addTime":"2022-06-06 14:06:55","updateTime":"2022-06-06 14:06:55","deleted":false,"content":"<p>牛皮</p>"}},"errmsg":"成功"}
        TopicReadVO topicReadVO = topicService.getTopicInfo(id);
        return BaseRespVo.ok(topicReadVO);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody TopicUpdateBO topicUpdateBO) {
        Topic topic = topicService.updateTopic(topicUpdateBO);
        return BaseRespVo.ok(topic);
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Topic topic) {
        // 请求参数：{"id":318,"title":"HUAWEI MatePad Pro","subtitle":"12.6 英寸 OLED 全面屏，麒麟 9000 系列芯片，升级高效体验，突破创造边界。","price":5999,"readCount":"100000","picUrl":"http://localhost:8083/pic/652efa71-586f-485e-93bd-9f5a7538b621.jpg","sortOrder":0,"goods":[],"addTime":"2022-06-06 16:59:00","updateTime":"2022-06-06 16:59:00","deleted":false,"content":null}
        topicService.deleteTopic(topic);

        // 响应参数：{"errno":0,"errmsg":"成功"}
        return BaseRespVo.ok(null);
    }

    @RequestMapping("batch-delete")
    public BaseRespVo batchDelete(@RequestBody TopicBachDeleteBO topicBachDeleteBO) {
        // 请求参数：{"ids":[317,264]}
        topicService.batchDelete(topicBachDeleteBO);

        // 响应参数：{"errno":0,"errmsg":"成功"}
        return BaseRespVo.ok(null);
    }
}