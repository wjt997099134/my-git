// package com.cskaoyan.controller.shiro;
//
// import lombok.Data;
// import lombok.NoArgsConstructor;
// import org.apache.shiro.authc.UsernamePasswordToken;
//
// /**
//  * @ClassName: MallToken
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:23
//  */
//
// @Data
// @NoArgsConstructor
// public class MallToken extends UsernamePasswordToken {
//
//     String type;
//
//     public MallToken(String username, String password, String type) {
//         super(username, password);
//         this.type = type;
//     }
// }