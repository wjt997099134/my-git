// package com.cskaoyan.controller.shiro;
//
// import org.apache.shiro.authc.AuthenticationException;
// import org.apache.shiro.authc.AuthenticationInfo;
// import org.apache.shiro.authc.AuthenticationToken;
// import org.apache.shiro.authc.pam.ModularRealmAuthenticator;
// import org.apache.shiro.realm.Realm;
//
// import java.util.ArrayList;
// import java.util.Collection;
// import java.util.Iterator;
//
// /**
//  * @ClassName: MarketAuthenticator
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:41
//  */
//
// public class MarketAuthenticator extends ModularRealmAuthenticator {
//
//     @Override
//     protected AuthenticationInfo doAuthenticate(AuthenticationToken authenticationToken) throws AuthenticationException {
//         this.assertRealmsConfigured();
//         Collection<Realm> originRealms = this.getRealms();
//         // 如果认证器设置realm把customRealm、AdminRealm、wxRealm都添加上
//         ArrayList<Realm> realms = new ArrayList<>();
//         String type = ((MallToken) authenticationToken).getType();
//         Iterator<Realm> iterator = originRealms.iterator();
//         while (iterator.hasNext()) {
//             Realm realm = iterator.next();
//             if (realm.getName().toLowerCase().contains(type)) {
//                 realms.add(realm);
//             }
//         }
//
//         return realms.size() == 1 ? this.doSingleRealmAuthentication((Realm)realms.iterator().next(), authenticationToken) : this.doMultiRealmAuthentication(realms, authenticationToken);
//     }
// }