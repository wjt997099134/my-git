// package com.cskaoyan.controller.shiro;
//
// import org.apache.shiro.authc.AuthenticationException;
// import org.apache.shiro.authc.AuthenticationInfo;
// import org.apache.shiro.authc.AuthenticationToken;
// import org.apache.shiro.authz.AuthorizationInfo;
// import org.apache.shiro.realm.AuthorizingRealm;
// import org.apache.shiro.subject.PrincipalCollection;
// import org.springframework.stereotype.Component;
//
// /**
//  * @ClassName: WxRealm
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:46
//  */
//
// @Component
// public class WxRealm extends AuthorizingRealm {
//
//     @Override
//     protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//         return null;
//     }
//
//     @Override
//     protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
//         return null;
//     }
// }