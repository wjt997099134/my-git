// package com.cskaoyan.controller.shiro;
//
// import org.apache.shiro.realm.Realm;
// import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
// import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
// import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
// import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
// import org.springframework.context.annotation.Bean;
// import org.springframework.context.annotation.Configuration;
//
// import java.util.LinkedHashMap;
// import java.util.List;
//
// /**
//  * @ClassName: ShiroConfiguration
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:43
//  */
//
// @Configuration
// public class ShiroConfiguration {
//
//     // ShiroFilter → 依赖于SecurityManager
//     // ShiroFilterFactoryBean
//     @Bean
//     public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager securityManager) {
//         ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
//         shiroFilterFactoryBean.setSecurityManager(securityManager);
//
//         // 如果想要修改重定向的地址
//         // 将重定向url设置为匿名请求，并且响应认证失败的json数据
//         // shiroFilterFactoryBean.setLoginUrl("/redirect");
//
//         // shiro管理的内部的filter：anon、authc、perms
//         // Filter是谁, filter的作用范围是什么，filter的顺序是什么
//         // LinkedHashMap
//         LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
//         filterChainDefinitionMap.put("/redirect", "anon");
//         filterChainDefinitionMap.put("/admin/admin/list", "anon");
//         filterChainDefinitionMap.put("/admin/admin/add", "anon");
//         // 这个请求就是登陆请求 → 把当前用户（Subject）变为已经认证的状态
//         filterChainDefinitionMap.put("/admin/shiro/login", "anon");
//         filterChainDefinitionMap.put("/admin/shiro/info", "anon");
//         filterChainDefinitionMap.put("/admin/**", "authc");
//
//         // 权限是和URL绑定的，url绑定了Handler方法 → Handler方法和权限绑定
//         // filterChainDefinitionMap.put("/admin/user/list", "perms[admin:user:list]");
//
//         shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
//         return shiroFilterFactoryBean;
//     }
//
//     // SecurityManager → 依赖于realm、SessionManager、默认的认证器和授权器
//     @Bean
//     public DefaultWebSecurityManager securityManager(CustomRealm realm, DefaultWebSessionManager sessionManager, MarketAuthenticator authenticator) {
//         DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
//         // 如果没有使用setAuthenticator、没有使用setAuthorizer方法，使用的就是默认的认证器和授权器
//         // defaultWebSecurityManager.setAuthenticator();
//         // defaultWebSecurityManager.setAuthorizer();
//         defaultWebSecurityManager.setRealm(realm);
//         defaultWebSecurityManager.setAuthenticator(authenticator);
//
//         defaultWebSecurityManager.setSessionManager(sessionManager);
//         return defaultWebSecurityManager;
//     }
//
//     // 自定义认证器
//     @Bean
//     public MarketAuthenticator authenticator(List<Realm> realms) {
//         MarketAuthenticator marketAuthenticator = new MarketAuthenticator();
//         marketAuthenticator.setRealms(realms);
//         return marketAuthenticator;
//     }
//
//
//     // realms → extends AuthorizingRealm
//     // 可以通过@Bean，也可以直接注册为容器中的组件@Component
//     /*@Bean
//     public AuthorizingRealm realm() {
//         return new CustomRealm();
//     }*/
//
//
//     // SessionManager
//     // 前后端分离的应用 → 跨域 → Session会发生变化
//     // Shiro中的信息是通过Session来维护的，而如果session发生变化了，信息就拿不到了
//     @Bean
//     public DefaultWebSessionManager sessionManager() {
//
//         return new CustomSessionManager();
//     }
//
//     // 用到AspectJ → 使用注解的方式，将权限和url绑定起来
//     @Bean
//     public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
//         AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
//         authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
//         return authorizationAttributeSourceAdvisor;
//     }
// }