// package com.cskaoyan.controller.shiro;
//
// import com.cskaoyan.bean.po.Admin;
// import com.cskaoyan.bean.po.AdminExample;
// import com.cskaoyan.mapper.AdminMapper;
// import org.apache.shiro.authc.AuthenticationException;
// import org.apache.shiro.authc.AuthenticationInfo;
// import org.apache.shiro.authc.AuthenticationToken;
// import org.apache.shiro.authc.SimpleAuthenticationInfo;
// import org.apache.shiro.authz.AuthorizationInfo;
// import org.apache.shiro.authz.SimpleAuthorizationInfo;
// import org.apache.shiro.realm.AuthorizingRealm;
// import org.apache.shiro.subject.PrincipalCollection;
// import org.springframework.beans.factory.annotation.Autowired;
// import org.springframework.stereotype.Component;
//
// import java.util.Arrays;
// import java.util.List;
//
// /**
//  * @ClassName: CustomRealm
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:19
//  */
//
// @Component
// public class CustomRealm extends AuthorizingRealm {
//
//     @Autowired
//     AdminMapper adminMapper;
//
//     // 用户权限信息
//     @Override
//     protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
//         // 获得principal信息 → 根据principal信息获得对应的权限信息
//         // 上面的doGetAuthorizationInfo中的第一个参数
//         Admin primaryPrincipal = (Admin) principalCollection.getPrimaryPrincipal();
//
//         // 根据Admin查询它对应的权限
//         // 拿到用户信息，可以获得role角色信息，根据角色信息，拿到权限信息
//         List<String> permissions = getPermissionsByAdminId(primaryPrincipal.getId());
//
//         SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
//         simpleAuthorizationInfo.addStringPermissions(permissions);
//         return simpleAuthorizationInfo;
//     }
//
//     public List<String> getPermissionsByAdminId(Integer id) {
//
//         // 应该来源于数据库
//         AdminExample example = new AdminExample();
//         example.createCriteria().andIdEqualTo(id);
//         List<Admin> admins = adminMapper.selectByExample(example);
//         if (admins != null && admins.size() == 1) {
//             return Arrays.asList(admins.get(0).getPassword());
//         }
//
//         return null;
//     }
//
//     // 用户认证信息
//     @Override
//     protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
//         // 根据subject.login传入的authenticationToken获得认证信息
//         // 根据token中传入的username来查询该用户在系统中正确的密码（Credentials）
//         String type = ((MallToken) authenticationToken).getType();
//         String username = (String) authenticationToken.getPrincipal();
//
//         // 根据用户名从数据库查询用户在系统中的密码
//         AdminExample example = new AdminExample();
//         example.createCriteria().andUsernameEqualTo(username);
//         List<Admin> marketAdmins = adminMapper.selectByExample(example);
//         if (marketAdmins != null && marketAdmins.size() == 1) {
//             Admin admin = marketAdmins.get(0);
//             String credentials = admin.getPassword();
//             // 第一个参数：principal信息 → 放入啥信息都行，取决于开发人员 → 你放入的是什么信息，你取出的就是什么信息
//             return new SimpleAuthenticationInfo(admin, credentials, this.getName());
//         }
//
//         return null;
//     }
// }