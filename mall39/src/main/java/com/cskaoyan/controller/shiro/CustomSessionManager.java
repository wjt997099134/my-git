// package com.cskaoyan.controller.shiro;
//
// import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
//
// import javax.servlet.ServletRequest;
// import javax.servlet.ServletResponse;
// import javax.servlet.http.HttpServletRequest;
// import java.io.Serializable;
//
// /**
//  * @ClassName: CustomSessionManager
//  * @Description:
//  * @Author: 瞳
//  * @Date: 2022/6/7 10:36
//  */
//
// public class CustomSessionManager extends DefaultWebSessionManager {
//     private static final String HEADER = "X-CskaoyanMarket-Admin-Token";
//     private static final String WX_HEADER = "X-CskaoyanMarket-Token";
//
//     @Override
//     protected Serializable getSessionId(ServletRequest srequest, ServletResponse response) {
//         HttpServletRequest request = (HttpServletRequest) srequest;
//
//         String sessionId = request.getHeader(HEADER);
//         if (sessionId != null && !"".equals(sessionId)) {
//             return sessionId;
//         }
//
//         String sessionId2 = request.getHeader(WX_HEADER);
//         if (sessionId2 != null && !"".equals(sessionId2)) {
//             return sessionId2;
//         }
//
//         return super.getSessionId(srequest, response);
//     }
// }