package com.cskaoyan.controller.market;


import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.BrandCreateBo;
import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.BrandData;
import com.cskaoyan.service.MarketService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("admin/brand")
public class MarketBrandController {
    @Autowired
    MarketService marketService;
    @Autowired
    ConstantProperties CONSTANT;

    @RequestMapping("list")
    public BaseRespVo list(BaseParam param,Integer id,String name) {
        BrandData brandData = marketService.brandList(param,id,name);
        return BaseRespVo.ok(brandData);
    }
    /**
     * 删除品牌商，旗下所有产品brand_id 置为0
     * @author wangjiting
     * @date 2022/6/5 10:25
     * @param brand
     * @return BaseRespVo
     */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody Brand brand) {
        marketService.brandDelete(brand);
        return BaseRespVo.ok(null);
    }
    /**
     * 创建品牌商
     * 不能同名，先查询是否有已删除的可用的数据
     * @author wangjiting
     * @date 2022/6/5 15:30
     * @param brandCreateBo
     * @return BaseRespVo
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody BrandCreateBo brandCreateBo) {
        int code = marketService.brandCreate(brandCreateBo);
        if (code == CONSTANT.getFailCode1()) {
            return BaseRespVo.invalidParameter("已有同名品牌商，请不要重复创建!");
        }
        return BaseRespVo.ok(null);
    }
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody Brand brand){
        Brand result = marketService.brandUpdate(brand);
        if (result == null) {
            return BaseRespVo.invalidParameter("品牌商不可同名!");
        }
        return BaseRespVo.ok(result);
    }

}
