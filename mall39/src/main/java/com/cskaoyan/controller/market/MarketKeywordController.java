package com.cskaoyan.controller.market;

import com.cskaoyan.bean.bo.KeywordBO;
import com.cskaoyan.bean.bo.KeywordDetailBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.KeywordVO;
import com.cskaoyan.bean.vo.KeywordsListVO;
import com.cskaoyan.service.MarketKeywordServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @param
 * @Date: 2022/6/4 16:29
 * @Description:商场关键词模块 Doing
 * @Author: 李宇浩
 * @return
 */
@RestController
@RequestMapping("admin/keyword")
public class MarketKeywordController {

    @Autowired
    MarketKeywordServiceImpl marketKeywordService;

    /**
     * 关键字列表展示
     */
    @RequestMapping("list")
    public BaseRespVo listKeywords(Integer page, Integer limit, String keyword, String url, String sort, String order) {

        KeywordsListVO keywordsListVO = marketKeywordService.querryKeywords(page, limit, keyword, url, sort, order);

        return BaseRespVo.ok(keywordsListVO);
    }

    /**
     * 关键字新增
     */
    @RequestMapping("create")
    public BaseRespVo createKeyword(@RequestBody KeywordBO keywordBO) {

        KeywordVO keywordVO = marketKeywordService.addKeyword(keywordBO);

        return BaseRespVo.ok(keywordVO);
    }

    /**
     * 关键字更新
     */
    @RequestMapping("update")
    public BaseRespVo updateKeyword(@RequestBody KeywordDetailBO keywordDetailBO) {

        KeywordVO keywordVO = marketKeywordService.updateKeyword(keywordDetailBO);

        return BaseRespVo.ok(keywordVO);
    }

    /**
     * 关键字删除
     */
    @RequestMapping("delete")
    public BaseRespVo deleteKeyword(@RequestBody KeywordDetailBO keywordDetailBO) {

        marketKeywordService.delete(keywordDetailBO);

        return BaseRespVo.ok(" ");
    }

}
