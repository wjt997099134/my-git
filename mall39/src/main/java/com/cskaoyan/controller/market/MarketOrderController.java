package com.cskaoyan.controller.market;


import com.cskaoyan.bean.bo.*;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.ChannelData;
import com.cskaoyan.bean.vo.OrderData;
import com.cskaoyan.bean.vo.OrderDetailData;
import com.cskaoyan.service.MarketService;
import com.cskaoyan.util.ConstantProperties;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("admin/order")
public class MarketOrderController {

    @Autowired
    MarketService marketService;
    @Autowired
    ConstantProperties CONSTANT;

    /**
     * 根据条件查询订单，均为精确查询
     *
     * @param param
     * @param orderListPostBo
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/5 20:12
     */
    @RequestMapping("list")
    public BaseRespVo list(BaseParam param, OrderListPostBo orderListPostBo) {
        OrderData orderData = marketService.orderList(param, orderListPostBo);
        return BaseRespVo.ok(orderData);
    }

    /**
     * 根据ID查询订单详情
     *
     * @param id
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/5 20:13
     */
    @RequestMapping("detail")
    public BaseRespVo detail(Integer id) {
        OrderDetailData orderDetailData = marketService.orderDetail(id);
        return BaseRespVo.ok(orderDetailData);
    }

    /**
     * 订单不能删除
     *
     * @param id
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/5 22:08
     */
    @RequestMapping("delete")
    public BaseRespVo delete(Integer id) {
        return BaseRespVo.invalidParameter("订单不能删除!");
    }

    /**
     * 快递公司信息
     *
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/5 22:13
     */
    @RequestMapping("channel")
    public BaseRespVo channel() {
        List<ChannelData> channelDataList = marketService.channelList();
        return BaseRespVo.ok(channelDataList);
    }

    /**
     * 订单发货
     *
     * @param shipDataBo
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/5 22:34
     */
    @RequestMapping("ship")
    public BaseRespVo ship(@RequestBody ShipDataBo shipDataBo) {
        marketService.ship(shipDataBo);
        return BaseRespVo.ok(null);
    }

    @RequestMapping("refund")
    public BaseRespVo refund(@RequestBody RefundDataBo refundDataBo) {
        marketService.refund(refundDataBo);
        return BaseRespVo.ok(null);
    }

    /*
     * @param null:
     * @return: null
     * @author: zhanghaiyang
     * @date: 2022/06/06 20:25
     * @description:
     */
    @RequestMapping("reply")
    public BaseRespVo reply(@RequestBody AdminReplyBO adminReplyBO) {
        // 成功为1，失败为0
        Integer hasComment = marketService.reply(adminReplyBO);

        if (hasComment == 0) {
            return BaseRespVo.invalidParameter("订单商品已回复！");
        } else {
            return BaseRespVo.ok(null);
        }
    }
}
