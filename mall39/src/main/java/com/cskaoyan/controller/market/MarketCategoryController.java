package com.cskaoyan.controller.market;


import com.cskaoyan.bean.bo.CategoryCreateBo;
import com.cskaoyan.bean.bo.CategoryUpdateBo;
import com.cskaoyan.bean.po.Category;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.CategoryData;
import com.cskaoyan.bean.vo.CategoryDataListBean;
import com.cskaoyan.bean.vo.FatherCategory;
import com.cskaoyan.service.MarketService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商场管理商品类目模块
 *
 * @author wangjiting
 * @date 2022/6/2 23:09
 */

@RestController
@RequestMapping("admin/category")
public class MarketCategoryController {

    @Autowired
    MarketService marketService;
    @Autowired
    ConstantProperties CONSTANT;

    /**
     * 回显商品类别
     *
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/4 17:56
     */
    @RequestMapping("list")
    public BaseRespVo list() {
        CategoryData categoryData = marketService.categoryList();
        return BaseRespVo.ok(categoryData);
    }

    @RequestMapping("l1")
    public BaseRespVo l1List() {
        FatherCategory fatherCategory = marketService.fatherCategoryList();
        return BaseRespVo.ok(fatherCategory);
    }

    /**
     * 新建商品类别
     *判断是否有可用的已删除的类目信息并使用
     * @param categoryCreateBo
     * @return BaseRespVo
     * @author wangjiting
     * @date 2022/6/4 17:58
     */
    @RequestMapping("create")
    public BaseRespVo create(@RequestBody CategoryCreateBo categoryCreateBo) {
        if (CONSTANT.getCategoryFatherPid().equals(categoryCreateBo.getPid()) && CONSTANT.getCategorySonLevel().equals(categoryCreateBo.getLevel())) {
            return BaseRespVo.invalidParameter("参数不合法,新建失败!");
        }
        Category category = marketService.categoryCreate(categoryCreateBo);
        if (category == null) {
            return BaseRespVo.invalidParameter("一级类目不能同名!");
        }
        return BaseRespVo.ok(category);
    }
/**
 * 删除类目，delete置为true，保留配置信息
 * @author wangjiting
 * @date 2022/6/4 22:34
 * @param categoryDataListBean
 * @return BaseRespVo
 */
    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody CategoryDataListBean categoryDataListBean){
        marketService.categoryDelete(categoryDataListBean);
        return BaseRespVo.ok(null);
    }
    @RequestMapping("update")
    public BaseRespVo update(@RequestBody CategoryUpdateBo categoryUpdateBo){
        int code = marketService.categoryUpdate(categoryUpdateBo);
        if (code == CONSTANT.getFailCode1()) {
            return BaseRespVo.invalidParameter("有子类不可以修改类目级别!");
        }
        if (code == CONSTANT.getFailCode2()) {
            return BaseRespVo.invalidParameter("参数不合法!不可以做自己的子类!");
        }
        if (code == CONSTANT.getFailCode3()) {
            return BaseRespVo.invalidParameter("一级类目不能同名!");
        }
        return BaseRespVo.ok(null);
    }
}
