package com.cskaoyan.controller.market;


import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.RegionData;
import com.cskaoyan.service.MarketService;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 商场行政区域
 *
 * @author wangjiting
 * @date 2022/6/5 0:49
 */
@RestController
@RequestMapping("admin/region")
public class MarketRegionController {

    @Autowired
    MarketService marketService;
    @Autowired
    ConstantProperties CONSTANT;


    @RequestMapping("list")
    public BaseRespVo list() {
        RegionData regionData = marketService.regionList();
        return BaseRespVo.ok(regionData);
    }
}
