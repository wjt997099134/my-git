package com.cskaoyan.controller.market;


import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.IssueBO;
import com.cskaoyan.bean.bo.IssueDetailBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.IssueVO;
import com.cskaoyan.bean.vo.MarketIssueVO;
import com.cskaoyan.service.MarketIssueService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @param
 * @Date: 2022/6/4 16:29
 * @Description:商场通用管理 TODO
 * @Author: 李宇浩
 * @return
 */
@RestController
@RequestMapping("admin/issue")
public class MarketIssueController {

    @Autowired
    MarketIssueService issueService;

    @RequestMapping("list")
    public BaseRespVo list(Integer page, Integer limit, String question, String sort, String order) {
        MarketIssueVO marketIssueVO = issueService.querryList(page, limit, question, sort, order);

        return BaseRespVo.ok(marketIssueVO);
    }

    @RequestMapping("create")
    public BaseRespVo create(@RequestBody IssueBO issueBO) {

        String question = issueBO.getQuestion();
        String answer = issueBO.getAnswer();
        IssueVO issueVO = issueService.create(question, answer);

        return BaseRespVo.ok(issueVO);
    }

    @RequestMapping("update")
    public BaseRespVo update(@RequestBody IssueDetailBO issueDetailBO) {

        IssueVO issueVO = issueService.update(issueDetailBO);

        return BaseRespVo.ok(issueVO);
    }

    @RequestMapping("delete")
    public BaseRespVo delete(@RequestBody IssueDetailBO issueDetailBO) {

        issueService.delete(issueDetailBO);

        return BaseRespVo.ok();
    }
}
