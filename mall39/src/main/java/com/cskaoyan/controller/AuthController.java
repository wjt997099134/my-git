package com.cskaoyan.controller;

import com.cskaoyan.bean.bo.AdminInfoBean;
import com.cskaoyan.bean.po.Admin;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.InfoData;
import com.cskaoyan.bean.vo.LoginUserData;
import com.cskaoyan.config.shiro.MallToken;
import com.cskaoyan.service.SystemService;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//shiro整合之后，在做具体的开发
//响应结果都是JSON，Controller组件上我们都用@RestController注解
@RestController
@RequestMapping("admin/auth")
public class AuthController {
    @Autowired
    SystemService systemService;

    /**
     * 如果参数比较少，类型比较简单的话，使用map来接收也可以
     */
    @RequestMapping("login")
    public BaseRespVo login(@RequestBody Map map) {
        // 登录输入的用户名和密码
        String username = (String) map.get("username");
        String password = (String) map.get("password");
        // 将当前用户变为登录状态
        // 获得subject,固定写法
        Subject subject = SecurityUtils.getSubject();
        // 用token进行登录
        // 执行到realm
        try {
            subject.login(new MallToken(username, Md5Util.getMD5(password), "admin"));
        } catch (Exception e){
            return BaseRespVo.invalidParameter("用户名或密码错误");
        }
        // 登录之后可以通过subject获得信息
        Admin primaryPrincipal = ((Admin) subject.getPrincipals().getPrimaryPrincipal());

        LoginUserData loginUserData = new LoginUserData();
        loginUserData.setAdminInfo(new AdminInfoBean(primaryPrincipal.getUsername(), primaryPrincipal.getAvatar()));
        loginUserData.setToken(((String) subject.getSession().getId()));
        return BaseRespVo.ok(loginUserData);
    }

    @RequestMapping("info")
    public BaseRespVo info(String token) {
        Subject subject = SecurityUtils.getSubject();
        Admin primaryPrincipal = (Admin) subject.getPrincipals().getPrimaryPrincipal();
        //根据primaryPrincipal做查询
        // admin--rolesId
        // roleId-->角色名
        // roleId-->perms
        Integer[] roleIds = primaryPrincipal.getRoleIds();
        List<String> roles = new ArrayList<>();
        List<String> permUrls = new ArrayList<>();
        if (roleIds == null || roleIds.length == 0){
            permUrls = null;
        } else {
            for (Integer roleId : roleIds) {
                // 根据角色id查询角色名
                String role = systemService.adminRoles(roleId);
                // 根据角色id查询对应的权限url,超级管理员 permUrls = *
                List<String> permsOfRoleId = systemService.adminPerms(roleId);
                // 该角色下的所有权限对应的所有url
                List<String> urls = new ArrayList<>();
                for (int i = 0; i < permsOfRoleId.size(); i++) {
                    // 根据权限名查询对应url
                    String permUrl = systemService.adminPermUrl(permsOfRoleId.get(i));
                    urls.add(permUrl);
                }
                roles.add(role);
                permUrls.addAll(urls);
            }
        }

        InfoData infoData = new InfoData(
                primaryPrincipal.getUsername(),
                primaryPrincipal.getAvatar(),
                roles,permUrls
        );
        return BaseRespVo.ok(infoData);
    }

    @RequestMapping("logout")
    public BaseRespVo logout() {
        return BaseRespVo.ok(null);
    }
}
