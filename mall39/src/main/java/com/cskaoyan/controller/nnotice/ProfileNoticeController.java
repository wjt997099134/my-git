package com.cskaoyan.controller.nnotice;

import com.cskaoyan.bean.vo.BaseRespVo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/** 解决烦人的红色弹窗提示
 * @author karlfu
 * @since 2022/06/08 16:34
 */

@RestController
@RequestMapping("/admin/profile/")
public class ProfileNoticeController {

    @GetMapping("nnotice")
    public BaseRespVo profileNotice() {
        // 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        boolean authenticated = subject.isAuthenticated();
        if (!authenticated) {
            return BaseRespVo.invalidData("未登录或登录凭据已失效");
        }
        return BaseRespVo.ok();
    }
}
