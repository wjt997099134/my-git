package com.cskaoyan.controller.stat;

import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.GoodsStatVO;
import com.cskaoyan.bean.vo.OrderStatVO;
import com.cskaoyan.bean.vo.UserStatVO;
import com.cskaoyan.service.StatService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author karlfu
 * @since 2022/06/06 21:48
 */

@RestController
@RequestMapping("admin/stat")
public class StatController {
    @Autowired
    StatService statService;

    @GetMapping("user")
    public BaseRespVo showUserStat() {
        UserStatVO userStatVO = statService.queryUserStat();
        return BaseRespVo.ok(userStatVO);
    }

    @GetMapping("order")
    public BaseRespVo showOrderStat() {
        OrderStatVO orderStatVO = statService.queryOrderStat();
        return BaseRespVo.ok(orderStatVO);
    }

    // TODO
    @GetMapping("goods")
    public BaseRespVo showGoodsStat() {
        GoodsStatVO goodsStatVO = statService.queryGoodsStat();
        return BaseRespVo.ok(goodsStatVO);
    }
}
