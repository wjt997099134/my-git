package com.cskaoyan.bean;

import lombok.Data;

@Data
public class GoodsListBaseParam {
    private Boolean isNew;
    private Boolean isHot;
    private Integer brandId;
    private String keyword;
    private Integer categoryId;
    private Integer page;
    private Integer limit;
    private String sort;
    private String order;
}
