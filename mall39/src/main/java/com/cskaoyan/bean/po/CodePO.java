package com.cskaoyan.bean.po;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/9 9:51
 * @Description:验证码数据库交互
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@AllArgsConstructor
@Data
public class CodePO {
    Integer id;
    String phone;
    String[] code;
}
