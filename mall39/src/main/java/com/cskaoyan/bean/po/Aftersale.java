package com.cskaoyan.bean.po;

import java.math.BigDecimal;
import java.util.Date;

public class Aftersale {
    private Integer id;

    private String aftersaleSn;

    private Integer orderId;

    private Integer userId;

    private Short type;

    private String reason;

    private BigDecimal amount;

    private String[] pictures;

    private String comment;

    private Short status;

    private Date handleTime;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    public Aftersale() {
    }

    public Aftersale(Integer id, String aftersaleSn, Integer orderId, Integer userId, Short type, String reason, BigDecimal amount, String[] pictures, String comment, Short status, Date handleTime, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.aftersaleSn = aftersaleSn;
        this.orderId = orderId;
        this.userId = userId;
        this.type = type;
        this.reason = reason;
        this.amount = amount;
        this.pictures = pictures;
        this.comment = comment;
        this.status = status;
        this.handleTime = handleTime;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAftersaleSn() {
        return aftersaleSn;
    }

    public void setAftersaleSn(String aftersaleSn) {
        this.aftersaleSn = aftersaleSn == null ? null : aftersaleSn.trim();
    }

    public Integer getOrderId() {
        return orderId;
    }

    public void setOrderId(Integer orderId) {
        this.orderId = orderId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Short getType() {
        return type;
    }

    public void setType(Short type) {
        this.type = type;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason == null ? null : reason.trim();
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public String[] getPictures() {
        return pictures;
    }

    public void setPictures(String[] pictures) {
        this.pictures = pictures;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment == null ? null : comment.trim();
    }

    public Short getStatus() {
        return status;
    }

    public void setStatus(Short status) {
        this.status = status;
    }

    public Date getHandleTime() {
        return handleTime;
    }

    public void setHandleTime(Date handleTime) {
        this.handleTime = handleTime;
    }

    public Date getAddTime() {
        return addTime;
    }

    public void setAddTime(Date addTime) {
        this.addTime = addTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }
}