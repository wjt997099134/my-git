package com.cskaoyan.bean.po;

import lombok.Data;

/**
 * @author WuHuaguo
 * @since 2022/06/07 10:54
 */

@Data
public class SystemPermissions {
    Integer id;
    Integer pid;
    String label;
    Integer type;
    String api;
    String permission;
}

 