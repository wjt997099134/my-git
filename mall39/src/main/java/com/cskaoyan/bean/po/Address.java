package com.cskaoyan.bean.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
@Data
public class Address {
    private Integer id;

    private String name;

    private Integer userId;

    private String province;

    private String city;

    private String county;

    private String addressDetail;

    private String areaCode;

    private String postalCode;

    private String tel;

    private Boolean isDefault;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;

    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean deleted;

    public Address(Integer id, String name, Integer userId, String province, String city, String county, String addressDetail, String areaCode, String postalCode, String tel, Boolean isDefault, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.province = province;
        this.city = city;
        this.county = county;
        this.addressDetail = addressDetail;
        this.areaCode = areaCode;
        this.postalCode = postalCode;
        this.tel = tel;
        this.isDefault = isDefault;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}