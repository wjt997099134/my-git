package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 地址删除时POST的参数
 *
 * @author yemingfei
 * @since 2022/06/08 19:50
 */
@Data
public class AddressDeleteData {
    private Integer id;
}
