package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 取消收藏POST参数
 *
 * @author yemingfei
 * @since 2022/06/08 22:29
 */

@Data
public class WxCollectAddordeleteBo {

    /**
     * valueId : 1109008
     * type : 0
     */
    private Integer valueId;
    private Byte type;


}
