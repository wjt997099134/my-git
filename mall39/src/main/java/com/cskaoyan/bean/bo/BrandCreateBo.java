package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class BrandCreateBo {
    //{"name":"xiaomi制造商","desc":"快TM买小米","floorPrice":"10000","picUrl":"http://182.92.235.201:8083/wx/storage/fetch/wlg95ahbfbd6b1hsff9f.jfif"}

    private String name;
    private String desc;
    private BigDecimal floorPrice;
    private String picUrl;
}
