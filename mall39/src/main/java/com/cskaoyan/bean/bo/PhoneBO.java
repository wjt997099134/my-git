package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

/**
 * @param
 * @Date: 2022/6/8 20:38
 * @Description:验证码发送模块手机号接收窗口
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class PhoneBO {
    private String mobile;
}
