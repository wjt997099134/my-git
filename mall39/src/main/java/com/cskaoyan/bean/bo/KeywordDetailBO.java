package com.cskaoyan.bean.bo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonValue;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 20:41
 * @Description:关键字详情更新接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class KeywordDetailBO {
    private boolean deleted;
    @JsonProperty("isDefault")
    private boolean isDefault;
    @JsonProperty("default")
    private boolean def;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private int id;
    private String keyword;
    private String url;
    @JsonProperty("isHot")
    private boolean isHot;
    private short sortOrder;
}
