package com.cskaoyan.bean.bo;


import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;

@Data
public class OrderListPostBo {
    //timeArray=&orderStatusArray=101&sort=add_time&order=desc&orderId=&start=&end=&userId=&orderSn=
    //orderStatusArray
    private ArrayList<Short> orderStatusArray;
    private Integer userId;
    private String orderSn;
    private Integer orderId;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date start;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date end;
}
