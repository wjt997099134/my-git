package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 微信小程序订单模块
 * wx/order/refund 的BO
 *
 * @author WuHuaguo
 * @since 2022/06/08 11:01
 */

@Data
public class WxOrderIdBO {
    Integer orderId;
}
