package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @ClassName: TopicBachDeleteBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 22:06
 */

@Data
public class TopicBachDeleteBO {

    /**
     * ids : [317,264]
     */
    private Integer[] ids;
}