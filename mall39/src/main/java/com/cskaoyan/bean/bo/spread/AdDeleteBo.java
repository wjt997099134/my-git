package com.cskaoyan.bean.bo.spread;

import lombok.Data;

/**
 * @author CR
 * @since 2022/06/05 19:38
 */
@Data
public class AdDeleteBo {

    /**
     * addTime : 2022-06-05 19:37:07
     * name : 1
     * updateTime : 2022-06-05 19:37:07
     * id : 71
     * position : 1
     * url : http://182.92.235.201:8083/wx/storage/fetch/tqmelhuqajgjd1yyuy2f.jpg
     * content : 1
     * enabled : true
     */
    private String addTime;
    private String name;
    private String updateTime;
    private Integer id;
    private Integer position;
    private String url;
    private String content;
    private boolean enabled;

}
