package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 系统管理
 * 删除管理员
 * admin/admin/delete 的BO
 *
 * @author WuHuaguo
 * @since 2022/06/05 10:35
 */

@Data
public class SystemDeleteAdminBO {
    Integer id;
    String username;
    String avatar;
    Integer[] roleIds;
}
