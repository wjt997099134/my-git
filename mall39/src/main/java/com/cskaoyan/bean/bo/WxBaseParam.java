package com.cskaoyan.bean.bo;

import lombok.Data;


@Data
public class WxBaseParam {
    Integer page;
    Integer limit;
    Integer showType;
}
