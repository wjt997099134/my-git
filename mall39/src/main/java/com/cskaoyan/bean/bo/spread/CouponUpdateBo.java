package com.cskaoyan.bean.bo.spread;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * @author CR
 * @since 2022/06/06 14:17
 */
@Data
public class CouponUpdateBo {

    /**
     * addTime : 2022-05-10 17:15:22
     * discount : 0
     * timeType : 0
     * goodsValue : []
     * updateTime : 2022-05-10 17:47:20
     * type : 0
     * goodsType : 0
     * total : 10
     * min : 0
     * deleted : false
     * name : test
     * limit : 1
     * days : 10
     * daysType : 1
     * startTime : 2022-05-10 00:00:00
     * id : 89
     * tag :
     * endTime : 2022-05-09 00:00:00
     * desc :
     * status : 1
     */
    private Date addTime;
    private Integer discount;
    private Integer timeType;
    private List<?> goodsValue;
    private String updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private boolean deleted;
    private String name;
    private Integer limit;
    private Integer days;
    private Integer daysType;
    private Date startTime;
    private Integer id;
    private String tag;
    private Date endTime;
    private String desc;
    private Integer status;

}
