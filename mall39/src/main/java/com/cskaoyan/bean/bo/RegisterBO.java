package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class RegisterBO {
    private String username;
    private String password;
    private String mobile;
    // 验证码
    private String code;
    // sessionKey
    private String wxCode;
}
