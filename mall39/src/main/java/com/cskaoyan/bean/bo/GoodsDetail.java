package com.cskaoyan.bean.bo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/5 20:23
 * @Description:新增商品传参的商品细节
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsDetail {
    private String goodsSn;
    private String name;
    private Integer categoryId;
    private Integer brandId;
    private String[] gallery;
    private String keywords;
    private String brief;
    private Boolean isOnSale;
    private String picUrl;
    private Boolean isNew;
    private Boolean isHot;
    private String unit;
    private Double counterPrice;
    private String detail;
}
