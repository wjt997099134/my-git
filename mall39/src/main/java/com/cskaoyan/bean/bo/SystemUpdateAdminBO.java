package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 系统管理
 * 编辑管理员模块BO
 *
 * @author WuHuaguo
 * @since 2022/06/06 21:21
 */

@Data
public class SystemUpdateAdminBO {
    Integer id;
    String username;
    String password;
    String avatar;
    Integer[] roleIds;
}
