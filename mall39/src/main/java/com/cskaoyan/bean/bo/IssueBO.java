package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class IssueBO {
    private String question;
    private String answer;
}
