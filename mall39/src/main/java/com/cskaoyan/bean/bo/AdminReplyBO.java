package com.cskaoyan.bean.bo;

/**
 * @ClassName: AdminReplyBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 20:26
 */

public class AdminReplyBO {

    /**
     * commentId : 1072
     * content : 666
     */
    private Integer commentId;
    private String content;

    public void setCommentId(Integer commentId) {
        this.commentId = commentId;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Integer getCommentId() {
        return commentId;
    }

    public String getContent() {
        return content;
    }
}