package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 系统管理
 * 角色管理
 * admin/admin/permissions 的BO
 *
 * @author WuHuaguo
 * @since 2022/06/07 09:21
 */

@Data
public class SystemPermissionsBO {
    Integer roleId;
    String[] permissions;
}
