package com.cskaoyan.bean.bo.cartbo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class CartCreateBO {

    private short number;
    private int productId;
    private int goodsId;

}
