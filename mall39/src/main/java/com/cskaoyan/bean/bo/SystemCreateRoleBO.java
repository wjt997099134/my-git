package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 系统管理
 * 角色管理模块新增角色的BO
 *
 * @author WuHuaguo
 * @since 2022/06/05 16:18
 */

@Data
public class SystemCreateRoleBO {
    String name;
    String desc;
}

 