package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * @ClassName: WxCommentPostBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 21:30
 */

@Data
public class WxCommentPostBO {
    private String content;
    private Boolean hasPicture;
    private String[] picUrls;
    private Short star;
    private Byte type;
    private Integer valueId;
}