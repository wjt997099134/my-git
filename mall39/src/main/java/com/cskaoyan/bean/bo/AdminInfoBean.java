package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class AdminInfoBean {
    /**
     * nickName : admin123
     * avatar : https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif
     */

    private String nickName;
    private String avatar;

    public AdminInfoBean(String nickName, String avatar) {
        this.nickName = nickName;
        this.avatar = avatar;
    }
}
