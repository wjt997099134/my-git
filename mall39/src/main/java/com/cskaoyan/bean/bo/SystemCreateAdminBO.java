package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 系统管理
 * 新增管理员的BO
 *
 * @author WuHuaguo
 * @since 2022/06/06 20:30
 */

@Data
public class SystemCreateAdminBO {
    String username;
    String password;
    String avatar;
    Integer[] roleIds;
}

 