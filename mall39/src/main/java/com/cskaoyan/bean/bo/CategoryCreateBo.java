package com.cskaoyan.bean.bo;

import lombok.Data;

@Data
public class CategoryCreateBo {
    private String desc;
    private String iconUrl;
    private String keywords;
    private String level;
    private String name;
    private String picUrl;
    private Integer pid;
}
