package com.cskaoyan.bean.bo;

import com.cskaoyan.bean.vo.CategoryDataChildrenBean;
import lombok.Data;

import java.util.List;

@Data
public class CategoryUpdateBo {
    private Integer id;
    private String name;
    private String keywords;
    private String desc;
    private String iconUrl;
    private String picUrl;
    private String level;
    private List<CategoryDataChildrenBean> children;
    private Integer pid;
}
