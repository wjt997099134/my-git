package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 微信 地址保存 POST请求参数
 *
 * @author yemingfei
 * @since 2022/06/08 16:57
 */
@Data
public class WxAddressSaveBo {

    /**
     * areaCode : 140302
     * isDefault : false
     * addressDetail : 知道，那就去找啊！
     * province : 山西省
     * city : 阳泉市
     * name : 小白龙
     * county : 城区
     * tel : 12306123456
     * id : 68
     */
    // 地址id 小程序新建地址传过来的id = 0,
    private Integer id;
    // 收货人名称
    private String name;
    // 手机号码
    private String tel;
    // 省市区名
    private String province;
    private String city;
    private String county;
    // 地区邮编
    private String areaCode;
    //  详细地址
    private String addressDetail;
    // 是否是默认地址
    private Boolean isDefault;

}
