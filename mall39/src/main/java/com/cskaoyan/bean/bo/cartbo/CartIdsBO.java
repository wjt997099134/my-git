package com.cskaoyan.bean.bo.cartbo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/7 20:24
 * @Description:购物车订单数组接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartIdsBO {
    List<Integer> productIds;
}
