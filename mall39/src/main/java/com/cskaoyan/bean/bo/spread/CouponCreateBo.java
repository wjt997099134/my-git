package com.cskaoyan.bean.bo.spread;

import lombok.Data;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/05 21:18
 */
@Data
public class CouponCreateBo {

    /**
     * discount : 5
     * timeType : 0
     * goodsValue : []
     * type : 0
     * goodsType : 0
     * total : 10
     * min : 10
     * name : aa
     * limit : 1
     * days : 10
     * startTime : null
     * tag : aa
     * endTime : null
     * desc : aa
     * status : 0
     */
    private Integer discount;
    private Integer timeType;
    private String[] goodsValue;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private String name;
    private Integer limit;
    private Integer days;
    private String startTime;
    private String tag;
    private String endTime;
    private String desc;
    private Integer status;

}
