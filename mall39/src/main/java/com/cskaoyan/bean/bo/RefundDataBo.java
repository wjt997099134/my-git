package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class RefundDataBo {
    Integer orderId;
    BigDecimal refundMoney;
}
