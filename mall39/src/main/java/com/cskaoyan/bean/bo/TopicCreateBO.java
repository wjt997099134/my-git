package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @ClassName: TopicCreateBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 15:40
 */

@Data
public class TopicCreateBO {

    /**
     * picUrl : http://localhost:8083/pic/d6ea00a7-2c74-45ed-b78a-eeb61ce45fd9.jpg
     * price : 20
     * subtitle : 换个卡该回家了捡垃圾
     * goods : []
     * title : 大概是点分公司发的
     * readCount : 0
     * content : <p>ldsml;agmdl;gdfl;mald;mvl;dmvkdjogjreoigjqe[rkg,l;fdamblarb616515631</p>
     */

    private String picUrl;
    private BigDecimal price;
    private String subtitle;
    private Integer[] goods;
    private String title;
    private String readCount;
    private String content;
}