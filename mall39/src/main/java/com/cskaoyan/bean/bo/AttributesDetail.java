package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/5 20:27
 * @Description:新增商品传参的商品参数细节
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class AttributesDetail {
    String attribute;
    String value;
}
