package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 首页修改密码 POST请求参数
 *
 * @author yemingfei
 * @since 2022/06/08 13:27
 */
@NoArgsConstructor
@Data
public class ProfilePasswordBo {

    /**
     * newPassword2 : admin123
     * oldPassword : admin123
     * newPassword : admin123
     */
    private String newPassword2;
    private String oldPassword;
    private String newPassword;


}
