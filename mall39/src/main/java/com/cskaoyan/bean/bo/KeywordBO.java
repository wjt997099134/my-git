package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/6 20:19
 * @Description:新增关键字接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class KeywordBO {

    private boolean isDefault;
    private String keyword;
    private String url;
    private boolean isHot;
}
