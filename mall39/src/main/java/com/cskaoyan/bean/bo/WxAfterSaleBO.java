package com.cskaoyan.bean.bo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: WxAfterSaleBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 9:43
 */

@Data
public class WxAfterSaleBO {
    private BigDecimal amount;
    private String comment;
    private Integer orderId;
    private String[] pictures;
    private String reason;
    private Short type;
    private String typeDesc;
}