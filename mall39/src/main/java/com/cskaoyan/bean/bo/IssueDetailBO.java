package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 17:28
 * @Description:通用问题详情接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class IssueDetailBO {

    private String question;
    private String answer;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private int id;

}
