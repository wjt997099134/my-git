package com.cskaoyan.bean.bo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * @param
 * @Date: 2022/6/5 20:24
 * @Description:新增商品传参的货品库存细节
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class Products {
    private Integer id;
    private String[] specifications;
    private String price;
    private String number;
    private String url;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private String updateTime;
    private Boolean deleted;
}
