package com.cskaoyan.bean.bo;

import lombok.Data;

@Data
public class ShipDataBo {
    private Integer orderId;
    private String shipChannel;
    private String shipSn;
}
