package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.*;

/**
 * @author karlfu
 * @since 2022/06/07 17:37
 */

@NoArgsConstructor
@Data
public class WxFeedbackBO {
    @NotEmpty(message = "手机号不能为空")
    @Pattern(regexp = "1[3|4|5|7|8][0-9]\\d{8}", message = "手机号有误")
    private String mobile;
    @NotEmpty(message = "反馈类型不能为空")
    private String feedType;
    @NotEmpty(message = "反馈内容不能为空")
    private String content;
    @NotNull(message = "请告知是否有图片")
    private Boolean hasPicture;
    private String[] picUrls;
}
