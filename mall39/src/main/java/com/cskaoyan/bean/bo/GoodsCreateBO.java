package com.cskaoyan.bean.bo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * @param
 * @Date: 2022/6/5 20:23
 * @Description:新增商品传参接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsCreateBO {
    private GoodsDetail goods;
    private List<Specifications> specifications;
    private List<Products> products;
    private List<AttributesDetail> attributes;
}
