package com.cskaoyan.bean.bo.spread;

import lombok.Data;

/**
 * @author CR
 * @since 2022/06/05 16:26
 */
@Data
public class AdCreateBo {

    /**
     * name : 2
     * link : 2
     * position : 1
     * content : 2
     * url : http://182.92.235.201:8083/wx/storage/fetch/86a0jtgdo2z65zshvj87.jpg
     * enabled : true
     */
    private String name;
    private String link;
    private Integer position;
    private String content;
    private String url;
    private boolean enabled;

}
