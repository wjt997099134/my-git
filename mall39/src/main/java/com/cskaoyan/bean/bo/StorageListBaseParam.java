package com.cskaoyan.bean.bo;

import lombok.Data;

@Data
public class StorageListBaseParam {

    String key;
    String name;
    Integer page;
    Integer limit;
    String sort;
    String order;
}
