package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 微信小程序订单模块
 * wx/order/comment 的BO
 *
 * @author WuHuaguo
 * @since 2022/06/08 16:35
 */

@Data
public class WxOrderCommentBO {
    Integer orderGoodsId;
    String content;
    Short star;
    Boolean hasPicture;
    String[] picUrls;
}

 