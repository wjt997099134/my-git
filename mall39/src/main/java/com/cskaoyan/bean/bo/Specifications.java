package com.cskaoyan.bean.bo;


import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/5 20:24
 * @Description:新增商品传参的规格细节
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class Specifications {
    private String specification;
    private String value;
    private String picUrl;
}
