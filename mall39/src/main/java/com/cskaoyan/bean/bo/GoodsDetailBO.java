package com.cskaoyan.bean.bo;

import com.cskaoyan.bean.vo.AttributesInfo;
import com.cskaoyan.bean.vo.GoodsInfo;
import com.cskaoyan.bean.vo.ProductsInfo;
import com.cskaoyan.bean.vo.SpecificationsInfo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/5 22:09
 * @Description:商品详情显示封装类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsDetailBO {
    private Integer[] categoryIds;
    private GoodsInfo goods;
    private List<AttributesInfo> attributes;
    private List<ProductsInfo> products;
    private List<SpecificationsInfo> specifications;
}
