package com.cskaoyan.bean.bo.cartbo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/7 20:36
 * @Description:接收用于更新购物车商品参数的类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartUpdateBO {

    private int number;
    private int productId;
    private int goodsId;
    private int id;

}