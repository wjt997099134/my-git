package com.cskaoyan.bean.bo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: TopicUpdateBO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 19:47
 */

@Data
public class TopicUpdateBO {
    private Integer id;
    private String title;
    private String subtitle;
    private String price;
    private String readCount;
    private String picUrl;
    private Integer sortOrder;
    private Integer[] goods;
    private Date addTime;
    private Date updateTime;
    private Boolean deleted;
    private String content;
}