package com.cskaoyan.bean.bo.cartbo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/7 20:01
 * @Description:购物车商品选中状态接收
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartCheckedBO extends CartIdsBO {
    Boolean isChecked;
}
