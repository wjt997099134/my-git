package com.cskaoyan.bean.bo.spread;

import lombok.Data;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/06 16:42
 */
@Data
public class CouponDeleteBo {

    /**
     * addTime : 2022-06-06 09:31:53
     * discount : 0
     * timeType : 1
     * goodsValue : []
     * updateTime : 2022-06-06 14:06:14
     * type : 2
     * goodsType : 0
     * total : 1
     * min : 0
     * deleted : false
     * name : 沃尔什
     * limit : 1
     * days : 0
     * startTime : 2022-06-06 14:06:09
     * id : 117
     * tag :
     * endTime : 2022-06-07 00:00:00
     * desc :
     * status : 0
     */
    private String addTime;
    private Integer discount;
    private Integer timeType;
    private List<?> goodsValue;
    private String updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private boolean deleted;
    private String name;
    private Integer limit;
    private Integer days;
    private String startTime;
    private Integer id;
    private String tag;
    private String endTime;
    private String desc;
    private Integer status;

}
