package com.cskaoyan.bean.bo;

import lombok.Data;




@Data
public class BaseParam {

    Integer page;
    Integer limit;
    String sort;
    String order;
}
