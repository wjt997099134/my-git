package com.cskaoyan.bean.bo;

import lombok.Data;

/**
 * 微信小程序订单模块
 * wx/order/submit 的BO
 *
 * @author WuHuaguo
 * @since 2022/06/08 20:44
 */

@Data
public class WxOrderSubmitBO {
    Integer cartId;
    Integer addressId;
    Integer couponId;
    Integer userCouponId;
    String message;
    Integer grouponRulesId;
    Integer grouponLinkId;
}
