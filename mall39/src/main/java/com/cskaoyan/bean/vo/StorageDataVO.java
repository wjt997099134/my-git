package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Storage;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/05 20:26
 */

@NoArgsConstructor
@Data
public class StorageDataVO {
    private Integer total; //总的数据量，没有分页的情况下的数据量
    private Integer pages; //总的页码数
    private Integer limit; //当前页最多的数据量
    private Integer page; //当前页的页码
    private List<Storage> list; //当前页的数据
    //alt + s

    public static StorageDataVO data(long total, Integer pages, Integer limit, Integer page, List<Storage> list) {
        StorageDataVO storageDataVO = new StorageDataVO();
        storageDataVO.setTotal((int) total);
        storageDataVO.setPages(pages);
        storageDataVO.setPage(page);
        storageDataVO.setLimit(limit);
        storageDataVO.setList(list);
        return storageDataVO;
    }
}
