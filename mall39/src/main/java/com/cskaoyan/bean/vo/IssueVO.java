package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 16:38
 * @Description:问题添加返回参数
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class IssueVO {

    private String question;
    private String answer;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private int id;

}
