package com.cskaoyan.bean.vo.spread;

import lombok.Data;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/08 21:08
 */
@Data
public class WxCouponSelectData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxCouponSelectDataListBean> list;

    public static WxCouponSelectData data(long total, Integer pages, long limit, Integer page, List<WxCouponSelectDataListBean> list) {
        WxCouponSelectData couponSelectData = new WxCouponSelectData();
        couponSelectData.setTotal((int)total);
        couponSelectData.setPages(pages);
        couponSelectData.setLimit((int)limit);
        couponSelectData.setPage(page);
        couponSelectData.setList(list);
        return couponSelectData;
    }
}
