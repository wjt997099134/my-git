package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * 系统管理
 * 编辑管理员模块BO
 *
 * @author WuHuaguo
 * @since 2022/06/06 21:21
 */

@Data
public class SystemUpdateAdminVO {
    Integer id;
    String username;
    String avatar;
    Date updateTime;
    Integer[] roleIds;
}

 