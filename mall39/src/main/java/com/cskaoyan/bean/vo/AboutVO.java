package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class AboutVO {
    private String qq;
    private String address;
    private String phone;
    private Double latitude;
    private String name;
    private Double longitude;
}
