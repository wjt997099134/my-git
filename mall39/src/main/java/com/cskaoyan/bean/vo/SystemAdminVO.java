package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * 管理员管理模块
 * admin/admin/list 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/04 22:01
 */

@Data
public class SystemAdminVO {
    Integer limit;
    Integer page;
    Integer pages;
    Integer total;
    List<AdminOfSystemAdminVO> list;
}

 