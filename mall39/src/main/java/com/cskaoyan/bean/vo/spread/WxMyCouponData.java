package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 20:23
 */
@Data
@NoArgsConstructor
public class WxMyCouponData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxMyCouponDataListBean> list;

    public static WxMyCouponData data(long total, Integer pages, Integer limit, Integer page, List<WxMyCouponDataListBean> list) {
        WxMyCouponData myCouponData = new WxMyCouponData();
        myCouponData.setTotal((int)total);
        myCouponData.setPages(pages);
        myCouponData.setLimit(limit);
        myCouponData.setPage(page);
        myCouponData.setList(list);
        return myCouponData;
    }
}
