package com.cskaoyan.bean.vo.cartvo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/7 23:09
 * @Description:已选择地址
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CheckedAddress {
    private Integer id;
    private String name;
    private Integer userId;
    private String province;
    private String city;
    private String county;
    private String addressDetail;
    private String areaCode;
    private String tel;
    private Boolean isDefault;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Boolean deleted;
}
