package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Comment;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: CommentListVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/5 22:05
 */

@Data
public class CommentListVO {
    private Integer limit;
    private List<Comment> list;
    private Integer page;
    private Integer pages;
    private Integer total;

    public CommentListVO() {
    }

    public CommentListVO(Integer limit, List<Comment> list, Integer page, Integer pages, Integer total) {
        this.limit = limit;
        this.list = list;
        this.page = page;
        this.pages = pages;
        this.total = total;
    }
}