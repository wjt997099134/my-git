package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

@Data
@NoArgsConstructor
public class WxGoodsDetailVo {
    private Goods goods;
    private List<GoodsAttribute> attribute;
    private Brand brand;
    private GoodsCommentBean comment;
    private List<GrouponDetailBean> groupon;
    private Goods info;
    private List<Issue> issue;
    private List<GoodsProduct> productList;
    private Boolean share;
    private String shareUrl;
    private List<GoodsSpecificationsBean> specificationList;
    private Integer userHasCollect;
    private Boolean isGroupon = false;
    private Groupon grouponLink;

    @Data
    @NoArgsConstructor
    public static class GoodsCommentBean {
        Integer count;
        List<WxCommentVo> data;
    }

    @Data
    @NoArgsConstructor
    public static class GoodsSpecificationsBean {
        private String name;
        private List<GoodsSpecification> valueList;
    }
    @Data
    @NoArgsConstructor
    public static class GrouponDetailBean{
        private  Integer id;
        private String specification;
        private BigDecimal discount;
        private Integer discountMember;
    }
}
