package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Issue;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/6 15:52
 * @Description:商品展示列表展示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class MarketIssueVO {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<IssueListBean> list;
}
