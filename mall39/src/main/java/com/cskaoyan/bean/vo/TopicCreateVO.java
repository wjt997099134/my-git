package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: TopicCreateVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 15:48
 */

@Data
public class TopicCreateVO {

    /**
     * picUrl : http://182.92.235.201:8083/wx/storage/fetch/ahgyhi8fshddscke5rgb.jpg
     * addTime : 2022-06-06 15:45:41
     * price : 20
     * subtitle : 256165156
     * goods : [1181012]
     * updateTime : 2022-06-06 15:45:41
     * id : 370
     * title : 水电费撒地方
     * content : <p>是的发生的发生的发生的</p>
     */
    private String picUrl;
    private Date addTime;
    private BigDecimal price;
    private String subtitle;
    private Integer[] goods;
    private Date updateTime;
    private Integer id;
    private String title;
    private String content;

    public TopicCreateVO() {
    }

    public TopicCreateVO(String picUrl, Date addTime, BigDecimal price, String subtitle, Integer[] goods, Date updateTime, Integer id, String title, String content) {
        this.picUrl = picUrl;
        this.addTime = addTime;
        this.price = price;
        this.subtitle = subtitle;
        this.goods = goods;
        this.updateTime = updateTime;
        this.id = id;
        this.title = title;
        this.content = content;
    }
}