package com.cskaoyan.bean.vo;


import lombok.Data;

@Data
public class FatherCategoryListBean {
    Integer value;
    String label;

    public FatherCategoryListBean(Integer value, String label) {
        this.value = value;
        this.label = label;
    }

    public FatherCategoryListBean() {
    }
}
