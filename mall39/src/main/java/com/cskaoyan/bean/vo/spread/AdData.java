package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/04 21:51
 */
@Data
@NoArgsConstructor
public class AdData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<AdDataListBean> list;

    public static AdData data(long total, Integer pages, Integer limit, Integer page, List<AdDataListBean> list) {
        AdData adData = new AdData();
        adData.setTotal((int)total);
        adData.setPages(pages);
        adData.setLimit(limit);
        adData.setPage(page);
        adData.setList(list);
        return adData;
    }
}
