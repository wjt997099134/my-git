package com.cskaoyan.bean.vo.spread;


/**
 * @author CR
 * @since 2022/06/04 21:57
 */
public class AdDataListBean {

    /**
     * deleted : false
     * addTime : 2022-05-17 07:51:53
     * name : 测试
     * link : https://www.warriorshoes.com/
     * updateTime : 2022-06-03 17:10:14
     * id : 46
     * position : 1
     * url : http://182.92.235.201:8083/wx/storage/fetch/i9oz9u2i28cjc7nv47cc.jpg
     * content : 用于回力鞋品的推广
     * enabled : true
     */
    private boolean deleted;
    private String addTime;
    private String name;
    private String link;
    private String updateTime;
    private Integer id;
    private Integer position;
    private String url;
    private String content;
    private boolean enabled;

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setPosition(Integer position) {
        this.position = position;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public String getAddTime() {
        return addTime;
    }

    public String getName() {
        return name;
    }

    public String getLink() {
        return link;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public Integer getId() {
        return id;
    }

    public Integer getPosition() {
        return position;
    }

    public String getUrl() {
        return url;
    }

    public String getContent() {
        return content;
    }

    public boolean isEnabled() {
        return enabled;
    }
}
