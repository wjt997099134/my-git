package com.cskaoyan.bean.vo.cartvo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/7 17:24
 * @Description:购物车参数展示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartTotalBean {
    private Integer goodsCount;
    private Integer checkedGoodsCount;
    private Double goodsAmount;
    private Double checkedGoodsAmount;
}
