package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author karlfu
 * @since 2022/06/06 14:23
 */
@NoArgsConstructor
@Data

public class MallConfigVO {

    private String market_mall_longitude;
    private String market_mall_latitude;
    private String market_mall_address;
    private String market_mall_qq;
    private String market_mall_phone;
    private String market_mall_name;

}
