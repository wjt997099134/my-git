package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * @ClassName: WxCommentPostVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 21:36
 */

@Data
public class WxCommentPostVO {
    private Integer id;

    private Integer valueId;

    private Byte type;

    private String content;

    private Integer userId;

    private Boolean hasPicture;

    private String[] picUrls;

    private Short star;

    private Date addTime;

    private Date updateTime;

    public WxCommentPostVO() {
    }

    public WxCommentPostVO(Integer id, Integer valueId, Byte type, String content, Integer userId, Boolean hasPicture, String[] picUrls, Short star, Date addTime, Date updateTime) {
        this.id = id;
        this.valueId = valueId;
        this.type = type;
        this.content = content;
        this.userId = userId;
        this.hasPicture = hasPicture;
        this.picUrls = picUrls;
        this.star = star;
        this.addTime = addTime;
        this.updateTime = updateTime;
    }
}