package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class WxGoodsListVo {
    List<Category> filterCategoryList;
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxHomeListVo.WxHomeGoodsVo> list;
    public static WxGoodsListVo data(Integer total,Integer pages,Integer limit,Integer page,List list){
        WxGoodsListVo goodsData = new WxGoodsListVo();
        goodsData.setTotal(total);
        goodsData.setPages(pages);
        goodsData.setLimit(limit);
        goodsData.setPage(page);
        goodsData.setList(list);
        return goodsData;
    }

}
