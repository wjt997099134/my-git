package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 14:20
 */
@Data
@NoArgsConstructor
public class WxCouponData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxCouponDataListBean> list;

    public static WxCouponData data(long total, Integer pages, Integer limit, Integer page, List<WxCouponDataListBean> list) {
        WxCouponData couponData = new WxCouponData();
        couponData.setTotal((int)total);
        couponData.setPages(pages);
        couponData.setLimit(limit);
        couponData.setPage(page);
        couponData.setList(list);
        return couponData;
    }

}
