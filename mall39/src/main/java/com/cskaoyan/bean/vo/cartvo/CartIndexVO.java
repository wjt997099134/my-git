package com.cskaoyan.bean.vo.cartvo;

import com.cskaoyan.bean.po.Cart;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/7 17:19
 * @Description:购物车列表展示详情类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartIndexVO {

    // 购物车物品数量总价展示
    CartTotalBean cartTotal;

    // 购物车商品详细参数展示
    List<Cart> cartList;

}
