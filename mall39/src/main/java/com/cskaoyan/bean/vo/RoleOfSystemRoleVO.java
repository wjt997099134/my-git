package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * 系统管理
 * 角色管理模块
 * admin/role/list
 *
 * @author WuHuaguo
 * @since 2022/06/05 15:18
 */

@Data
public class RoleOfSystemRoleVO {
    Integer id;
    String name;
    String desc;
    Boolean enabled;
    Boolean deleted;
    Date addTime;
    Date updateTime;
}
