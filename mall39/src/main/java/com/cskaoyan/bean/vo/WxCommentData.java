package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@NoArgsConstructor
@Data
public class WxCommentData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxCommentDataBean> list;

    public static WxCommentData data(Integer total, Integer pages, Integer limit, Integer page, List list) {
        WxCommentData data = new WxCommentData();
        data.setTotal(total);
        data.setPages(pages);
        data.setLimit(limit);
        data.setPage(page);
        data.setList(list);
        return data;
    }

    @NoArgsConstructor
    @Data
    public static class WxCommentDataBean {

        private String content;
        private String adminContent;
        private String[] picList;
        private Date addTime;
        private WxCommentUserBean userInfo;

        public WxCommentDataBean(String content, String adminContent, String[] picList, Date addTime, WxCommentUserBean userInfo) {
            this.content = content;
            this.adminContent = adminContent;
            this.picList = picList;
            this.addTime = addTime;
            this.userInfo = userInfo;
        }

        @NoArgsConstructor
        @Data
        public static class WxCommentUserBean {
            private String avatarUrl;
            private String nickname;

            public WxCommentUserBean(String avatarUrl, String nickname) {
                this.avatarUrl = avatarUrl;
                this.nickname = nickname;
            }
        }

    }
}