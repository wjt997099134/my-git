package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WxCommentCountData {
    Integer allCount;
    Integer hasPicCount;
}
