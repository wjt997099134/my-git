package com.cskaoyan.bean.vo.spread;

import lombok.Data;

/**
 * @author CR
 * @since 2022/06/07 10:58
 */
@Data
public class WxBrandDataListBean {
    private Integer id;
    private String name;
    private String desc;
    private String picUrl;
    private Double floorPrice;
}
