package com.cskaoyan.bean.vo;


import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 15:53
 * @Description:通用问题列表展示信息子模块类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class IssueListBean {
    Integer id;
    String question;
    String answer;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime;
    Boolean deleted;
}
