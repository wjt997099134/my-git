package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * 系统管理
 * 角色管理
 * admin/role/options 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/05 21:16
 */

@Data
public class RoleOfSystemRoleOptionsVO {
    Integer value;
    String label;
}
