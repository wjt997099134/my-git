package com.cskaoyan.bean.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author karlfu
 * @since 2022/06/05 17:34
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
public class StorageVO {

    private Integer id;
    private String key;
    private String name;
    private String type;
    private Integer size;
    private String url;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

}
