package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Brand;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class BrandData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Brand> list;
    public static BrandData data(Integer total,Integer pages,Integer limit,Integer page,List list){
        BrandData brandData = new BrandData();
        brandData.setTotal(total);
        brandData.setPages(pages);
        brandData.setLimit(limit);
        brandData.setPage(page);
        brandData.setList(list);
        return brandData;
    }
}

