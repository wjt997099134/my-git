package com.cskaoyan.bean.vo.spread;

import com.cskaoyan.bean.vo.WxHomeListVo;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 10:57
 */
@Data
@NoArgsConstructor
public class WxBrandData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxBrandDataListBean> list;

    public static WxBrandData data(long total, Integer pages, Integer limit, Integer page, List<WxBrandDataListBean> list) {
        WxBrandData brandData = new WxBrandData();
        brandData.setTotal((int)total);
        brandData.setPages(pages);
        brandData.setLimit(limit);
        brandData.setPage(page);
        brandData.setList(list);
        return brandData;
    }
}
