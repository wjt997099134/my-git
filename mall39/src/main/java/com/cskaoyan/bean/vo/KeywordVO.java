package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.bo.BaseParam;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.SerializedName;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 20:22
 * @Description:新增关键字展示类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class KeywordVO {
    @SerializedName("isDefault")
    private String isDefault;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private int id;
    private String keyword;
    private String url;
    @SerializedName("isHot")
    private String isHot;
}
