package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/5 20:24
 * @Description:新增商品传参的货品库存细节
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class ProductsInfo {
    private Integer id;
    private Integer goodsId;
    private String[] specifications;
    private String price;
    private String number;
    private String url;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Boolean deleted;
}
