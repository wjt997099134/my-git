package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * @param
 * @Date: 2022/6/6 10:55
 * @Description:商品类目和品牌列表展示类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CatAndBrandVO {
    private List<CategoryListBean> categoryList;
    private List<BrandListBean> brandList;
}
