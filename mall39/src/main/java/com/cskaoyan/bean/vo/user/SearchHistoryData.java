package com.cskaoyan.bean.vo.user;

import com.cskaoyan.bean.po.SearchHistory;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-搜索历史VO
 *
 * @author yemingfei
 * @since 2022/06/05 16:48
 */
@NoArgsConstructor
@Data
public class SearchHistoryData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<SearchHistory> list;

    public SearchHistoryData(Integer total, Integer pages, Integer limit, Integer page, List<SearchHistory> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
