package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author CR
 * @since 2022/06/06 14:20
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponUpdate {

    /**
     * addTime : 2022-05-10 17:15:22
     * discount : 0
     * timeType : 0
     * goodsValue : []
     * updateTime : 2022-06-06 14:12:49
     * type : 0
     * goodsType : 0
     * total : 10
     * min : 0
     * deleted : false
     * name : test
     * limit : 1
     * days : 10
     * startTime : 2022-05-10 00:00:00
     * id : 89
     * tag :
     * endTime : 2022-05-09 00:00:00
     * desc :
     * status : 1
     */
    private Date addTime;
    private Integer discount;
    private Integer timeType;
    private List<?> goodsValue;
    private Date updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private boolean deleted;
    private String name;
    private Integer limit;
    private Integer days;
    private Date startTime;
    private Integer id;
    private String tag;
    private Date endTime;
    private String desc;
    private Integer status;

}
