package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * 系统管理
 * 角色管理模块
 * admin/role/list 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/05 15:14
 */

@Data
public class SystemRoleVO {
    Integer limit;
    Integer page;
    Integer pages;
    Integer total;
    List<RoleOfSystemRoleVO> list;
}

 