package com.cskaoyan.bean.vo.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户管理-收货地址VO-地址信息
 *
 * @author yemingfei
 * @since 2022/06/05 16:59
 */
@NoArgsConstructor
@Data
public class AddressDataListBean {
    // 地址id
    private Integer id;
    // 收货人名称
    private String name;
    // 用户id
    private Integer userId;
    // 省、市、区名字
    private String province;
    private String city;
    private String county;
    // 详细地址
    private String addressDetail;
    // 邮编
    private String areaCode;
    // 手机号码
    private String tel;
    // 是否是默认地址
    private Boolean isDefault;
    // 创建时间
    private Date addTime;
    // 更新时间
    private Date updateTime;
    // 是否逻辑删除 0 不删
    private Boolean deleted;

    public AddressDataListBean(Integer id, String name, Integer userId, String province, String city, String county, String addressDetail, String areaCode, String tel, Boolean isDefault, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.name = name;
        this.userId = userId;
        this.province = province;
        this.city = city;
        this.county = county;
        this.addressDetail = addressDetail;
        this.areaCode = areaCode;
        this.tel = tel;
        this.isDefault = isDefault;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}
