package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class OrderDetailData {


    private List<OrderGoods> orderGoods;
    private UserBean user;
    private OrderBean order;

    @Data
    @NoArgsConstructor
    public static class UserBean {
        private String nickname;
        private String avatar;
    }
    @Data
    @NoArgsConstructor
    public static class OrderBean{
        private Integer id;
        private Integer userId;
        private String orderSn;
        private Short orderStatus;
        private Short aftersaleStatus;
        private String consignee;
        private String mobile;
        private String address;
        private String message;
        private BigDecimal goodsPrice;
        private BigDecimal freightPrice;
        private BigDecimal couponPrice;
        private BigDecimal integralPrice;
        private BigDecimal grouponPrice;
        private BigDecimal orderPrice;
        private BigDecimal actualPrice;
        private String shipSn;
        private String shipChannel;
        private Date shipTime;
        private Short comments;
        private Date addTime;
        private Date updateTime;
        private Boolean deleted;

        public OrderBean(Integer id, Integer userId, String orderSn, Short orderStatus, Short aftersaleStatus, String consignee, String mobile, String address, String message, BigDecimal goodsPrice, BigDecimal freightPrice, BigDecimal couponPrice, BigDecimal integralPrice, BigDecimal grouponPrice, BigDecimal orderPrice, BigDecimal actualPrice, String shipSn, String shipChannel, Date shipTime, Short comments, Date addTime, Date updateTime, Boolean deleted) {
            this.id = id;
            this.userId = userId;
            this.orderSn = orderSn;
            this.orderStatus = orderStatus;
            this.aftersaleStatus = aftersaleStatus;
            this.consignee = consignee;
            this.mobile = mobile;
            this.address = address;
            this.message = message;
            this.goodsPrice = goodsPrice;
            this.freightPrice = freightPrice;
            this.couponPrice = couponPrice;
            this.integralPrice = integralPrice;
            this.grouponPrice = grouponPrice;
            this.orderPrice = orderPrice;
            this.actualPrice = actualPrice;
            this.shipSn = shipSn;
            this.shipChannel = shipChannel;
            this.shipTime = shipTime;
            this.comments = comments;
            this.addTime = addTime;
            this.updateTime = updateTime;
            this.deleted = deleted;
        }
    }
}
