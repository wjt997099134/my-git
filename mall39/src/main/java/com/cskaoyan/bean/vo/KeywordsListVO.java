package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Keyword;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/6 17:53
 * @Description:关键字列表展示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class KeywordsListVO {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<KeywordDetailBean> list;
}
