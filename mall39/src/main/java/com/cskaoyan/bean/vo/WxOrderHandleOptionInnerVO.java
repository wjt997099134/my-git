package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @author WuHuaguo
 * @since 2022/06/07 23:33
 */

@Data
public class WxOrderHandleOptionInnerVO {
    // 按钮（true表示前端会出现该按钮）：
    Boolean cancel = false;    // 取消订单
    Boolean delete = false;    // 删除订单
    Boolean pay = false;       // 去付款
    Boolean comment = false;   // 去评价
    Boolean confirm = false;   // 确认收货
    Boolean refund = false;    // 申请退款
    Boolean rebuy = false;     // 再次购买
    Boolean aftersale = false; // 申请售后

    // true表示前端按钮会亮起来
    // 已退款(203): 无
    // 已付款(201): refund
    // 订单取消，退款中: 无
    // 已退款: delete
    // 已发货: confirm
    // 已收货: aftersale,comment,delete,rebuy
}

 