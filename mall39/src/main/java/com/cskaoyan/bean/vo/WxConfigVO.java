package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author karlfu
 * @since 2022/06/06 19:44
 */

@NoArgsConstructor
@Data
public class WxConfigVO {
    private String market_wx_index_new;
    private String market_wx_index_topic;
    private String market_wx_share;
    private String market_wx_index_brand;
    private String market_wx_catlog_goods;
    private String market_wx_catlog_list;
    private String market_wx_index_hot;
}
