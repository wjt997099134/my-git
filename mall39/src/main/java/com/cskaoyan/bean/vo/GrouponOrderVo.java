package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.po.Groupon;
import com.cskaoyan.bean.po.GrouponRules;
import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class GrouponOrderVo {
    private Integer id;
    private Short status;
    private Boolean isCreator;
    private String orderSn;
    private Short orderStatusText;
    private GrouponRules rules;
    private Groupon groupon;
    private Integer joinerCount;
    private List<OrderGoods> goodsList;
    private BigDecimal actualPrice;

    public GrouponOrderVo(Integer id, Short status, Boolean isCreator, String orderSn, Short orderStatusText, GrouponRules rules, Groupon groupon, Integer joinerCount, List<OrderGoods> goodsList, BigDecimal actualPrice) {
        this.id = id;
        this.status = status;
        this.isCreator = isCreator;
        this.orderSn = orderSn;
        this.orderStatusText = orderStatusText;
        this.rules = rules;
        this.groupon = groupon;
        this.joinerCount = joinerCount;
        this.goodsList = goodsList;
        this.actualPrice = actualPrice;
    }
}
