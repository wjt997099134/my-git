package com.cskaoyan.bean.vo.user;

import com.cskaoyan.bean.po.Footprint;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-会员足迹VO
 *
 * @author yemingfei
 * @since 2022/06/05 16:48
 */
@NoArgsConstructor
@Data
public class FootprintData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Footprint> list;

    public FootprintData(Integer total, Integer pages, Integer limit, Integer page, List<Footprint> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
