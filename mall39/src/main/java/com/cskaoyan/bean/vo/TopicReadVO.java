package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Topic;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: TopicReadVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 17:15
 */

@Data
public class TopicReadVO {
    private List<TopicReadGoodsListVO> goodsList;
    private Topic topic;

    public TopicReadVO() {
    }

    public TopicReadVO(List<TopicReadGoodsListVO> goodsList, Topic topic) {
        this.goodsList = goodsList;
        this.topic = topic;
    }
}