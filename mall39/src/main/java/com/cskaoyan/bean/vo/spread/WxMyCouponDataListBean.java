package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @author CR
 * @since 2022/06/07 20:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxMyCouponDataListBean {
    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private BigDecimal min;
    private BigDecimal discount;
    private Date startTime;
    private Date endTime;
    private boolean available;

}
