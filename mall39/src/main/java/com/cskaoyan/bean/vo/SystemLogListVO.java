package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Log;
import lombok.Data;

import java.util.List;

/**
 * 系统管理
 * 操作日志模块
 * admin/log/list 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/06 14:22
 */

@Data
public class SystemLogListVO {
    Integer limit;
    Integer page;
    Integer pages;
    Integer total;
    List<Log> list;
}
