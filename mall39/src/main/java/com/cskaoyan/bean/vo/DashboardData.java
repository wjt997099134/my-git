package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 首页模块VO
 * @author yemingfei
 * @since 2022/06/05 21:06
 */
@NoArgsConstructor
@Data
public class DashboardData {
    private Integer goodsTotal;
    private Integer userTotal;
    private Integer productTotal;
    private Integer orderTotal;

    public DashboardData(Integer goodsTotal, Integer userTotal, Integer productTotal, Integer orderTotal) {
        this.goodsTotal = goodsTotal;
        this.userTotal = userTotal;
        this.productTotal = productTotal;
        this.orderTotal = orderTotal;
    }
}
