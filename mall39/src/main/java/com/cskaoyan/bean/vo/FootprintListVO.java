package com.cskaoyan.bean.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/08 19:58
 */

@NoArgsConstructor
@Data
public class FootprintListVO {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<FootprintDetail> list;

    @NoArgsConstructor
    @Data
    public static class FootprintDetail {
        private String brief;
        private String picUrl;
        @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        private Date addTime;
        private Integer goodsId;
        private String name;
        private Integer id;
        private BigDecimal retailPrice;
    }

    public static FootprintListVO data(long total, Integer pages, Integer limit, Integer page, List<FootprintDetail> list) {
        FootprintListVO footprintListVO = new FootprintListVO();
        footprintListVO.setTotal((int) total);
        footprintListVO.setPages(pages);
        footprintListVO.setPage(page);
        footprintListVO.setLimit(limit);
        footprintListVO.setList(list);
        return footprintListVO;
    }
}
