package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Order;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class OrderData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Order> list;
    public static OrderData data(Integer total,Integer pages,Integer limit,Integer page,List list){
        OrderData orderData = new OrderData();
        orderData.setTotal(total);
        orderData.setPages(pages);
        orderData.setLimit(limit);
        orderData.setPage(page);
        orderData.setList(list);
        return orderData;
    }
}
