package com.cskaoyan.bean.vo.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-会员管理VO
 *
 * @author yemingfei
 * @since 2022/06/05 15:20
 */

@NoArgsConstructor
@Data
public class MemberData {
    // 会员信息总条数
    private Integer total;
    // 共有几页信息
    private Integer pages;
    // 每页显示几条信息
    private Integer limit;
    // 当前所处页
    private Integer page;
    // 具体的会员信息
    private List<MemberDataListBean> list;

    public MemberData(Integer total, Integer pages, Integer limit, Integer page, List<MemberDataListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
