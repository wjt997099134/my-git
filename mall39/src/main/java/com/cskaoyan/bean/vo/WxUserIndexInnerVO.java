package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @ClassName: WxUserIndexInnerVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 11:40
 */

@Data
public class WxUserIndexInnerVO {
    private Integer uncomment;
    private Integer unpaid;
    private Integer unrecv;
    private Integer unship;

    public WxUserIndexInnerVO() {
    }

    public WxUserIndexInnerVO(Integer uncomment, Integer unpaid, Integer unrecv, Integer unship) {
        this.uncomment = uncomment;
        this.unpaid = unpaid;
        this.unrecv = unrecv;
        this.unship = unship;
    }
}