package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

////id":1,"name":"北京市","type":1,"code":110000,"children":
@Data
public class RegionData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<RegionDataListBean> list;

    public RegionData() {
    }

    public RegionData(Integer total, Integer pages, Integer limit, Integer page, List<RegionDataListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }

    @Data
    public static class RegionDataListBean {
        private Integer id;
        private String name;
        private Integer type;
        private Integer code;
        private List<RegionDataChildrenBean> children;

        public RegionDataListBean() {
        }

        public RegionDataListBean(Integer id, String name, Integer type, Integer code, List<RegionDataChildrenBean> children) {
            this.id = id;
            this.name = name;
            this.type = type;
            this.code = code;
            this.children = children;
        }

        @Data
        public static class RegionDataChildrenBean {
            private Integer id;
            private String name;
            private Integer type;
            private Integer code;
            private List<RegionDataChildrenListBean> children;

            public RegionDataChildrenBean(Integer id, String name, Integer type, Integer code, List<RegionDataChildrenListBean> children) {
                this.id = id;
                this.name = name;
                this.type = type;
                this.code = code;
                this.children = children;
            }

            public RegionDataChildrenBean() {
            }

            @Data
            public static class RegionDataChildrenListBean {
                private Integer id;
                private String name;
                private Integer type;
                private Integer code;

                public RegionDataChildrenListBean(Integer id, String name, Integer type, Integer code) {
                    this.id = id;
                    this.name = name;
                    this.type = type;
                    this.code = code;
                }

                public RegionDataChildrenListBean() {
                }
            }
        }

    }

}
