package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class WxGoodsRelatedData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxHomeListVo.WxHomeGoodsVo> list;
    public static WxGoodsRelatedData data(Integer total,Integer pages,Integer limit,Integer page,List list){
        WxGoodsRelatedData data = new WxGoodsRelatedData();
        data.setTotal(total);
        data.setPages(pages);
        data.setLimit(limit);
        data.setPage(page);
        data.setList(list);
        return data;
    }
}
