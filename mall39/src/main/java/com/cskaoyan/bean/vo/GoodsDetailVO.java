package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.bo.Products;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/5 22:09
 * @Description:商品详情显示封装类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsDetailVO {
    private Integer[] categoryIds;
    private GoodsInfo goods;
    private List<AttributesInfo> attributes;
    private List<ProductsInfo> products;
    private List<SpecificationsInfo> specifications;
}
