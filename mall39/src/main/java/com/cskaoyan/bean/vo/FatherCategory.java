package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;


@Data
public class FatherCategory {
    Integer total;
    Integer pages;
    Integer limit;
    Integer page;
    List<FatherCategoryListBean> list;

    public FatherCategory(Integer total, Integer pages, Integer limit, Integer page, List<FatherCategoryListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }

    public FatherCategory() {
    }
}
