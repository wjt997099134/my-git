package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Keyword;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/09 09:54
 */

@Data
@NoArgsConstructor
public class SearchIndexVO {

    private Keyword defaultKeyword;
    private List<Keyword> hotKeywordList;
    private List<HistoryKeyword> historyKeywordList;

    @Data
    @NoArgsConstructor
    public static class HistoryKeyword {
        private String keyword;
    }
}
