package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author CR
 * @since 2022/06/08 21:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WxCouponSelectDataListBean {
    private Integer id;
    private Integer cid;
    private String name;
    private String desc;
    private String tag;
    private Double min;
    private Double discount;
    private Date startTime;
    private Date endTime;
    private boolean available;
}
