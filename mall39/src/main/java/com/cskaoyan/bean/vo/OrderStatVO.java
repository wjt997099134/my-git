package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/06 22:56
 */
@NoArgsConstructor
@Data
public class OrderStatVO {
    private String[] columns = {"day","orders","customers","amount","pcr"};
    private List<OrderStatVORow> rows;
}
