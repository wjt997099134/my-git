package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Topic;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: TopicListVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 9:29
 */

@Data
public class TopicListVO {
    private Integer limit;
    private List<Topic> list;
    private Integer page;
    private Integer pages;
    private Integer total;

    public TopicListVO() {
    }

    public TopicListVO(Integer limit, List<Topic> list, Integer page, Integer pages, Integer total) {
        this.limit = limit;
        this.list = list;
        this.page = page;
        this.pages = pages;
        this.total = total;
    }
}