package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @ClassName: WxUserIndexVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 14:53
 */

@Data
public class WxUserIndexVO {
    private WxUserIndexInnerVO order;

    public WxUserIndexVO() {
    }

    public WxUserIndexVO(WxUserIndexInnerVO order) {
        this.order = order;
    }
}