package com.cskaoyan.bean.vo;



import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class WxHomeListVo {
    private List<WxHomeAdVo> banner;
    private List<WxHomeBrandVo> brandList;
    private List<WxHomeCategoryVo> channel;
    private List<WxHomeCouponVo> couponList;
    private List<WxHomeFloorGoodsVo> floorGoodsList;
    private List<WxHomeGoodsVo> hotGoodsList;
    private List<WxHomeGoodsVo> newGoodsList;
    private List<WxHomeTopicVo> topicList;

    @Data
    @NoArgsConstructor
    public static class WxHomeAdVo {
        private Integer id;
        private String name;
        private String link;
        private String url;
        private Byte position;
        private String content;
        private Boolean enabled;
        private Date addTime;
        private Date updateTime;
        private Boolean deleted;
    }

    @Data
    @NoArgsConstructor
    public static class WxHomeBrandVo {
        private Integer id;
        private String name;
        private String desc;
        private String picUrl;
        private BigDecimal floorPrice;
    }

    @Data
    @NoArgsConstructor
    public static class WxHomeCategoryVo {
        private Integer id;
        private String name;
        private String iconUrl;
    }

    @Data
    @NoArgsConstructor
    public static class WxHomeCouponVo {
        private Integer id;
        private String name;
        private String desc;
        private String tag;
        private BigDecimal discount;
        private BigDecimal min;
        private Short days;
        private Date startTime;
        private Date endTime;
    }

    @Data
    @NoArgsConstructor
    public static class WxHomeFloorGoodsVo {
        private List<WxHomeGoodsVo> goodsList;
        private Integer id;
        private String name;
    }

    @Data
    @NoArgsConstructor
    public static class WxHomeGoodsVo {
        private String brief;
        private BigDecimal counterPrice;
        private Integer id;
        private Boolean isHot;
        private Boolean isNew;
        private String name;
        private String picUrl;
        private BigDecimal retailPrice;
    }
    @Data
    @NoArgsConstructor
    public static class WxHomeTopicVo{
        private Integer id;
        private String title;
        private String subtitle;
        private BigDecimal price;
        private String readCount;
        private String picUrl;

    }
}
