package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
public class WxCommentVo {

    private Integer id;
    private String content;
    private String adminContent;
    private String[] picList;
    private Date addTime;
    private String avatar;
    private String nickname;

    public WxCommentVo(Integer id, String content, String adminContent, String[] picList, Date addTime, String avatar, String nickname) {
        this.id = id;
        this.content = content;
        this.adminContent = adminContent;
        this.picList = picList;
        this.addTime = addTime;
        this.avatar = avatar;
        this.nickname = nickname;
    }
}
