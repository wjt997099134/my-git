package com.cskaoyan.bean.vo.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-收货地址VO
 *
 * @author yemingfei
 * @since 2022/06/05 16:48
 */
@NoArgsConstructor
@Data
public class AddressData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<AddressDataListBean> list;

    public AddressData(Integer total, Integer pages, Integer limit, Integer page, List<AddressDataListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
