package com.cskaoyan.bean.vo.spread;

import com.cskaoyan.bean.po.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 16:12
 */
@Data
@NoArgsConstructor
public class WxCategoryData {
    List<Category> categoryList;
    Category currentCategory;
    List<Category> currentSubCategory;
}
