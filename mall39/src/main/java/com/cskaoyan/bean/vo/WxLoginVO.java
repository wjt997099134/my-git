package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @ClassName: WxLoginVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/7 20:50
 */

@Data
public class WxLoginVO {
    private String token;
    private WxLoginUserInfoVO userInfo;

    public WxLoginVO() {
    }

    public WxLoginVO(String token, WxLoginUserInfoVO userInfo) {
        this.token = token;
        this.userInfo = userInfo;
    }
}