package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/06 21:51
 */
@NoArgsConstructor
@Data
public class UserStatVO {

    private String[] columns = {"day","users"};
    private List<UserStatVORow> rows;
}
