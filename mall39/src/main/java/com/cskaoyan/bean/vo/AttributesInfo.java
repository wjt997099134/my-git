package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/5 22:22
 * @Description:商品参数详情类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class AttributesInfo {
    Integer id;
    Integer goodsId;
    String attribute;
    String value;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    Date updateTime;
    Boolean deleted;
}
