package com.cskaoyan.bean.vo.topic;

import com.cskaoyan.bean.po.Topic;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: WxTopicDetailVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 15:43
 */

@Data
public class WxTopicDetailVO {
    private List<WxTopicDetailGoodsVO> goods;
    private Topic topic;

    public WxTopicDetailVO() {
    }

    public WxTopicDetailVO(List<WxTopicDetailGoodsVO> goods, Topic topic) {
        this.goods = goods;
        this.topic = topic;
    }
}