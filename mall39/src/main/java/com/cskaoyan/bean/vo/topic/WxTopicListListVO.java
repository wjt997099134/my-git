package com.cskaoyan.bean.vo.topic;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: WxTopicListListVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 16:30
 */

@Data
public class WxTopicListListVO {
    private Integer id;
    private String picUrl;
    private BigDecimal price;
    private String readCount;
    private String subtitle;
    private String title;

    public WxTopicListListVO() {
    }

    public WxTopicListListVO(Integer id, String picUrl, BigDecimal price, String readCount, String subtitle, String title) {
        this.id = id;
        this.picUrl = picUrl;
        this.price = price;
        this.readCount = readCount;
        this.subtitle = subtitle;
        this.title = title;
    }
}