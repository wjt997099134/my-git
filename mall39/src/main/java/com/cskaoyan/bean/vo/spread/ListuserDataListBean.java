package com.cskaoyan.bean.vo.spread;

import lombok.Data;

import java.util.Date;

/**
 * @author CR
 * @since 2022/06/06 17:09
 */
@Data
public class ListuserDataListBean {

    /**
     * deleted : false
     * addTime : 2022-05-10 16:27:17
     * startTime : 2022-05-10 16:27:17
     * updateTime : 2022-05-20 16:39:44
     * id : 133
     * endTime : 2022-05-20 16:27:17
     * couponId : 57
     * userId : 1
     * status : 2
     */
    private boolean deleted;
    private Date addTime;
    private Date startTime;
    private Date updateTime;
    private Integer id;
    private Date endTime;
    private Integer couponId;
    private Integer userId;
    private Integer status;

}
