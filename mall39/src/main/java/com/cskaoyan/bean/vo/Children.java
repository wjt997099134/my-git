package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;


/**
 * @param
 * @Date: 2022/6/6 10:50
 * @Description:商品类目列表子类参数
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class Children {
    private Integer value;
    private String label;
}
