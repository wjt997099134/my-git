package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author CR
 * @since 2022/06/05 16:21
 */
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AdCreate {

    /**
     * addTime : 2022-06-05 16:17:30
     * name : 2
     * link : 2
     * updateTime : 2022-06-05 16:17:30
     * id : 68
     * position : 1
     * url : http://182.92.235.201:8083/wx/storage/fetch/86a0jtgdo2z65zshvj87.jpg
     * content : 2
     * enabled : true
     */
    private Date addTime;
    private String name;
    private String link;
    private Date updateTime;
    private Integer id;
    private Integer position;
    private String url;
    private String content;
    private boolean enabled;

    public AdCreate(Date addTime, String name, Date updateTime, Integer id, Integer position, String url, String content, boolean enabled) {
        this.addTime = addTime;
        this.name = name;
        this.updateTime = updateTime;
        this.id = id;
        this.position = position;
        this.url = url;
        this.content = content;
        this.enabled = enabled;
    }
}
