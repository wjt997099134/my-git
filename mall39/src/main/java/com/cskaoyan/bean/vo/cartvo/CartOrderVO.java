package com.cskaoyan.bean.vo.cartvo;

import com.cskaoyan.bean.po.Cart;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;

/**
 * @param
 * @Date: 2022/6/7 22:32
 * @Description:购物车订单详情展示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CartOrderVO {
    private Integer grouponRulesId;
    // 实付
    private String actualPrice;
    // 订单总价(含运费)
    private String orderTotalPrice;
    private Integer cartId;
    private Integer userCouponId;
    private Integer couponId;
    // 商品总价
    private String goodsTotalPrice;
    private Integer addressId;
    private String grouponPrice;
    private String couponPrice;
    private Integer availableCouponLength;
    private String freightPrice = "100";
    CheckedAddress checkedAddress;
    List<Cart> checkedGoodsList;
}
