package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * 微信小程序订单模块
 *
 * @author WuHuaguo
 * @since 2022/06/08 21:06
 */

@Data
public class WxOrderSubmitVO {
    Integer orderId;
    Integer grouponLinkId;
}

 