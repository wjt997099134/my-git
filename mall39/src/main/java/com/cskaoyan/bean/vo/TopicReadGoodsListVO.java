package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: TopicReadGoodsListVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 17:42
 */

@Data
public class TopicReadGoodsListVO {
    private String brief;
    private BigDecimal counterPrice;
    private Integer id;
    private Boolean isHot;
    private Boolean isNew;
    private String name;
    private String picUrl;
    private BigDecimal retailPrice;

    public TopicReadGoodsListVO() {
    }

    public TopicReadGoodsListVO(String brief, BigDecimal counterPrice, Integer id, Boolean isHot, Boolean isNew, String name, String picUrl, BigDecimal retailPrice) {
        this.brief = brief;
        this.counterPrice = counterPrice;
        this.id = id;
        this.isHot = isHot;
        this.isNew = isNew;
        this.name = name;
        this.picUrl = picUrl;
        this.retailPrice = retailPrice;
    }
}