package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author yemingfei
 * @since 2022/06/08 20:16
 */
@NoArgsConstructor
@Data
public class WxCollectListDataListBean {

    /**
     * brief : haokang
     * picUrl : http://182.92.235.201:8083/wx/storage/fetch/zt2wild5a2ny1i9kpw99.jpg
     * valueId : 1181217
     * name : yu
     * id : 178
     * type : 0
     * retailPrice : 66666.0
     */
    private String brief;
    private String picUrl;
    private Integer valueId;
    private String name;
    private Integer id;
    private Integer type;
    private Double retailPrice;

    public WxCollectListDataListBean(String brief, String picUrl, Integer valueId, String name, Integer id, Integer type, Double retailPrice) {
        this.brief = brief;
        this.picUrl = picUrl;
        this.valueId = valueId;
        this.name = name;
        this.id = id;
        this.type = type;
        this.retailPrice = retailPrice;
    }
}
