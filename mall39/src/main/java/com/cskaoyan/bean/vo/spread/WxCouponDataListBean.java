package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author CR
 * @since 2022/06/07 14:20
 */
@Data
public class WxCouponDataListBean {
    private Integer id;
    private String name;
    private String desc;
    private String tag;
    private Double discount;
    private Double min;
    private Integer days;
    private Date startTime;
    private Date endTime;
}
