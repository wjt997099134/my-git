package com.cskaoyan.bean.vo.user;

import com.cskaoyan.bean.po.Feedback;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-意见反馈VO
 *
 * @author yemingfei
 * @since 2022/06/05 16:48
 */
@NoArgsConstructor
@Data
public class FeedbackData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Feedback> list;

    public FeedbackData(Integer total, Integer pages, Integer limit, Integer page, List<Feedback> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
