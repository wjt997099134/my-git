package com.cskaoyan.bean.vo.topic;

import lombok.Data;

import java.math.BigDecimal;

/**
 * @ClassName: WxTopicDetailGoodsVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 15:44
 */

@Data
public class WxTopicDetailGoodsVO {
    private String biref;
    private BigDecimal counterPrice;
    private Integer id;
    private Boolean isHot;
    private Boolean isNew;
    private String name;
    private String picUrl;
    private BigDecimal retailPrice;

    public WxTopicDetailGoodsVO() {
    }

    public WxTopicDetailGoodsVO(String biref, BigDecimal counterPrice, Integer id, Boolean isHot, Boolean isNew, String name, String picUrl, BigDecimal retailPrice) {
        this.biref = biref;
        this.counterPrice = counterPrice;
        this.id = id;
        this.isHot = isHot;
        this.isNew = isNew;
        this.name = name;
        this.picUrl = picUrl;
        this.retailPrice = retailPrice;
    }
}