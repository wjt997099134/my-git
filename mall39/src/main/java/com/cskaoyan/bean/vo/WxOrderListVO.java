package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * 微信小程序订单模块
 * wx/order/list 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/07 15:46
 */

@Data
public class WxOrderListVO {
    Integer page;
    Integer limit;
    Integer pages;
    Integer total;
    List<OrderInnerVO> list;

    @Data
    public static class OrderInnerVO {
        String orderStatusText;
        // 售后状态：
        // 0是可申请，1是用户已申请，2是管理员审核通过，3是管理员退款成功，4是管理员审核拒绝，5是用户已取消
        Short aftersaleStatus;
        Boolean isGoupin;
        String orderSn;
        BigDecimal actualPrice;
        Integer id;
        List<GoodsInnerVO> goodsList;
        WxOrderHandleOptionInnerVO handleOption;

        @Data
        public static class GoodsInnerVO {
            Short number;
            String picUrl;
            BigDecimal price;
            Integer id;
            String goodsName;
            String[] specifications;
        }
    }
}

 