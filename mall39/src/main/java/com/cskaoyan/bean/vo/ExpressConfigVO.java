package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author karlfu
 * @since 2022/06/06 19:33
 */

@NoArgsConstructor
@Data
public class ExpressConfigVO {
    private String market_express_freight_min;
    private String market_express_freight_value;
}
