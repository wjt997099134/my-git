package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/5 22:18
 * @Description:商品详情规格显示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class SpecificationsInfo {
    private Integer id;
    private Integer goodsId;
    private String specification;
    private String value;
    private String picUrl;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Boolean deleted;
}
