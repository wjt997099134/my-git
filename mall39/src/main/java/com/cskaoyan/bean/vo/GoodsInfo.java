package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/5 20:22
 * @Description:商品修改页面显示类的商品细节封装
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsInfo {
    private Integer id;
    private String goodsSn;
    private String name;
    private Integer categoryId;
    private Integer brandId;
    private String[] gallery;
    private String keywords;
    private String brief;
    private Boolean isOnSale;
    private Integer sortOrder;
    private String picUrl;
    private String shareUrl;
    private Boolean isNew;
    private Boolean isHot;
    private String unit;
    private Double counterPrice;
    private Double retailPrice;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private Boolean deleted;
    private String detail;
}
