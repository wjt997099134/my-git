package com.cskaoyan.bean.vo.topic;

import lombok.Data;

import java.util.List;

/**
 * @ClassName: WxTopicListVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/8 16:29
 */

@Data
public class WxTopicListVO {
    private Integer limit;
    private List<WxTopicListListVO> list;
    private Integer page;
    private Integer pages;
    private Integer total;

    public WxTopicListVO() {
    }

    public WxTopicListVO(Integer limit, List<WxTopicListListVO> list, Integer page, Integer pages, Integer total) {
        this.limit = limit;
        this.list = list;
        this.page = page;
        this.pages = pages;
        this.total = total;
    }
}