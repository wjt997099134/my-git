package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author karlfu
 * @since 2022/06/06 19:38
 */

@NoArgsConstructor
@Data
public class OrderConfigVO {

    private String market_order_unconfirm;
    private String market_order_unpaid;
    private String market_order_comment;
}
