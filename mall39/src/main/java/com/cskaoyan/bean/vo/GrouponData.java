package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class GrouponData {
    List<GrouponDataBean> list;
    Integer page;
    Integer limit;
    Integer pages;
    Integer total;

    public static GrouponData data(Integer total, Integer pages, Integer limit, Integer page, List list) {
        GrouponData data = new GrouponData();
        data.setTotal(total);
        data.setPages(pages);
        data.setLimit(limit);
        data.setPage(page);
        data.setList(list);
        return data;
    }

    @Data
    @NoArgsConstructor
    public static class GrouponDataBean {
        private Integer id;
        private String name;
        private String picUrl;
        private Integer grouponMember;
        private Date expireTime;
        private String brief;
        private BigDecimal retailPrice;
        private BigDecimal grouponPrice;
        private Integer grouponId;
        private BigDecimal discount;
        private Boolean isGroupon;
    }
}
