package com.cskaoyan.bean.vo;


import lombok.Data;

@Data
public class ChannelData {
    private String code;
    private String name;
}
