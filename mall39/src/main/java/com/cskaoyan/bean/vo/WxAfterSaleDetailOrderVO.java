package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: WxAfterSaleDetailOrderVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 11:09
 */

@Data
public class WxAfterSaleDetailOrderVO {
    private Integer id;

    private Integer userId;

    private String orderSn;

    private Short orderStatus;

    private Short aftersaleStatus;

    private String consignee;

    private String mobile;

    private String address;

    private String message;

    private BigDecimal goodsPrice;

    private BigDecimal freightPrice;

    private BigDecimal couponPrice;

    private BigDecimal integralPrice;

    private BigDecimal grouponPrice;

    private BigDecimal orderPrice;

    private BigDecimal actualPrice;

    private Date payTime;

    private String shipSn;

    private String shipChannel;

    private Date shipTime;

    private Date confirmTime;

    private Short comments;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    public WxAfterSaleDetailOrderVO() {
    }

    public WxAfterSaleDetailOrderVO(Integer id, Integer userId, String orderSn, Short orderStatus, Short aftersaleStatus, String consignee, String mobile, String address, String message, BigDecimal goodsPrice, BigDecimal freightPrice, BigDecimal couponPrice, BigDecimal integralPrice, BigDecimal grouponPrice, BigDecimal orderPrice, BigDecimal actualPrice, Date payTime, String shipSn, String shipChannel, Date shipTime, Date confirmTime, Short comments, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.userId = userId;
        this.orderSn = orderSn;
        this.orderStatus = orderStatus;
        this.aftersaleStatus = aftersaleStatus;
        this.consignee = consignee;
        this.mobile = mobile;
        this.address = address;
        this.message = message;
        this.goodsPrice = goodsPrice;
        this.freightPrice = freightPrice;
        this.couponPrice = couponPrice;
        this.integralPrice = integralPrice;
        this.grouponPrice = grouponPrice;
        this.orderPrice = orderPrice;
        this.actualPrice = actualPrice;
        this.payTime = payTime;
        this.shipSn = shipSn;
        this.shipChannel = shipChannel;
        this.shipTime = shipTime;
        this.confirmTime = confirmTime;
        this.comments = comments;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}