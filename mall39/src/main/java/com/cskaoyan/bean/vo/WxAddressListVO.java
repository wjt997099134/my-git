package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.Address;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/07 20:41
 */

@NoArgsConstructor
@Data
public class WxAddressListVO {

    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Address> list;

}
