package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author karlfu
 * @since 2022/06/07 09:24
 */

@NoArgsConstructor
@Data
public class GoodsStatVORow {
    private Double amount;
    private Integer orders;
    private String day;
    private Integer products;
}
