package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor
@Data
public class InfoData {
    private String name;
    private String avatar;
    private List<String> roles;
    private List<String> perms;

    public InfoData(String name, String avatar, List<String> roles, List<String> perms) {
        this.name = name;
        this.avatar = avatar;
        this.roles = roles;
        this.perms = perms;
    }
}
