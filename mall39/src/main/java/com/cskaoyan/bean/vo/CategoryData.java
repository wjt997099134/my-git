package com.cskaoyan.bean.vo;



import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;



@NoArgsConstructor
@Data
public class CategoryData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<CategoryDataListBean> list;

    public CategoryData(Integer total, Integer pages, Integer limit, Integer page, List<CategoryDataListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
