package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

/**
 * @ClassName: WxAfterSaleDetailAftersaleVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 11:03
 */

@Data
public class WxAfterSaleDetailAftersaleVO {
    private Integer id;

    private String aftersaleSn;

    private Integer orderId;

    private Integer userId;

    private Short type;

    private String reason;

    private BigDecimal amount;

    private String[] pictures;

    private String comment;

    private Short status;

    private Date addTime;

    private Date updateTime;

    private Boolean deleted;

    public WxAfterSaleDetailAftersaleVO() {
    }

    public WxAfterSaleDetailAftersaleVO(Integer id, String aftersaleSn, Integer orderId, Integer userId, Short type, String reason, BigDecimal amount, String[] pictures, String comment, Short status, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.aftersaleSn = aftersaleSn;
        this.orderId = orderId;
        this.userId = userId;
        this.type = type;
        this.reason = reason;
        this.amount = amount;
        this.pictures = pictures;
        this.comment = comment;
        this.status = status;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}