package com.cskaoyan.bean.vo;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * @param
 * @Date: 2022/6/6 19:28
 * @Description:关键字列表详情子模块
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class KeywordDetailBean {
    @JsonProperty("isDefault")
    private boolean isDefault;
    private boolean deleted;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date addTime;
    private int sortOrder;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private int id;
    private String keyword;
    private String url;
    @JsonProperty("isHot")
    private boolean isHot;
}
