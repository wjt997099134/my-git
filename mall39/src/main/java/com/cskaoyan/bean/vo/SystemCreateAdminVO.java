package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.Date;

/**
 * 系统管理
 * 新增管理员的VO
 *
 * @author WuHuaguo
 * @since 2022/06/06 20:38
 */

@Data
public class SystemCreateAdminVO {
    Integer id;
    String username;
    String password;
    String avatar;
    Integer[] roleIds;
    Date addTime;
    Date updateTime;
}

 