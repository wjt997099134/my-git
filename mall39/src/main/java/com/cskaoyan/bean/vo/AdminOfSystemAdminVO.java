package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * 管理员管理
 * admin/admin/list 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/04 21:32
 */

@Data
public class AdminOfSystemAdminVO {
    String avatar;
    Integer id;
    String username;
    Integer[] roleIds;
}

 