package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

@Data
@NoArgsConstructor
public class MyGrouponData {
    List<GrouponOrderVo> orderList;

    public MyGrouponData(List<GrouponOrderVo> orderList) {
        this.orderList = orderList;
    }
}
