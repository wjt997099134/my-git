package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/07 09:13
 */

@NoArgsConstructor
@Data
public class GoodsStatVO {

    private String[] columns = {"day","orders","products","amount"};
    private List<GoodsStatVORow> rows;

}
