package com.cskaoyan.bean.vo.spread;

import com.cskaoyan.bean.po.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 17:31
 */
@Data
@NoArgsConstructor
public class WxCurrentCategoryData {
    Category currentCategory;
    List<Category> currentSubCategory;
}
