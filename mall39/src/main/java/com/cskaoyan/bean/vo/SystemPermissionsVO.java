package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 系统管理
 * admin/role/permission 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/07 10:36
 */

@Data
@NoArgsConstructor
public class SystemPermissionsVO {
    List<String> assignedPermissions;
    List<Child1> systemPermissions;


    @Data
    public static class Child1 {
        List<Child2> children;
        String id;
        String label;


        @Data
        public static class Child2 {
            List<Child3> children;
            String id;
            String label;


            @Data
            public static class Child3 {
                String api;
                String id;
                String label;
            }
        }
    }
}

 