package com.cskaoyan.bean.vo.user;

import com.cskaoyan.bean.po.Collect;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 用户管理-会员收藏VO
 *
 * @author yemingfei
 * @since 2022/06/05 19:36
 */
@NoArgsConstructor
@Data
public class CollectData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<Collect> list;

    public CollectData(Integer total, Integer pages, Integer limit, Integer page, List<Collect> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
