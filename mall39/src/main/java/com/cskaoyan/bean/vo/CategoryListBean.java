package com.cskaoyan.bean.vo;

import com.sun.org.apache.xml.internal.dtm.ref.sax2dtm.SAX2DTM2;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;


/**
 * @param
 * @Date: 2022/6/6 10:50
 * @Description:商品类目列表
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class CategoryListBean {
    private Integer value;
    private String label;
    private List<Children> children;
}
