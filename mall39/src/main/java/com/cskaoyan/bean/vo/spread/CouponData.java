package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/05 20:09
 */
@Data
@NoArgsConstructor
public class CouponData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<CouponDataListBean> list;

    public static CouponData data(long total, Integer pages, Integer limit, Integer page, List<CouponDataListBean> list) {
        CouponData couponData = new CouponData();
        couponData.setTotal((int)total);
        couponData.setPages(pages);
        couponData.setLimit(limit);
        couponData.setPage(page);
        couponData.setList(list);
        return couponData;
    }
}
