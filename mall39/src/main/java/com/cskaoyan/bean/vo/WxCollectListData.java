package com.cskaoyan.bean.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * 小程序-查看收藏返回的VO
 *
 * @author yemingfei
 * @since 2022/06/08 20:14
 */
@NoArgsConstructor
@Data
public class WxCollectListData {

    /**
     * total : 21
     * pages : 3
     * limit : 10
     * page : 1
     */
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<WxCollectListDataListBean> list;

    public WxCollectListData(Integer total, Integer pages, Integer limit, Integer page, List<WxCollectListDataListBean> list) {
        this.total = total;
        this.pages = pages;
        this.limit = limit;
        this.page = page;
        this.list = list;
    }
}
