package com.cskaoyan.bean.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author karlfu
 * @since 2022/06/06 23:02
 */

@NoArgsConstructor
@Data
public class OrderStatVORow {

    private Double amount;
    private Integer orders;
    private Integer customers;
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private Double pcr;
}
