package com.cskaoyan.bean.vo.spread;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/06 17:08
 */
@Data
@NoArgsConstructor
public class ListuserData {
    private Integer total;
    private Integer pages;
    private Integer limit;
    private Integer page;
    private List<ListuserDataListBean> list;

    public static ListuserData data(long total, Integer pages, Integer limit, Integer page, List<ListuserDataListBean> list) {
        ListuserData listuserData = new ListuserData();
        listuserData.setTotal((int)total);
        listuserData.setPages(pages);
        listuserData.setLimit(limit);
        listuserData.setPage(page);
        listuserData.setList(list);
        return listuserData;
    }

}
