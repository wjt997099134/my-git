package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @param
 * @Date: 2022/6/5 20:22
 * @Description:商品页面显示
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class GoodsData {
    private Integer total;
    private Integer page;
    private Integer pages;
    private Integer limit;
    private List<GoodsDataListBean> list;
}
