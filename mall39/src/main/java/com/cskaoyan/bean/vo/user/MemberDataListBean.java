package com.cskaoyan.bean.vo.user;

import com.sun.org.apache.xpath.internal.operations.Bool;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * 用户管理-会员管理VO-会员信息
 *
 * @author yemingfei
 * @since 2022/06/05 15:27
 */
@NoArgsConstructor
@Data
public class MemberDataListBean {
    // 会员id
    private Integer id;
    // 会员名
    private String username;
    // 密码
    private String password;
    // 性别 0 未知， 1男， 1 女
    private Byte gender;
    // 最近一次登录时间
    private Date lastLoginTime;
    // 最近一次登录的ip地址
    private String lastLoginIp;
    // 会员等级 0 普通用户，1 VIP用户，2 高级VIP用户
    private Byte userLevel;
    // 会员昵称
    private String nickname;
    // 手机号码
    private String mobile;
    // 头像图片
    private String avatar;
    // 微信登录id
    private String weixinOpenid;
    // 微信登录会话id
    private String sessionKey;
    // 状态 0 可用, 1 禁用, 2 注销
    private Byte status;
    // 创建时间
    private Date addTime;
    // 更新时间
    private Date updateTime;
    // 是否逻辑删除 0-不删
    private Boolean deleted;

    public MemberDataListBean(Integer id, String username, String password, Byte gender, Date lastLoginTime, String lastLoginIp, Byte userLevel, String nickname, String mobile, String avatar, String weixinOpenid, String sessionKey, Byte status, Date addTime, Date updateTime, Boolean deleted) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.gender = gender;
        this.lastLoginTime = lastLoginTime;
        this.lastLoginIp = lastLoginIp;
        this.userLevel = userLevel;
        this.nickname = nickname;
        this.mobile = mobile;
        this.avatar = avatar;
        this.weixinOpenid = weixinOpenid;
        this.sessionKey = sessionKey;
        this.status = status;
        this.addTime = addTime;
        this.updateTime = updateTime;
        this.deleted = deleted;
    }
}
