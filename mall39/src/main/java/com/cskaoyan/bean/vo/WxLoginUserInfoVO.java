package com.cskaoyan.bean.vo;

import lombok.Data;

/**
 * @ClassName: WxLoginUserInfoVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/7 20:52
 */

@Data
public class WxLoginUserInfoVO {
    private String avatarUrl;
    private String nickName;

    public WxLoginUserInfoVO() {
    }

    public WxLoginUserInfoVO(String avatarUrl, String nickName) {
        this.avatarUrl = avatarUrl;
        this.nickName = nickName;
    }
}