package com.cskaoyan.bean.vo;


import com.google.gson.annotations.SerializedName;

import java.util.List;

public class KeywordBaseVO {

    private int errno;
    private DataEntity data;
    private String errmsg;


    public void setErrno(int errno) {
        this.errno = errno;
    }

    public void setData(DataEntity data) {
        this.data = data;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public int getErrno() {
        return errno;
    }

    public DataEntity getData() {
        return data;
    }

    public String getErrmsg() {
        return errmsg;
    }

    public class DataEntity {

        private int total;
        private int pages;
        private int limit;
        private int page;
        private List<ListEntity> list;

        public void setTotal(int total) {
            this.total = total;
        }

        public void setPages(int pages) {
            this.pages = pages;
        }

        public void setLimit(int limit) {
            this.limit = limit;
        }

        public void setPage(int page) {
            this.page = page;
        }

        public void setList(List<ListEntity> list) {
            this.list = list;
        }

        public int getTotal() {
            return total;
        }

        public int getPages() {
            return pages;
        }

        public int getLimit() {
            return limit;
        }

        public int getPage() {
            return page;
        }

        public List<ListEntity> getList() {
            return list;
        }

        public class ListEntity {

            private Boolean isDefault;
            private boolean deleted;
            private String addTime;
            private int sortOrder;
            private String updateTime;
            private int id;
            private String keyword;
            private boolean isHot;
            private String url;

            public void setIsDefault(boolean isDefault) {
                this.isDefault = isDefault;
            }

            public void setDeleted(boolean deleted) {
                this.deleted = deleted;
            }

            public void setAddTime(String addTime) {
                this.addTime = addTime;
            }

            public void setSortOrder(int sortOrder) {
                this.sortOrder = sortOrder;
            }

            public void setUpdateTime(String updateTime) {
                this.updateTime = updateTime;
            }

            public void setId(int id) {
                this.id = id;
            }

            public void setKeyword(String keyword) {
                this.keyword = keyword;
            }

            public void setIsHot(boolean isHot) {
                this.isHot = isHot;
            }

            public void setUrl(String url) {
                this.url = url;
            }

            public boolean isDefault() {
                return isDefault;
            }

            public boolean isDeleted() {
                return deleted;
            }

            public String getAddTime() {
                return addTime;
            }

            public int getSortOrder() {
                return sortOrder;
            }

            public String getUpdateTime() {
                return updateTime;
            }

            public int getId() {
                return id;
            }

            public String getKeyword() {
                return keyword;
            }

            public boolean isHot() {
                return isHot;
            }

            public String getUrl() {
                return url;
            }
        }
    }
}
