package com.cskaoyan.bean.vo;


import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @param
 * @Date: 2022/6/6 10:55
 * @Description:品牌列表类
 * @Author: 李宇浩
 * @return
 */
@NoArgsConstructor
@Data
public class BrandListBean {
    private Integer value;
    private String label;
}
