package com.cskaoyan.bean.vo;

import com.cskaoyan.bean.po.OrderGoods;
import lombok.Data;

import java.util.List;

/**
 * @ClassName: WxAfterSaleDetailVO
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 11:02
 */

@Data
public class WxAfterSaleDetailVO {
    private WxAfterSaleDetailAftersaleVO aftersale;
    private WxAfterSaleDetailOrderVO order;
    private List<OrderGoods> orderGoods;

    public WxAfterSaleDetailVO() {
    }

    public WxAfterSaleDetailVO(WxAfterSaleDetailAftersaleVO aftersale, WxAfterSaleDetailOrderVO order, List<OrderGoods> orderGoods) {
        this.aftersale = aftersale;
        this.order = order;
        this.orderGoods = orderGoods;
    }
}