package com.cskaoyan.bean.vo;

import lombok.Data;

import java.util.List;

/**
 * 系统管理
 * 角色管理
 * admin/role/options 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/05 21:12
 */

@Data
public class SystemRoleOptionsVO {
    Integer limit;
    Integer page;
    Integer pages;
    Integer total;
    List<RoleOfSystemRoleOptionsVO> list;
}
