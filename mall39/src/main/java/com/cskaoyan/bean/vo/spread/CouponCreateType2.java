package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author CR
 * @since 2022/06/08 19:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CouponCreateType2 {
    private String code;
    private Date addTime;
    private Integer discount;
    private Integer timeType;
    private String[] goodsValue;
    private Date updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private String name;
    private Integer limit;
    private Integer days;
    private Integer id;
    private String tag;
    private String desc;
    private Integer status;
}
