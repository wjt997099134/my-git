package com.cskaoyan.bean.vo.spread;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/05 20:10
 */

public class CouponDataListBean {

    /**
     * addTime : 2022-06-04 23:53:21
     * discount : 0
     * timeType : 0
     * goodsValue : []
     * updateTime : 2022-06-04 23:53:21
     * type : 0
     * goodsType : 0
     * total : 0
     * min : 0
     * deleted : false
     * name : 1
     * limit : 1
     * days : 0
     * id : 110
     * tag : 1
     * desc : 1
     * status : 0
     */
    private String addTime;
    private Integer discount;
    private Integer timeType;
    private String[] goodsValue;
    private String updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private boolean deleted;
    private String name;
    private Integer limit;
    private Integer days;
    private Integer id;
    private String tag;
    private String desc;
    private Integer status;

    public void setAddTime(String addTime) {
        this.addTime = addTime;
    }

    public void setDiscount(Integer discount) {
        this.discount = discount;
    }

    public void setTimeType(Integer timeType) {
        this.timeType = timeType;
    }

    public void setGoodsValue(String[] goodsValue) {
        this.goodsValue = goodsValue;
    }

    public void setUpdateTime(String updateTime) {
        this.updateTime = updateTime;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public void setGoodsType(Integer goodsType) {
        this.goodsType = goodsType;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public void setMin(Integer min) {
        this.min = min;
    }

    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public String getAddTime() {
        return addTime;
    }

    public Integer getDiscount() {
        return discount;
    }

    public Integer getTimeType() {
        return timeType;
    }

    public String[] getGoodsValue() {
        return goodsValue;
    }

    public String getUpdateTime() {
        return updateTime;
    }

    public Integer getType() {
        return type;
    }

    public Integer getGoodsType() {
        return goodsType;
    }

    public Integer getTotal() {
        return total;
    }

    public Integer getMin() {
        return min;
    }

    public boolean isDeleted() {
        return deleted;
    }

    public String getName() {
        return name;
    }

    public Integer getLimit() {
        return limit;
    }

    public Integer getDays() {
        return days;
    }

    public Integer getId() {
        return id;
    }

    public String getTag() {
        return tag;
    }

    public String getDesc() {
        return desc;
    }

    public Integer getStatus() {
        return status;
    }
}
