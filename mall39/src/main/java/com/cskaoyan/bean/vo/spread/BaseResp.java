package com.cskaoyan.bean.vo.spread;

/**
 * @author CR
 * @since 2022/06/05 17:13
 */

public class BaseResp {

    /**
     * errno : 0
     * errmsg : 成功
     */
    private Integer errno;
    private String errmsg;

    public static BaseResp ok() {
        BaseResp baseResp = new BaseResp();
        baseResp.setErrno(0);
        baseResp.setErrmsg("成功");
        return baseResp;
    }

    public static BaseResp invalidRequest(String msg) {
        BaseResp baseResp = new BaseResp();
        baseResp.setErrno(740);
        baseResp.setErrmsg(msg);
        return baseResp;
    }

    public static BaseResp invalidRequest1(String msg) {
        BaseResp baseResp = new BaseResp();
        baseResp.setErrno(742);
        baseResp.setErrmsg(msg);
        return baseResp;
    }

    public void setErrno(Integer errno) {
        this.errno = errno;
    }

    public void setErrmsg(String errmsg) {
        this.errmsg = errmsg;
    }

    public Integer getErrno() {
        return errno;
    }

    public String getErrmsg() {
        return errmsg;
    }
}
