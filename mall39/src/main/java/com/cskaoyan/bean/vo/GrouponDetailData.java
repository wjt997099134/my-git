package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.*;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.List;


@Data
@NoArgsConstructor
public class GrouponDetailData {

    private GrouponRules groupon;
    private List<User> joiners;
    private GrouponDetailData.OrderBean orderInfo;
    private List<OrderGoodsBean> orderGoods;
    private GrouponRules rules;
    private Boolean active;
    @Data
    @NoArgsConstructor
    public static class OrderBean {
        private BigDecimal goodsPrice;
        private BigDecimal freightPrice;
        private BigDecimal actualPrice;

        public OrderBean(BigDecimal goodsPrice, BigDecimal freightPrice, BigDecimal actualPrice) {
            this.goodsPrice = goodsPrice;
            this.freightPrice = freightPrice;
            this.actualPrice = actualPrice;
        }
    }
    @Data
    @NoArgsConstructor
    public static class OrderGoodsBean{

        private String picUrl;
        private String goodsName;
        private Integer number;
        private String[] goodsSpecificationValues;
        private BigDecimal retailPrice;
    }
}
