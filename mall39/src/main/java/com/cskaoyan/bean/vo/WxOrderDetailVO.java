package com.cskaoyan.bean.vo;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * 微信小程序订单模块
 * wx/order/detail 的VO
 *
 * @author WuHuaguo
 * @since 2022/06/07 22:50
 */

@Data
public class WxOrderDetailVO {
    String[] expressInfo;
    OrderInnerVO orderInfo;
    List<OrderGoodsInnerVO> orderGoods;

    @Data
    public static class OrderInnerVO {
        String consignee;
        String address;
        Date addTime;
        String orderSn;
        BigDecimal actualPrice;
        String mobile;
        String message;
        String orderStatusText;
        Short aftersaleStatus;
        BigDecimal goodsPrice;
        BigDecimal couponPrice;
        BigDecimal freightPrice;
        Integer id;
        WxOrderHandleOptionInnerVO handleOption;
    }

    @Data
    public static class OrderGoodsInnerVO {
        Integer id;
        Integer orderId;
        Integer goodsId;
        String goodsName;
        String goodsSn;
        Integer productId;
        Short number;
        BigDecimal price;
        String[] specifications;
        String picUrl;
        Integer comment;
        Date addTime;
        Date updateTime;
        Boolean deleted;
    }

}
