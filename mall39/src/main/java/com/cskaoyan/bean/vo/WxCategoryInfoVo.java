package com.cskaoyan.bean.vo;


import com.cskaoyan.bean.po.Category;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

@Data
@NoArgsConstructor
public class WxCategoryInfoVo {
    List<Category> brotherCategory;
    Category currentCategory;
    Category parentCategory;
}
