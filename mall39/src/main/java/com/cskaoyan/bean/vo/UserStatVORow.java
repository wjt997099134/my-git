package com.cskaoyan.bean.vo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author karlfu
 * @since 2022/06/06 22:23
 */

@NoArgsConstructor
@Data
public class UserStatVORow {
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date day;
    private Integer users;
}
