package com.cskaoyan.bean.vo.spread;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;
import java.util.List;

/**
 * @author CR
 * @since 2022/06/05 21:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CouponCreate {

    /**
     * addTime : 2022-06-05 21:13:40
     * discount : 5
     * timeType : 0
     * goodsValue : []
     * updateTime : 2022-06-05 21:13:40
     * type : 0
     * goodsType : 0
     * total : 10
     * min : 10
     * name : aa
     * limit : 1
     * days : 10
     * id : 114
     * tag : aa
     * desc : aa
     * status : 0
     */
    private Date addTime;
    private Integer discount;
    private Integer timeType;
    private String[] goodsValue;
    private Date updateTime;
    private Integer type;
    private Integer goodsType;
    private Integer total;
    private Integer min;
    private String name;
    private Integer limit;
    private Integer days;
    private Integer id;
    private String tag;
    private String desc;
    private Integer status;

}
