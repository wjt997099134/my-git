package com.cskaoyan.util;

import com.cskaoyan.bean.vo.BaseRespVo;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

public class ValidationUtils {
    public static BaseRespVo valid(BindingResult bindingResult) {
        if (bindingResult.hasFieldErrors()) {// 如果参数有错，则没通过校验
            FieldError fieldError = bindingResult.getFieldError();
            // 成员变量名 → 请求参数名
            String field = fieldError.getField();
            // rejectedValue
            Object rejectedValue = fieldError.getRejectedValue();
            // defaultMessage
            String defaultMessage = fieldError.getDefaultMessage();
            return BaseRespVo.invalidParameter(defaultMessage);
        }
        return null;
    }

}
