package com.cskaoyan.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

/**
 * @author karlfu
 * @since 2022/06/05 20:26
 */

@Data
@ConfigurationProperties(prefix = "cskaoyan.storage")
@PropertySource("classpath:application.yml")
public class StorageProperties {
    String storagePath;
    String netPath;
}
