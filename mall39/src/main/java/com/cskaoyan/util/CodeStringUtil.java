package com.cskaoyan.util;


/**
 * @param
 * @Date: 2022/6/9 10:06
 * @Description:处理数组长度工具类
 * @Author: 李宇浩
 * @return
 */
public class CodeStringUtil {

    private static final int INIT_CAPACITY = 1;


    public static String[] init(String code) {
        String[] newStr = new String[INIT_CAPACITY];
        newStr[0] = code;
        return newStr;
    }

    /**
     * 数组长度自增1
     *
     * @return
     */
    public static String[] addLength(String[] str, String code) {
        String[] newStr = new String[str.length + 1];
        System.arraycopy(str, 0, newStr, 0, str.length);
        newStr[str.length] = code;
        return newStr;
    }

    /**
     * 数组长度自减1
     */
    public static String[] subLength(String[] str) {
        if (str.length != 0) {
            String[] newStr = new String[str.length - 1];
            for (int i = 1; i < str.length; i++) {
                newStr[i - 1] = str[i];
            }
            return newStr;
        }
        return str;
    }
}
