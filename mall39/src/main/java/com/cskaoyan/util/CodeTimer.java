package com.cskaoyan.util;

import com.cskaoyan.service.CodeService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
@EnableScheduling
public class CodeTimer {

    @Autowired
    CodeService codeService;

    String phone;

    public void setPhone(String phone) {
        this.phone = phone;
    }

//    Boolean enabled = false;

    @Scheduled(cron = "0/300 * * * * *")
    public void removeCode() {
        if (StringUtils.isNotEmpty(phone)) {
            codeService.removeCode(phone);
        }
        phone = null;
    }
}
