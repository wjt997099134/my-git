package com.cskaoyan.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;

import java.math.BigDecimal;

@Data
@ConfigurationProperties(prefix = "cskaoyan.constant")
@PropertySource("classpath:application.yml")
public class ConstantProperties {
    String categoryFatherLevel;
    String categorySonLevel;
    Integer categoryFatherPid;
    Integer categoryId;
    Integer pagesDefault;
    Integer pageDefault;
    Integer failCode1;
    Integer failCode2;
    Integer failCode3;
    Integer failCode4;
    Integer failCode5;
    Integer successCode;
    Integer provinceType;
    Boolean notDeleted;
    Short goodsShipStatus;
    Short goodsRefundStatus;
    Integer logLoginType;
    Integer defaultSortOrder;
    Integer type1;
    Integer type2;
    Integer type3;
    Short orderNotPay;
    Short orderCancel;
    Short orderPay;
    Short orderRefund;
    Short orderRefunded;
    Short orderShip;
    Short orderReceive;
    Byte commentTypeGoods;
    Byte commentTypeTopic;
    BigDecimal integralPrice;
    BigDecimal freightPrice;
    Short couponUserStatus1;
}
