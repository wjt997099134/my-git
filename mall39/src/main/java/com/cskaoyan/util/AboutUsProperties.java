package com.cskaoyan.util;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.PropertySource;


@Data
@ConfigurationProperties(prefix = "cskaoyan.aboutus")
@PropertySource("classpath:application.yml")
public class AboutUsProperties {
    String qq;
    String address;
    String phone;
    Double latitude;
    String name;
    Double longitude;
}
