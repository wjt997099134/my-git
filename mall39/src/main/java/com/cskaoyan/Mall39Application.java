package com.cskaoyan;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.cskaoyan.mapper")
public class Mall39Application {
    public static void main(String[] args) {
        SpringApplication.run(Mall39Application.class, args);
    }

}
