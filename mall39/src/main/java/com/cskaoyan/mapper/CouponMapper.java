package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.po.CouponExample;
import com.cskaoyan.bean.po.CouponUser;
import com.cskaoyan.bean.vo.WxHomeListVo;
import com.cskaoyan.bean.vo.spread.WxCouponDataListBean;
import com.cskaoyan.bean.vo.spread.WxMyCouponDataListBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CouponMapper {
    long countByExample(CouponExample example);

    int deleteByExample(CouponExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Coupon record);

    int insertSelective(Coupon record);

    List<Coupon> selectByExample(CouponExample example);

    Coupon selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Coupon record, @Param("example") CouponExample example);

    int updateByExample(@Param("record") Coupon record, @Param("example") CouponExample example);

    int updateByPrimaryKeySelective(Coupon record);

    int updateByPrimaryKey(Coupon record);

    List<WxHomeListVo.WxHomeCouponVo> queryWxHomeCoupon();

    List<WxCouponDataListBean> queryCoupons();

    Coupon queryCouponById(Integer couponId);

    Integer queryCouponIdByCode(String couponCode);

    List<Coupon> queryAllCoupons();

    int queryCouponsByCode(String couponCode);

    // void updateById(Integer id);
}