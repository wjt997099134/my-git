package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Cart;
import com.cskaoyan.bean.po.CartExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CartMapper {
    long countByExample(CartExample example);

    int deleteByExample(CartExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Cart record);

    int insertSelective(Cart record);

    List<Cart> selectByExample(CartExample example);

    Cart selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Cart record, @Param("example") CartExample example);

    int updateByExample(@Param("record") Cart record, @Param("example") CartExample example);

    int updateByPrimaryKeySelective(Cart record);

    int updateByPrimaryKey(Cart record);

    Cart queryCartInfoById(Integer cartId);


    // 添加购物车并返回id
    int insertAndFetchId(Cart cart);

    // 购物车列表订单计数
    int querryCountByUserId(@Param("userId") Integer id);

    // 获取购物车商品总量
    Integer countGoodsByUserId(Integer id);

    // 获取购物车被选中的商品总量
    Integer countCheckedGoodsByUserId(Integer id);

    // 获取购物车商品总价
    Double countGoodsAmountByUserId(Integer id);

    // 获取购物车被选中的商品总价
    Double countCheckedAmountByUserId(Integer id);

    // 根据商品id更新选中状态
    void updateCheckById(@Param("id") Integer productId,@Param("isCheck") Integer check);

    // 删除商品
    void updateDeleteById(@Param("productId") Integer productId,@Param("userId") Integer userId);

    // 更新订单库存
    void updateNumberById(Integer id,Integer number);


}