package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Aftersale;
import com.cskaoyan.bean.po.AftersaleExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AftersaleMapper {
    long countByExample(AftersaleExample example);

    int deleteByExample(AftersaleExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Aftersale record);

    int insertSelective(Aftersale record);

    List<Aftersale> selectByExample(AftersaleExample example);

    Aftersale selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Aftersale record, @Param("example") AftersaleExample example);

    int updateByExample(@Param("record") Aftersale record, @Param("example") AftersaleExample example);

    int updateByPrimaryKeySelective(Aftersale record);

    int updateByPrimaryKey(Aftersale record);
}