package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.SearchHistory;
import com.cskaoyan.bean.po.SearchHistoryExample;
import com.cskaoyan.bean.vo.SearchIndexVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SearchHistoryMapper {
    long countByExample(SearchHistoryExample example);

    int deleteByExample(SearchHistoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(SearchHistory record);

    int insertSelective(SearchHistory record);

    List<SearchHistory> selectByExample(SearchHistoryExample example);

    SearchHistory selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") SearchHistory record, @Param("example") SearchHistoryExample example);

    int updateByExample(@Param("record") SearchHistory record, @Param("example") SearchHistoryExample example);

    int updateByPrimaryKeySelective(SearchHistory record);

    int updateByPrimaryKey(SearchHistory record);

    int queryHistoryByUserIdAndkeyword(@Param("id") Integer id, @Param("keyword") String keyword);

    void updateByDemo(SearchHistory searchHistory);

    List<SearchIndexVO.HistoryKeyword> selectHistoryKeywordByExample(SearchHistoryExample example);

    List<String> selectHistoryKeywordHelperByExample(SearchHistoryExample example);
}