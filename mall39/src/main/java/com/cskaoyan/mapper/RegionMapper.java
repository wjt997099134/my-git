package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Region;
import com.cskaoyan.bean.po.RegionExample;
import com.cskaoyan.bean.vo.RegionData;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface RegionMapper {
    long countByExample(RegionExample example);

    int deleteByExample(RegionExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Region record);

    int insertSelective(Region record);

    List<Region> selectByExample(RegionExample example);

    Region selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Region record, @Param("example") RegionExample example);

    int updateByExample(@Param("record") Region record, @Param("example") RegionExample example);

    int updateByPrimaryKeySelective(Region record);

    int updateByPrimaryKey(Region record);

    List<RegionData.RegionDataListBean> queryProvinceListByType(Integer type);

    List<RegionData.RegionDataListBean.RegionDataChildrenBean> queryCityListByPid(Integer pid);

    List<RegionData.RegionDataListBean.RegionDataChildrenBean.RegionDataChildrenListBean> queryDistrictListByPid(Integer pid);

    Integer queryRegionIdByName(String name);

    int queryRegionIdByNameAndPid(@Param("name") String name, @Param("pid") String pid);
}