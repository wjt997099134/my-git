package com.cskaoyan.mapper;

import com.cskaoyan.bean.bo.WxAddressSaveBo;
import com.cskaoyan.bean.po.Address;
import com.cskaoyan.bean.po.AddressExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AddressMapper {
    long countByExample(AddressExample example);

    int deleteByExample(AddressExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Address record);

    int insertSelective(Address record);

    List<Address> selectByExample(AddressExample example);

    Address selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Address record, @Param("example") AddressExample example);

    int updateByExample(@Param("record") Address record, @Param("example") AddressExample example);

    int updateByPrimaryKeySelective(Address record);

    int updateByPrimaryKey(Address record);

    String queryRegionNameById(int id);

    void deleteAddressByIdBySetDeleted(@Param("id") Integer id);

    /**
     * 插入一条地址数据并返回自增的id
     * @param newAddress
     * @return
     */
    int insertAddressRetureId(Address newAddress);

    void updateToNotDefault(int newAddressId);
}
