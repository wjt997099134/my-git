package com.cskaoyan.mapper;

public interface DashboardMapper {
    int queryGoodsTotal();

    int queryUserTotal();

    int queryProductTotal();

    int queryOrderTotal();
}
