package com.cskaoyan.mapper;

import com.cskaoyan.bean.vo.SystemPermissionsVO;

import java.util.List;

public interface SystemPermissionsMapper {

    // 通过type=1查询label
    List<SystemPermissionsVO.Child1> queryLabelByType1(Integer type1);

    // 通过pid查询对应的id（二级权限的id）
    List<Integer> queryIdByPid(int pid);

    // 通过id查询label
    SystemPermissionsVO.Child1.Child2 queryLabelById(Integer id);

    // 通过pid查询api、label、permission
    List<SystemPermissionsVO.Child1.Child2.Child3> queryLabelAndApiAndPermissionByPid(Integer pid);
}
