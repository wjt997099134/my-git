package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.GrouponRules;
import com.cskaoyan.bean.po.GrouponRulesExample;
import com.cskaoyan.bean.vo.GrouponData;
import com.cskaoyan.bean.vo.WxGoodsDetailVo;
import org.apache.ibatis.annotations.Param;

import java.math.BigDecimal;
import java.util.List;

public interface GrouponRulesMapper {
    long countByExample(GrouponRulesExample example);

    int deleteByExample(GrouponRulesExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(GrouponRules record);

    int insertSelective(GrouponRules record);

    List<GrouponRules> selectByExample(GrouponRulesExample example);

    GrouponRules selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") GrouponRules record, @Param("example") GrouponRulesExample example);

    int updateByExample(@Param("record") GrouponRules record, @Param("example") GrouponRulesExample example);

    int updateByPrimaryKeySelective(GrouponRules record);

    int updateByPrimaryKey(GrouponRules record);

    List<GrouponData.GrouponDataBean> queryGrouponInfo();

    List<WxGoodsDetailVo.GrouponDetailBean> queryGrouponDetailInfo(Integer id);

    // 根据goodsId拿到discount
    BigDecimal selectByGoodsId(Integer goodsId);
}