package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Category;
import com.cskaoyan.bean.po.CategoryExample;
import com.cskaoyan.bean.vo.CategoryDataChildrenBean;
import com.cskaoyan.bean.vo.CategoryDataListBean;
import com.cskaoyan.bean.vo.WxHomeListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CategoryMapper {
    long countByExample(CategoryExample example);

    int deleteByExample(CategoryExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Category record);

    int insertSelective(Category record);

    List<Category> selectByExample(CategoryExample example);

    Category selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Category record, @Param("example") CategoryExample example);

    int updateByExample(@Param("record") Category record, @Param("example") CategoryExample example);

    int updateByPrimaryKeySelective(Category record);

    int updateByPrimaryKey(Category record);

    List<CategoryDataListBean> queryCategoryByLevel(String level);

    List<CategoryDataChildrenBean> queryCategoryByPid(Integer pid);

    Byte queryMaxSortOrder();

    Category selectCategoryByNameAndDeleted(@Param("name") String name, @Param("deleted") boolean b);

    int queryCategoryByIdAndName(@Param("id") Integer id, @Param("name") String name);

    Byte queryCategorySortOrderById(Integer id);

    int queryCategoryByLevelAndNameAndDeleted(@Param("level") String level, @Param("name") String name, @Param("deleted") boolean deleted);

    List<WxHomeListVo.WxHomeCategoryVo> queryWxHomeCategory();

    List<WxHomeListVo.WxHomeFloorGoodsVo> queryWxHomeFloorGoods();

    List<Category> queryCategoriesByLevel(String categoryFatherLevel);

    List<Category> queryCategoriesByPid(Integer id);
}