package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Groupon;
import com.cskaoyan.bean.po.GrouponExample;
import com.cskaoyan.bean.vo.GrouponOrderVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GrouponMapper {
    long countByExample(GrouponExample example);

    int deleteByExample(GrouponExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Groupon record);

    int insertSelective(Groupon record);

    List<Groupon> selectByExample(GrouponExample example);

    Groupon selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Groupon record, @Param("example") GrouponExample example);

    int updateByExample(@Param("record") Groupon record, @Param("example") GrouponExample example);

    int updateByPrimaryKeySelective(Groupon record);

    int updateByPrimaryKey(Groupon record);

    int queryCountGrouponMember(Integer rulesId);

    List<Integer> queryUserIdByGrouponId(Integer grouponId);

    List<Groupon> queryGrouponByUserId(Integer id);

    List<Groupon> queryGrouponByCreatorUserId(Integer id);

    int queryCountMember(Integer grouponId);

    Integer queryOrderId(Integer grouponId);

    Groupon queryGrouponLinkByGrouponId(Integer grouponId);

    Integer queryRulesIdByGrouponIdAndUserId(@Param("grouponId") Integer grouponId,@Param("userId")Integer userId);

    int queryCountGroupon(Integer grouponId);

    void updateByGrouponId(Integer grouponId);

}