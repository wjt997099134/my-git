package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.vo.spread.*;
import org.apache.ibatis.annotations.Param;

import java.util.Date;
import java.util.List;

public interface SpreadMapper {
    List<AdDataListBean> queryAds(@Param("sort") String sort, @Param("order") String order, @Param("name") String name, @Param("content") String content);

    void insertAd(AdCreate adCreate);

    void updateAd(AdCreate adCreate1);

    void deleteAd(Integer id);

    List<CouponDataListBean> queryCoupons(@Param("sort") String sort, @Param("order") String order, @Param("name") String name, @Param("type") Integer type, @Param("status") Integer status);

    void insertCoupon(CouponCreate couponCreate);

    void insertCoupon1(CouponCreate couponCreate, String startTime, String endTime);

    void updateCoupon(CouponUpdate couponUpdate);

    void deleteCoupon(Integer id);

    Coupon queryCouponById(Integer id);

    List<ListuserDataListBean> queryListusers(@Param("sort") String sort, @Param("order") String order, @Param("couponId") Integer couponId, @Param("userId") Integer userId, @Param("status") Integer status);


    void insertAd1(AdCreate adCreate);

    void insertCoupon2(CouponCreateType2 couponCreateType2);

    void insertCoupon3(@Param("couponCreateType2") CouponCreateType2 couponCreateType2, @Param("startTime") String startTime, @Param("endTime") String endTime);

    void updateCoupon1(@Param("couponUpdate") CouponUpdate couponUpdate, @Param("code") String code);
}
