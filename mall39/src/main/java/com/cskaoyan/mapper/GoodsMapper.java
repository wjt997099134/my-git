package com.cskaoyan.mapper;

import com.cskaoyan.bean.GoodsListBaseParam;
import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.po.GoodsExample;
import com.cskaoyan.bean.vo.WxCollectListDataListBean;
import com.cskaoyan.bean.vo.WxHomeListVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface GoodsMapper {
    long countByExample(GoodsExample example);

    int deleteByExample(GoodsExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Goods record);

    int insertSelective(Goods record);

    List<Goods> selectByExampleWithBLOBs(GoodsExample example);

    List<Goods> selectByExample(GoodsExample example);

    Goods selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExampleWithBLOBs(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByExample(@Param("record") Goods record, @Param("example") GoodsExample example);

    int updateByPrimaryKeySelective(Goods record);

    int updateByPrimaryKeyWithBLOBs(Goods record);

    int updateByPrimaryKey(Goods record);

    void updateBrandId(Integer id);

    List<WxHomeListVo.WxHomeGoodsVo> queryWxHotGoods();

    List<WxHomeListVo.WxHomeGoodsVo> queryWxNewGoods();

    List<WxHomeListVo.WxHomeGoodsVo> queryWxHomeFloorGoodsBycategoryId(Integer id);

    WxCollectListDataListBean queryWxCollectListDataListBean(Integer id);

    List<WxHomeListVo.WxHomeGoodsVo> queryWxHomeFloorGoodsByKeyword(String keyword);

    List<WxHomeListVo.WxHomeGoodsVo> queryWxHomeGoodsList(GoodsListBaseParam param);

}