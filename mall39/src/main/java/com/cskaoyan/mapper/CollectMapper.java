package com.cskaoyan.mapper;

import com.cskaoyan.bean.bo.WxCollectAddordeleteBo;
import com.cskaoyan.bean.po.Collect;
import com.cskaoyan.bean.po.CollectExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CollectMapper {
    long countByExample(CollectExample example);

    int deleteByExample(CollectExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Collect record);

    int insertSelective(Collect record);

    List<Collect> selectByExample(CollectExample example);

    Collect selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Collect record, @Param("example") CollectExample example);

    int updateByExample(@Param("record") Collect record, @Param("example") CollectExample example);

    int updateByPrimaryKeySelective(Collect record);

    int updateByPrimaryKey(Collect record);

    List<Integer> queryValueIdByUserIdAndType(@Param("userId") Integer userId, @Param("type") Integer type);


    void updateCollectReverseDeleted(@Param("bo") WxCollectAddordeleteBo wxCollectAddordeleteBo, @Param("userId") Integer userId);

    int queryCollect(@Param("bo")WxCollectAddordeleteBo wxCollectAddordeleteBo, @Param("userId") Integer userId);
}