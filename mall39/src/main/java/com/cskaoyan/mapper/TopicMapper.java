package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Topic;
import com.cskaoyan.bean.po.TopicExample;
import com.cskaoyan.bean.vo.WxHomeListVo;
import com.cskaoyan.bean.vo.topic.WxTopicListListVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface TopicMapper {
    long countByExample(TopicExample example);

    int deleteByExample(TopicExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Topic record);

    int insertSelective(Topic record);

    List<Topic> selectByExampleWithBLOBs(TopicExample example);

    List<Topic> selectByExample(TopicExample example);

    Topic selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Topic record, @Param("example") TopicExample example);

    int updateByExampleWithBLOBs(@Param("record") Topic record, @Param("example") TopicExample example);

    int updateByExample(@Param("record") Topic record, @Param("example") TopicExample example);

    int updateByPrimaryKeySelective(Topic record);

    int updateByPrimaryKeyWithBLOBs(Topic record);

    int updateByPrimaryKey(Topic record);

    List<WxHomeListVo.WxHomeTopicVo> queryWxHomeTopic();

    List<WxTopicListListVO> selectWxTopicListNotDeleted(Boolean notDeleted);
}