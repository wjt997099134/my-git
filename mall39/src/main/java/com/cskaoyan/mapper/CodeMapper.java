package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.CodePO;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @param
 * @Date: 2022/6/9 10:21
 * @Description:验证码查询
 * @Author: 李宇浩
 * @return
 */
public interface CodeMapper {

    // 插入验证码数据
    void insert(@Param("cod") CodePO codePO);

    // 查询用户验证码
    CodePO selectByPhone(String phone);

    // 更新用户验证码
    void update(@Param("cod")CodePO codePO);

    // 删除用户验证码
    void deleteByPhone(String phone);
}
