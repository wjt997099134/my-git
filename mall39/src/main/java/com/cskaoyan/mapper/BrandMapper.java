package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.po.BrandExample;
import com.cskaoyan.bean.vo.WxHomeListVo;
import com.cskaoyan.bean.vo.spread.WxBrandDataListBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface BrandMapper {
    long countByExample(BrandExample example);

    int deleteByExample(BrandExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Brand record);

    int insertSelective(Brand record);

    List<Brand> selectByExample(BrandExample example);

    Brand selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Brand record, @Param("example") BrandExample example);

    int updateByExample(@Param("record") Brand record, @Param("example") BrandExample example);

    int updateByPrimaryKeySelective(Brand record);

    int updateByPrimaryKey(Brand record);

    int queryBrandByNameAndDelete(@Param("name") String name,@Param("deleted") boolean deleted);

    Byte queryMaxSortOrder();

    Brand selectBrandByName(@Param("name") String name,@Param("deleted") boolean deleted);

    int queryBrandByNameAndDeleteAndId(@Param("name")String name, @Param("deleted")boolean b, @Param("id") Integer id);

    List<WxHomeListVo.WxHomeBrandVo> queryWxHomeBrand();

    List<WxBrandDataListBean> queryBrands();
}