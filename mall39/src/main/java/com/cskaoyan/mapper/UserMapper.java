package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.vo.OrderDetailData;
import com.cskaoyan.bean.vo.user.AddressDataListBean;
import com.cskaoyan.bean.vo.user.MemberDataListBean;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface UserMapper {
    long countByExample(UserExample example);

    int deleteByExample(UserExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(User record);

    int insertSelective(User record);

    List<User> selectByExample(UserExample example);

    User selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") User record, @Param("example") UserExample example);

    int updateByExample(@Param("record") User record, @Param("example") UserExample example);

    int updateByPrimaryKeySelective(User record);

    int updateByPrimaryKey(User record);


    int queryUserByIdAndDelete(@Param("id") Integer id, @Param("deleted") boolean b);

    MemberDataListBean queryUserById(Integer id);

    OrderDetailData.UserBean queryUserBeanByUserId(Integer userId);

    int selectIdByUsername(String username);
}