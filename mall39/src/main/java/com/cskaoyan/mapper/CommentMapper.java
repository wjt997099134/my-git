package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.Comment;
import com.cskaoyan.bean.po.CommentExample;
import com.cskaoyan.bean.vo.WxCommentData;
import com.cskaoyan.bean.vo.WxCommentVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface CommentMapper {
    long countByExample(CommentExample example);

    int deleteByExample(CommentExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(Comment record);

    int insertSelective(Comment record);

    List<Comment> selectByExample(CommentExample example);

    Comment selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") Comment record, @Param("example") CommentExample example);

    int updateByExample(@Param("record") Comment record, @Param("example") CommentExample example);

    int updateByPrimaryKeySelective(Comment record);

    int updateByPrimaryKey(Comment record);

    List<Comment> queryCommentInfoByGoodsId(Integer id);

    List<Comment> queryCommentData(@Param("valueId") Integer valueId, @Param("type") Integer type);

    List<Comment> queryCommentDataNoPic(@Param("valueId")Integer valueId, @Param("type")Integer type);

    int queryCountComment(@Param("valueId") Integer valueId, @Param("type") Integer type, @Param("hasPicture") Boolean hasPicture);

    int insertSelectiveReturnId(Comment record);
}