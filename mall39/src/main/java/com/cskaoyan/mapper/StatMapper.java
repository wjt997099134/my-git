package com.cskaoyan.mapper;

import com.cskaoyan.bean.vo.GoodsStatVORow;
import com.cskaoyan.bean.vo.OrderStatVORow;
import com.cskaoyan.bean.vo.UserStatVORow;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/06 22:08
 */

public interface StatMapper {
    List<UserStatVORow> queryUserStat();

    List<OrderStatVORow> queryOrderStat();

    List<GoodsStatVORow> queryGoodsStat();
}
