package com.cskaoyan.mapper;

public interface PermissionUrlMapper {
    String queryUrlByPerm(String perm);
}
