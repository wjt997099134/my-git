package com.cskaoyan.mapper;

import com.cskaoyan.bean.po.NoticeAdmin;
import com.cskaoyan.bean.po.NoticeAdminExample;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NoticeAdminMapper {
    long countByExample(NoticeAdminExample example);

    int deleteByExample(NoticeAdminExample example);

    int deleteByPrimaryKey(Integer id);

    int insert(NoticeAdmin record);

    int insertSelective(NoticeAdmin record);

    List<NoticeAdmin> selectByExample(NoticeAdminExample example);

    NoticeAdmin selectByPrimaryKey(Integer id);

    int updateByExampleSelective(@Param("record") NoticeAdmin record, @Param("example") NoticeAdminExample example);

    int updateByExample(@Param("record") NoticeAdmin record, @Param("example") NoticeAdminExample example);

    int updateByPrimaryKeySelective(NoticeAdmin record);

    int updateByPrimaryKey(NoticeAdmin record);
}