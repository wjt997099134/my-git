package com.cskaoyan.config;


import com.cskaoyan.util.ConstantProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(ConstantProperties.class)
public class ConstantConfiguration {
    ConstantProperties properties;

    public ConstantConfiguration(ConstantProperties properties) {
        this.properties = properties;
    }

}
