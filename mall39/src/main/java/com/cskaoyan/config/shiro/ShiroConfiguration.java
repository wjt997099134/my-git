package com.cskaoyan.config.shiro;

import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.spring.security.interceptor.AuthorizationAttributeSourceAdvisor;
import org.apache.shiro.spring.web.ShiroFilterFactoryBean;
import org.apache.shiro.web.mgt.DefaultWebSecurityManager;
import org.apache.shiro.web.session.mgt.DefaultWebSessionManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.LinkedHashMap;

/**
 * @author yemingfei
 * @since 2022/06/07 14:47
 */
@Configuration
public class ShiroConfiguration {

    // filter链
    // filter依赖安全管理器

    @Bean
    public ShiroFilterFactoryBean shiroFilter(DefaultWebSecurityManager securityManager) {
        ShiroFilterFactoryBean shiroFilterFactoryBean = new ShiroFilterFactoryBean();
        shiroFilterFactoryBean.setSecurityManager(securityManager);

        // 修改重定向的地址
        shiroFilterFactoryBean.setLoginUrl("/redirect");

        // 有序的hashmap
        LinkedHashMap<String, String> filterChainDefinitionMap = new LinkedHashMap<>();
        // 登录需要认证
        // 匿名请求放在前面

        filterChainDefinitionMap.put("/redirect", "anon");
        filterChainDefinitionMap.put("/admin/profile/nnotice", "anon");
        filterChainDefinitionMap.put("/admin/auth/login", "anon");
        filterChainDefinitionMap.put("/admin/**", "authc");
        filterChainDefinitionMap.put("/wx/search", "anon");
        filterChainDefinitionMap.put("/wx/auth/**", "anon");
        filterChainDefinitionMap.put("/wx/auth/login", "anon");
        filterChainDefinitionMap.put("/wx/storage", "anon");
        filterChainDefinitionMap.put("/wx/auth/info", "authc");
        filterChainDefinitionMap.put("/wx/cart", "authc");
        filterChainDefinitionMap.put("/wx/order", "authc");
        filterChainDefinitionMap.put("/wx/feedback", "authc");
        filterChainDefinitionMap.put("/wx/address", "authc");
        filterChainDefinitionMap.put("/wx/footprint", "authc");

        shiroFilterFactoryBean.setFilterChainDefinitionMap(filterChainDefinitionMap);
        return shiroFilterFactoryBean;
    }

    @Bean
    public DefaultWebSecurityManager securityManager(AdminRealm realm, AdminSessionManager sessionManager) {
        DefaultWebSecurityManager defaultWebSecurityManager = new DefaultWebSecurityManager();
        // 默认的认证器，授权器也是用这个realm
        defaultWebSecurityManager.setRealm(realm);
        defaultWebSecurityManager.setSessionManager(sessionManager);
        return defaultWebSecurityManager;
    }

    @Bean
    public AuthorizingRealm realm() {
        return new AdminRealm();
    }

    /**
     * 前后端分离-->跨域-->session会变化
     * shiro中的信息通过session维护
     *
     * @return
     */
    @Bean
    public DefaultWebSessionManager sessionManager() {
        return new AdminSessionManager();

    }

    // 用到AspectJ → 使用注解的方式，将权限和url绑定起来
    @Bean
    public AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor(DefaultWebSecurityManager securityManager) {
        AuthorizationAttributeSourceAdvisor authorizationAttributeSourceAdvisor = new AuthorizationAttributeSourceAdvisor();
        authorizationAttributeSourceAdvisor.setSecurityManager(securityManager);
        return authorizationAttributeSourceAdvisor;
    }
}
