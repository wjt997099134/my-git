package com.cskaoyan.config.shiro;

import com.cskaoyan.bean.po.Admin;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.po.UserExample;
import com.cskaoyan.mapper.UserMapper;
import com.cskaoyan.service.SystemService;
import com.cskaoyan.util.Md5Util;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 自定义realm
 *
 * @author yemingfei
 * @since 2022/06/07 15:26
 */
@Component
public class AdminRealm extends AuthorizingRealm {

    @Autowired
    SystemService systemService;

    @Autowired
    UserMapper userMapper;

    // 授权配置
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        // 获得principal-->获得授权信息
        Admin primaryPrincipal = ((Admin) principalCollection.getPrimaryPrincipal());
        // 查询其权限 roleId-->permission
        Integer[] roles = primaryPrincipal.getRoleIds();
        List<String> perms = new ArrayList<>();
        if (roles == null || roles.length == 0){
            perms.add("");
        } else {
            for (Integer role : roles) {
                // 根据角色id查询权限
                List<String> perm = systemService.adminPerms(role);
                perms.addAll(perm);
            }
        }
        SimpleAuthorizationInfo simpleAuthorizationInfo = new SimpleAuthorizationInfo();
        simpleAuthorizationInfo.addStringPermissions(perms);
        return simpleAuthorizationInfo;
    }


    // 认证信息
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken) throws AuthenticationException {
        // 根据subject.login()传入的token获得认证信息
        String type = ((MallToken) authenticationToken).getType();
        // 根据用户名查询密码
        // 获得用户名
        String username = (((MallToken) authenticationToken).getUsername());

        if ("admin".equals(type)) {
            // 查数据库该用户名对应的密码
            List<Admin> admins = systemService.adminLogin(username);
            if (admins != null && admins.size() == 1) {
                Admin admin = admins.get(0);
                // 正确的密码
                String credentials = admin.getPassword();
                // 构造认证信息
                return new SimpleAuthenticationInfo(admin, credentials, this.getName());
            }
        } else if ("wx".equals(type)) {
            UserExample example = new UserExample();
            UserExample.Criteria criteria = example.createCriteria();
            criteria.andDeletedEqualTo(false);
            criteria.andUsernameEqualTo(username);
            List<User> users = userMapper.selectByExample(example);
            if (users != null && users.size() == 1) {
                User user = users.get(0);
                String credentials = user.getPassword();
                return new SimpleAuthenticationInfo(user, credentials, this.getName());
            }
        }
        return null;
    }


}
