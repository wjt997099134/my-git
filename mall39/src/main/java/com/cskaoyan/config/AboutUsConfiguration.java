package com.cskaoyan.config;

import com.cskaoyan.util.AboutUsProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(AboutUsProperties.class)
public class AboutUsConfiguration {
    AboutUsProperties properties;

    public AboutUsConfiguration(AboutUsProperties properties) {
        this.properties = properties;
    }
}
