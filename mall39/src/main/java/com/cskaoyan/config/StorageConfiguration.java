package com.cskaoyan.config;

import com.cskaoyan.util.StorageProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;

/**
 * @author karlfu
 * @since 2022/06/05 20:26
 */

@Configuration
@EnableConfigurationProperties(StorageProperties.class)
public class StorageConfiguration {
    StorageProperties properties;

    public StorageConfiguration(StorageProperties properties) {
        this.properties = properties;
    }
    // 通过方法名作为默认的组件id来指定了
    // 也可以使用@Bean的value属性值来指定
    @Bean
    public CommonsMultipartResolver multipartResolver() {
        return new CommonsMultipartResolver();
    }
}
