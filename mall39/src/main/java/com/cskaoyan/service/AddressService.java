package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxAddressSaveBo;
import com.cskaoyan.bean.vo.WxAddressListVO;
import com.cskaoyan.bean.vo.user.AddressDataListBean;

public interface AddressService {
    WxAddressListVO getAddressList(Integer userId);

    AddressDataListBean addressDetail(Integer id);



    void addressDelete(Integer id);

    /**
     * 保存地址
     * 新建地址，POST的id = 0，返回新建的地址保存到数据库后自增的id值
     * 保存修改的地址信息，POST的id就是地址id,返回的也是该值
     * @param wxAddressSaveBo
     * @return
     */
    Integer addressSave(WxAddressSaveBo wxAddressSaveBo);
}
