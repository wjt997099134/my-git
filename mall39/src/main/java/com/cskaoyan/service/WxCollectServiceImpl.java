package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxCollectAddordeleteBo;
import com.cskaoyan.bean.po.Collect;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.WxCollectListData;
import com.cskaoyan.bean.vo.WxCollectListDataListBean;
import com.cskaoyan.mapper.CollectMapper;
import com.cskaoyan.mapper.GoodsMapper;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author yemingfei
 * @since 2022/06/08 20:21
 */
@Service
public class WxCollectServiceImpl implements WxCollectService {
    @Autowired
    CollectMapper collectMapper;

    @Autowired
    GoodsMapper goodsMapper;

    @Autowired
    ConstantProperties CONSTANT;

    @Override
    public WxCollectListData collectList(Integer type, Integer page, Integer limit) {
        // 分页
        PageHelper.startPage(page, limit);
        // type=0，(valueId)是商品ID；如果type=1，则是专题ID

        // userId and type --该用户的所有收藏，这里type=0,查看的是收藏的商品-->valueId（商品id）
        Subject subject = SecurityUtils.getSubject();
        User primaryPrincipal = (User) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();

        List<Integer> valueIds = collectMapper.queryValueIdByUserIdAndType(userId,type);
        List<WxCollectListDataListBean> list = new ArrayList<>();
        if (type == 0){
            // 商品id查询商品信息封装成要返回的对象
            for (int i = 0; i < valueIds.size(); i++) {
                WxCollectListDataListBean wxCollectListDataListBean =goodsMapper.queryWxCollectListDataListBean(valueIds.get(i));
                list.add(wxCollectListDataListBean);
            }
        }
        PageInfo pageInfo = new PageInfo(list);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        return new WxCollectListData(((int) total),pages,limit,page,list);
    }

    @Override
    public void addordelete(WxCollectAddordeleteBo wxCollectAddordeleteBo) {
        // 如果没有收藏就加入收藏
        // 如果已收藏则取消收藏
        Subject subject = SecurityUtils.getSubject();
        User primaryPrincipal = ((User) subject.getPrincipals().getPrimaryPrincipal());
        Integer userId = primaryPrincipal.getId();
        // 根据userId,valueId，type查询收藏记录
        int count = collectMapper.queryCollect(wxCollectAddordeleteBo,userId);
        Date addTime = new Date();
        Date updateTime = new Date();
        if(count == 0){
            // 没有收藏记录 添加收藏记录
            Collect collect = new Collect(
                    null,userId,
                    wxCollectAddordeleteBo.getValueId(),
                     wxCollectAddordeleteBo.getType(),
                    addTime,updateTime,false
            );
            collectMapper.insert(collect);
        } else {
            // 有收藏记录 置反deleted
            collectMapper.updateCollectReverseDeleted(wxCollectAddordeleteBo,userId);
        }

    }
}

