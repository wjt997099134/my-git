package com.cskaoyan.service;

import com.cskaoyan.bean.bo.*;
import com.cskaoyan.bean.po.Admin;
import com.cskaoyan.bean.po.Role;
import com.cskaoyan.bean.vo.*;

import java.util.List;

public interface SystemService {

    // 管理员管理模块：管理员列表
    SystemAdminVO adminList(BaseParam baseParam, String username);

    // 管理员管理模块：新增管理员
    SystemCreateAdminVO adminCreate(SystemCreateAdminBO createAdminBO);

    // 管理员管理模块：编辑管理员
    SystemUpdateAdminVO adminUpdate(SystemUpdateAdminBO updateAdminBO);

    // 管理员管理模块：删除管理员
    int adminDelete(SystemDeleteAdminBO deleteAdminBO);

    // 角色管理模块：角色列表
    SystemRoleVO roleList(BaseParam baseParam, String name);

    // 角色管理模块：获取角色对应权限和系统全部权限
    SystemPermissionsVO rolePermissionsShow(Integer roleId);

    // 角色管理模块：新增角色的权限
    void rolePermissionsAdd(SystemPermissionsBO permissionsBO);

    // 角色管理模块：新增角色
    RoleOfSystemRoleVO roleCreate(SystemCreateRoleBO createRoleBO);

    // 角色管理模块：修改角色
    int roleUpdate(Role role);

    // 角色管理模块：删除角色
    void roleDelete(Role role);

    // 角色管理模块：展示所有角色信息
    SystemRoleOptionsVO roleOptions();

    // 操作日志模块：获取日志列表
    SystemLogListVO logList(BaseParam baseParam);

    /**
     * 登录模块，根据admin的username查询
     * @param username
     * @return
     */
    List<Admin> adminLogin(String username);

    /**
     * 登录模块
     * 登录成功后返回admin的权限
     * 根据roleId查询对应的权限
     * @param role
     * @return
     */
    List<String> adminPerms(Integer role);

    /**
     * 首页修改密码
     * @param realPassword
     * @param newPassword
     * @param id
     */
    void profilePassword(String realPassword, String newPassword, Integer adminId);

    /**
     * 根据角色id查询角色名
     * @param roleId
     * @return
     */
    String adminRoles(Integer roleId);

    /**
     * 根据权限名查询对应url
     * @param perm
     * @return
     */
    String adminPermUrl(String perm);
}

 