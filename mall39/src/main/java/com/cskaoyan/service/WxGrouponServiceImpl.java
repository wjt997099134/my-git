package com.cskaoyan.service;


import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.NoArgsConstructor;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@Service
public class WxGrouponServiceImpl implements WxGrouponService {
    @Autowired
    GrouponMapper grouponMapper;
    @Autowired
    GrouponRulesMapper grouponRulesMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    OrderMapper orderMapper;
    @Autowired
    OrderGoodsMapper orderGoodsMapper;
    @Autowired
    GoodsAttributeMapper goodsAttributeMapper;
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    CommentMapper commentMapper;
    @Autowired
    IssueMapper issueMapper;
    @Autowired
    GoodsProductMapper goodsProductMapper;
    @Autowired
    GoodsSpecificationMapper goodsSpecificationMapper;
    @Autowired
    CollectMapper collectMapper;
    @Autowired
    ConstantProperties CONSTANT;

    @Override
    public GrouponData grouponList(Integer page, Integer limit) {

        List<GrouponData.GrouponDataBean> grouponList = grouponRulesMapper.queryGrouponInfo();
        for (GrouponData.GrouponDataBean grouponDataBean : grouponList) {
            Goods goods = goodsMapper.selectByPrimaryKey(grouponDataBean.getId());
            grouponDataBean.setRetailPrice(goods.getRetailPrice());
            grouponDataBean.setBrief(goods.getBrief());
            grouponDataBean.setGrouponPrice(goods.getRetailPrice().subtract(grouponDataBean.getDiscount()));
            grouponDataBean.setIsGroupon(true);
        }
        PageHelper.startPage(page, limit);
        PageInfo pageInfo = new PageInfo(grouponList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        GrouponData data = GrouponData.data(((int) total), pages, limit, page, grouponList);
        return data;
    }

    @Override
    public GrouponDetailData grouponDetail(Integer grouponId) {
        GrouponDetailData grouponDetailData = new GrouponDetailData();
        GrouponExample grouponExample = new GrouponExample();
        grouponExample.createCriteria().andGrouponIdEqualTo(grouponId);
        List<Groupon> groupons = grouponMapper.selectByExample(grouponExample);
        ArrayList<User> users = new ArrayList<>();
        for (Groupon gro : groupons) {
            User user = userMapper.selectByPrimaryKey(gro.getUserId());
            users.add(user);
        }
        grouponDetailData.setJoiners(users);
      //Groupon groupons;
        Subject subject = SecurityUtils.getSubject();
        User user = null;
        if (subject.getPrincipals().getPrimaryPrincipal() != null) {
            user = (User) subject.getPrincipals().getPrimaryPrincipal();
        }
        Integer grouponRulesId = grouponMapper.queryRulesIdByGrouponIdAndUserId(grouponId,user.getId());
        GrouponRules groupon = grouponRulesMapper.selectByPrimaryKey(grouponRulesId);
        int count = grouponMapper.queryCountGroupon(grouponId);
        if (count == groupon.getDiscountMember()) {
            groupon.setStatus(((short) 2));
            grouponMapper.updateByGrouponId(grouponId);
        }
        grouponDetailData.setGroupon(groupon);
//    List<User> joiners;
//    Order orderInfo;
        ArrayList<Order> orders = new ArrayList<>();
        for (Groupon gro : groupons) {
            orders.add(orderMapper.selectByPrimaryKey(gro.getOrderId()));
        }
        BigDecimal goodsPrice = BigDecimal.ZERO;
        BigDecimal freightPrice = BigDecimal.ZERO;
        BigDecimal actualPrice = BigDecimal.ZERO;
        for (Order order : orders) {
            goodsPrice = goodsPrice.add(order.getGoodsPrice());
            freightPrice = freightPrice.add(order.getFreightPrice());
            actualPrice = actualPrice.add(order.getActualPrice());
        }
        GrouponDetailData.OrderBean orderBean = new GrouponDetailData.OrderBean(goodsPrice, freightPrice, actualPrice);
        grouponDetailData.setOrderInfo(orderBean);
//    List<OrderGoods> orderGoods;
        int goodsNum = 0;
        BigDecimal retailPrice = BigDecimal.ZERO;
        Boolean tag = true;
        OrderGoodsExample orderGoodsExample = new OrderGoodsExample();
        for (Groupon gro : groupons) {
            orderGoodsExample.createCriteria().andOrderIdEqualTo(gro.getOrderId());
            List<OrderGoods> orderGoods = orderGoodsMapper.selectByExample(orderGoodsExample);
            if (orderGoods.size() != 0) {
                goodsNum += orderGoods.get(0).getNumber();
                if (tag) {
                    retailPrice = retailPrice.add(orderGoods.get(0).getPrice());
                    tag = false;
                }
            }
        }
        List<GrouponDetailData.OrderGoodsBean> orderGoods = orderGoodsMapper.queryOrderGoodsBean(groupons.get(0).getOrderId());
        orderGoods.get(0).setRetailPrice(retailPrice);
        orderGoods.get(0).setNumber(goodsNum);
        grouponDetailData.setOrderGoods(orderGoods);
//    GrouponRules rules;
        GrouponRules rules = grouponRulesMapper.selectByPrimaryKey(grouponRulesId);
        grouponDetailData.setRules(rules);
//    Boolean active;
        grouponDetailData.setActive(true);
        return grouponDetailData;
    }

    @Override
    public MyGrouponData myGroupon(Integer showType) {

        //// 判断是否已经登录
        Subject subject = SecurityUtils.getSubject();
        User user = null;
        if (subject.getPrincipals().getPrimaryPrincipal() != null) {
            user = (User) subject.getPrincipals().getPrimaryPrincipal();
        }
        Integer id = user.getId();
        List<Groupon> grouponList = null;
        if (showType == 0) {
            grouponList = grouponMapper.queryGrouponByCreatorUserId(id);
        } else {
            grouponList = grouponMapper.queryGrouponByUserId(id);
        }
        ArrayList<GrouponOrderVo> grouponOrderVos = new ArrayList<>();
        for (Groupon groupon : grouponList) {
            Order order = orderMapper.selectByPrimaryKey(groupon.getOrderId());
            GrouponRules grouponRules = grouponRulesMapper.selectByPrimaryKey(groupon.getRulesId());
            int joinerCount = grouponMapper.queryCountMember(groupon.getGrouponId());
            Boolean isCreator = false;
            if (groupon.getUserId().equals(groupon.getCreatorUserId())) {
                isCreator = true;
            }
            List<OrderGoods> orderGoods = orderGoodsMapper.queryOrderGoodsByOrderId(order.getId());
            GrouponOrderVo grouponOrderVo = new GrouponOrderVo(groupon.getGrouponId(), groupon.getStatus(), isCreator, order.getOrderSn(), order.getOrderStatus(), grouponRules, groupon, joinerCount, orderGoods, order.getActualPrice());
            grouponOrderVos.add(grouponOrderVo);


        }
        MyGrouponData myGrouponData = new MyGrouponData(grouponOrderVos);
        return new MyGrouponData(grouponOrderVos);
        //    private BigDecimal actualPrice;
    }

    @Override
    public WxGoodsDetailVo joinGroupon(Integer grouponId) {
        Integer orderId = grouponMapper.queryOrderId(grouponId);
        OrderGoodsExample orderGoodsExample = new OrderGoodsExample();
        orderGoodsExample.createCriteria().andOrderIdEqualTo(orderId);
        OrderGoods orderGoods = orderGoodsMapper.selectByExample(orderGoodsExample).get(0);
        Goods goods = goodsMapper.selectByPrimaryKey(orderGoods.getGoodsId());
        WxGoodsDetailVo wxGoodsDetailVo = new WxGoodsDetailVo();
        wxGoodsDetailVo.setGoods(goods);
        // List<GoodsAttribute> attribute;
        List<GoodsAttribute> attribute = goodsAttributeMapper.queryGoodsAttribute(orderGoods.getGoodsId());
        wxGoodsDetailVo.setAttribute(attribute);
        //    Brand brand;
        Brand brand = brandMapper.selectByPrimaryKey(goods.getBrandId());
        wxGoodsDetailVo.setBrand(brand);
        List<Comment> comments = commentMapper.queryCommentInfoByGoodsId(orderGoods.getGoodsId());
        ArrayList<WxCommentVo> wxCommentVos = new ArrayList<>();
        WxGoodsDetailVo.GoodsCommentBean goodsCommentBean = new WxGoodsDetailVo.GoodsCommentBean();
        goodsCommentBean.setCount(comments.size());
        for (Comment comment : comments) {
            User user = userMapper.selectByPrimaryKey(comment.getUserId());
            WxCommentVo wxCommentVo = new WxCommentVo(comment.getId(), comment.getContent(), comment.getAdminContent(), comment.getPicUrls(), comment.getAddTime(), user.getAvatar(), user.getNickname());
            wxCommentVos.add(wxCommentVo);
        }
        if (wxCommentVos.size() < 5) {
            goodsCommentBean.setData(wxCommentVos);
        } else {
            List<WxCommentVo> wxCommentVos1 = wxCommentVos.subList(0, 4);
            goodsCommentBean.setData(wxCommentVos1);
        }
        wxGoodsDetailVo.setComment(goodsCommentBean);
        //    List<Groupon> groupon;
        List<WxGoodsDetailVo.GrouponDetailBean> groupon = grouponRulesMapper.queryGrouponDetailInfo(orderGoods.getGoodsId());
        wxGoodsDetailVo.setGroupon(groupon);
        //    List<Goods> info;
        Goods info = goodsMapper.selectByPrimaryKey(orderGoods.getGoodsId());
        wxGoodsDetailVo.setInfo(info);
        //    List<Issue> issue;
        IssueExample issueExample = new IssueExample();
        issueExample.createCriteria().andDeletedEqualTo(false);
        List<Issue> issue = issueMapper.selectByExample(issueExample);
        wxGoodsDetailVo.setIssue(issue);
        //    List<GoodsProduct> productList;
        GoodsProductExample goodsProductExample = new GoodsProductExample();
        GoodsProductExample.Criteria criteria1 = goodsProductExample.createCriteria();
        criteria1.andDeletedEqualTo(false);
        criteria1.andGoodsIdEqualTo(orderGoods.getGoodsId());
        List<GoodsProduct> productList = goodsProductMapper.selectByExample(goodsProductExample);
        wxGoodsDetailVo.setProductList(productList);
        //    Boolean share;
        wxGoodsDetailVo.setShare(true);
        //    List<GoodsSpecificationsBean> specificationList;
        List<WxGoodsDetailVo.GoodsSpecificationsBean> specificationList = goodsSpecificationMapper.querySpecNameByGoodsId(orderGoods.getGoodsId());
        for (WxGoodsDetailVo.GoodsSpecificationsBean goodsSpecificationsBean : specificationList) {
            List<GoodsSpecification> valueList = goodsSpecificationMapper.querySpecInfoByGoodsIdAndSpec(orderGoods.getGoodsId(), goodsSpecificationsBean.getName());
            goodsSpecificationsBean.setValueList(valueList);
        }
        wxGoodsDetailVo.setSpecificationList(specificationList);
        //    Integer userHasCollect;
        CollectExample collectExample = new CollectExample();
        CollectExample.Criteria criteria2 = collectExample.createCriteria();
        criteria2.andDeletedEqualTo(false);
        criteria2.andValueIdEqualTo(orderGoods.getGoodsId());
        List<Collect> collects = collectMapper.selectByExample(collectExample);
        wxGoodsDetailVo.setUserHasCollect(collects.size());
        wxGoodsDetailVo.setShareUrl(goods.getShareUrl());
        wxGoodsDetailVo.setIsGroupon(true);
        Groupon grouponLink = grouponMapper.queryGrouponLinkByGrouponId(grouponId);
        wxGoodsDetailVo.setGrouponLink(grouponLink);
        return wxGoodsDetailVo;
    }
}
