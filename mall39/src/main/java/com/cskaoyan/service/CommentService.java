package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxCommentPostBO;
import com.cskaoyan.bean.po.Comment;
import com.cskaoyan.bean.vo.CommentListVO;
import com.cskaoyan.bean.vo.WxCommentPostVO;

public interface CommentService {

    CommentListVO getCommentList(Integer page, Integer limit, String sort, String order, Integer userId, Integer valueId);

    void deleteComment(Comment comment);

    WxCommentPostVO postComment(WxCommentPostBO wxCommentPostBO);
}