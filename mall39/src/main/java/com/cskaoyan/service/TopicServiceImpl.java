package com.cskaoyan.service;

import com.cskaoyan.bean.bo.TopicBachDeleteBO;
import com.cskaoyan.bean.bo.TopicCreateBO;
import com.cskaoyan.bean.bo.TopicUpdateBO;
import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.po.GoodsExample;
import com.cskaoyan.bean.po.Topic;
import com.cskaoyan.bean.po.TopicExample;
import com.cskaoyan.bean.vo.TopicListVO;
import com.cskaoyan.bean.vo.TopicReadGoodsListVO;
import com.cskaoyan.bean.vo.TopicReadVO;
import com.cskaoyan.bean.vo.topic.*;
import com.cskaoyan.mapper.GoodsMapper;
import com.cskaoyan.mapper.TopicMapper;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.*;

/**
 * @ClassName: TopicServiceImpl
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/6 9:18
 */

@Service
public class TopicServiceImpl implements TopicService {

    @Autowired
    ConstantProperties CONSTANT;

    @Autowired
    TopicMapper topicMapper;

    @Autowired
    GoodsMapper goodsMapper;

    @Override
    public TopicListVO getTopicList(Integer page, Integer limit, String sort, String order, String title, String subtitle) {
        TopicListVO topicListVO = new TopicListVO();
        topicListVO.setPage(page);
        topicListVO.setLimit(limit);

        // 分页插件PageHelper，辅助做分页以及分页信息的获得
        PageHelper.startPage(page, limit);

        TopicExample example = new TopicExample();
        TopicExample.Criteria criteria = example.createCriteria();

        // 未被删除
        criteria.andDeletedEqualTo(CONSTANT.getNotDeleted());
        // 按照指定字段降序
        example.setOrderByClause(sort + " " + order);

        // 查找专题标题，模糊查询
        if (title != null && !("").equals(title + "")) {
            criteria.andTitleLike("%" + title + "%");
        }

        // 查找专题子标题，模糊查询
        if (subtitle != null && !("").equals(subtitle + "")) {
            criteria.andSubtitleLike("%" + subtitle + "%");
        }

        List<Topic> topicList = topicMapper.selectByExample(example);
        PageInfo<Topic> topicPageInfo = new PageInfo<>(topicList);
        topicListVO.setList(topicList);

        long count = topicPageInfo.getTotal();
        topicListVO.setTotal((int) count);

        topicListVO.setPages((int) Math.ceil(count * 1.0 / limit));

        return topicListVO;
    }

    @Override
    public Topic createTopic(TopicCreateBO topicCreateBO) {
        Topic testTopic = new Topic(null, topicCreateBO.getTitle(), topicCreateBO.getSubtitle(), topicCreateBO.getPrice(), topicCreateBO.getReadCount(), topicCreateBO.getPicUrl(), CONSTANT.getDefaultSortOrder(), topicCreateBO.getGoods(), new Date(), new Date(), CONSTANT.getNotDeleted(), topicCreateBO.getContent());
        topicMapper.insert(testTopic);

        Topic topic = topicMapper.selectByPrimaryKey(testTopic.getId());
        return topic;
    }

    @Override
    public void deleteTopic(Topic topic) {
        // 根据topicId来锁定专题，将其假删除
        Integer topicId = topic.getId();

        TopicExample example = new TopicExample();
        TopicExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(topicId);
        Topic topic1 = topic;
        topic1.setDeleted(true);
        topicMapper.updateByExample(topic1, example);
    }

    @Override
    public TopicReadVO getTopicInfo(Integer id) {
        Topic topic = topicMapper.selectByPrimaryKey(id);
        List<TopicReadGoodsListVO> topicReadGoodsListVOS = new ArrayList<>();

        Integer[] goodsIds = topic.getGoods();
        for (Integer goodsId : goodsIds) {
            Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
            TopicReadGoodsListVO topicReadGoodsListVO = new TopicReadGoodsListVO(goods.getBrief(), goods.getCounterPrice(), goods.getId(), goods.getIsHot(), goods.getIsNew(), goods.getName(), goods.getPicUrl(), goods.getRetailPrice());
            topicReadGoodsListVOS.add(topicReadGoodsListVO);
        }

        TopicReadVO topicReadVO = new TopicReadVO(topicReadGoodsListVOS, topic);
        return topicReadVO;
    }

    @Override
    public Topic updateTopic(TopicUpdateBO topicUpdateBO) {
        BigDecimal price = new BigDecimal(topicUpdateBO.getPrice());

        Topic topic = new Topic(topicUpdateBO.getId(), topicUpdateBO.getTitle(), topicUpdateBO.getSubtitle(), price, topicUpdateBO.getReadCount(), topicUpdateBO.getPicUrl(), topicUpdateBO.getSortOrder(), topicUpdateBO.getGoods(), topicUpdateBO.getAddTime(), new Date(), topicUpdateBO.getDeleted(), topicUpdateBO.getContent());
        topicMapper.updateByPrimaryKey(topic);

        return topic;
    }

    @Override
    public void batchDelete(TopicBachDeleteBO topicBachDeleteBO) {
        Integer[] ids = topicBachDeleteBO.getIds();

        for (Integer id : ids) {
            // 根据topicId来锁定专题，将其假删除
            TopicExample example = new TopicExample();
            TopicExample.Criteria criteria = example.createCriteria();
            Topic topic = topicMapper.selectByPrimaryKey(id);
            topic.setDeleted(true);
            criteria.andIdEqualTo(id);
            topicMapper.updateByPrimaryKey(topic);
        }
    }

    @Override
    public WxTopicDetailVO getTopicDetail(Integer id) {
        Topic topic = topicMapper.selectByPrimaryKey(id);

        ArrayList<WxTopicDetailGoodsVO> wxTopicDetailGoodsVOS = new ArrayList<>();
        Integer[] goods = topic.getGoods();
        for (Integer goodsId : goods) {
            Goods good = goodsMapper.selectByPrimaryKey(goodsId);
            WxTopicDetailGoodsVO wxTopicDetailGoodsVO = new WxTopicDetailGoodsVO(good.getBrief(), good.getCounterPrice(), good.getId(), good.getIsHot(), good.getIsNew(), good.getName(), good.getPicUrl(), good.getRetailPrice());
            wxTopicDetailGoodsVOS.add(wxTopicDetailGoodsVO);
        }

        return new WxTopicDetailVO(wxTopicDetailGoodsVOS, topic);
    }

    @Override
    public WxTopicListVO getWxTopicList(Integer page, Integer limit) {
        WxTopicListVO wxTopicListVO = new WxTopicListVO();
        wxTopicListVO.setLimit(limit);
        wxTopicListVO.setPage(page);

        // 分页插件PageHelper，辅助做分页以及分页信息的获得
        PageHelper.startPage(page, limit);

        List<WxTopicListListVO> wxTopicListListVOList = topicMapper.selectWxTopicListNotDeleted(CONSTANT.getNotDeleted());

        PageInfo<WxTopicListListVO> topicListListVOPageInfo = new PageInfo<>(wxTopicListListVOList);

        wxTopicListVO.setList(wxTopicListListVOList);

        long count = topicListListVOPageInfo.getTotal();
        wxTopicListVO.setTotal((int) count);

        wxTopicListVO.setPages((int) Math.ceil(count * 1.0 / limit));

        return wxTopicListVO;
    }

    @Override
    public TopicListVO getRelateTopic(Integer id) {
        Topic topic = topicMapper.selectByPrimaryKey(id);
        Integer[] goods = topic.getGoods();

        List<Integer> ids = new ArrayList<>();
        for (Integer good : goods) {
            TopicExample example = new TopicExample();
            TopicExample.Criteria criteria = example.createCriteria();
            criteria.andGoodsLike("%" + good + "%");
            List<Topic> topicList = topicMapper.selectByExample(example);
            for (Topic oneTopic : topicList) {
                // 默认推荐7个相似的专题
                if (ids.size() == 7) {
                    break;
                }

                if ((int) oneTopic.getId() != id && !ids.contains(oneTopic.getId())) {
                    ids.add(oneTopic.getId());
                }
            }
        }

        List<Topic> topics = new ArrayList<>();
        for (Integer oneId : ids) {
            topics.add(topicMapper.selectByPrimaryKey(oneId));
        }

        TopicListVO topicListVO = new TopicListVO();
        // 默认推荐7个相似的专题
        topicListVO.setLimit(7);
        topicListVO.setTotal(topics.size());
        // 默认一页
        topicListVO.setPage(1);
        topicListVO.setPages(1);
        topicListVO.setList(topics);

        return topicListVO;
    }
}