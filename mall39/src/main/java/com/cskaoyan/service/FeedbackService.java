package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxFeedbackBO;

public interface FeedbackService {
    void feedbackSubmit(WxFeedbackBO wxFeedbackBO, Integer userId, String username);
}
