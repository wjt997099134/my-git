package com.cskaoyan.service;

import com.cskaoyan.bean.bo.*;
import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.po.Category;
import com.cskaoyan.bean.po.OrderGoods;
import com.cskaoyan.bean.vo.*;

import java.util.Date;
import java.util.List;

public interface MarketService {
    CategoryData categoryList();

    Category categoryCreate(CategoryCreateBo categoryCreateBo);

    FatherCategory fatherCategoryList();

    void categoryDelete(CategoryDataListBean categoryDataListBean);

    int categoryUpdate(CategoryUpdateBo categoryUpdateBo);

    RegionData regionList();

    BrandData brandList(BaseParam param, Integer id, String name);

    void brandDelete(Brand brand);

    int brandCreate(BrandCreateBo brandCreateBo);

    Brand brandUpdate(Brand brand);


    OrderData orderList(BaseParam param, OrderListPostBo orderListPostBo);

    OrderDetailData orderDetail(Integer id);

    List<ChannelData> channelList();


    void ship(ShipDataBo shipDataBo);

    void refund(RefundDataBo refundDataBo);

    Integer reply(AdminReplyBO adminReplyBO);


    // 微信订单模块：获取订单列表
    WxOrderListVO wxOrderList(WxBaseParam baseParam);

    // 微信订单模块：订单详情
    WxOrderDetailVO wxOrderDetail(Integer orderId);

    // 微信订单模块：申请退款
    void wxOrderRefund(WxOrderIdBO wxOrderIdBO);

    // 微信订单模块：删除订单
    void wxOrderDelete(WxOrderIdBO wxOrderIdBO);

    // 微信订单模块：确认收货
    void wxOrderConfirm(WxOrderIdBO wxOrderIdBO);

    // 微信订单模块：取消订单
    void wxOrderCancel(WxOrderIdBO wxOrderIdBO);

    // 微信订单模块：去评论中的获取商品信息
    OrderGoods wxOrderGoods(Integer orderId, Integer goodsId);

    // 微信订单模块：评论
    void wxOrderComment(WxOrderCommentBO wxOrderCommentBO);

    // 微信订单模块：下单
    WxOrderSubmitVO wxOrderSubmit(WxOrderSubmitBO wxOrderSubmitBO);

    // 微信订单模块：付款
    void wxOrderPrepay(WxOrderIdBO wxOrderIdBO);
}
