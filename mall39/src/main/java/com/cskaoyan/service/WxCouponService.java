package com.cskaoyan.service;

import com.cskaoyan.bean.vo.spread.WxCouponData;
import com.cskaoyan.bean.vo.spread.WxCouponSelectData;
import com.cskaoyan.bean.vo.spread.WxMyCouponData;

public interface WxCouponService {
    WxCouponData queryAllCoupons(Integer page, Integer limit);

    WxMyCouponData queryMyCoupons(Integer status, Integer page, Integer limit,Integer userId);

    WxCouponSelectData queryCouponsOfCart(Integer cartId, Integer grouponRulesId,Integer userId);

    int receive(Integer couponId,Integer userId);

    int exchange(String couponCode,Integer userId);
}
