package com.cskaoyan.service;


import com.cskaoyan.bean.bo.WxAfterSaleBO;
import com.cskaoyan.bean.vo.WxAfterSaleDetailVO;

public interface AfterSaleService {
    Integer submitAfterSale(WxAfterSaleBO wxAfterSaleBO);

    WxAfterSaleDetailVO afterSaleDetail(Integer orderId);
}
