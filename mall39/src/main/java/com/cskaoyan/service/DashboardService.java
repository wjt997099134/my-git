package com.cskaoyan.service;

import com.cskaoyan.bean.vo.DashboardData;

public interface DashboardService {
    DashboardData dashboard();
}
