package com.cskaoyan.service;

import com.aliyuncs.CommonRequest;
import com.aliyuncs.CommonResponse;
import com.aliyuncs.DefaultAcsClient;
import com.aliyuncs.IAcsClient;
import com.aliyuncs.exceptions.ClientException;
import com.aliyuncs.exceptions.ServerException;
import com.aliyuncs.http.MethodType;
import com.aliyuncs.profile.DefaultProfile;
import com.alibaba.fastjson.JSONObject;
import org.springframework.stereotype.Service;

import java.util.Map;

@Service
public class SendServiceImpl implements SendService {
    @Override
    public boolean sendMsg(String phone, String templateCode, Map<String, Object> map) {
        //连接阿里云
        DefaultProfile profile = DefaultProfile.getProfile("cn-hangzhou", "LTAI5t8gpxPTCR6W58RnZq4u", "mVPbpbxia0JQotb7HyJAREV8QUuq8h");
        IAcsClient client = new DefaultAcsClient(profile);

        //构建请求
        CommonRequest request = new CommonRequest();
        request.setMethod(MethodType.POST);
        request.setDomain("dysmsapi.aliyuncs.com");      //不要动
        request.setVersion("2017-05-25");                //不要动
        request.setAction("SendSms");

        //自定义的参数（手机号,验证码,签名,模板）
        //发送的手机号
        request.putQueryParameter("PhoneNumbers", phone);
        //短信签名
        request.putQueryParameter("SignName", "stone4j");
        //短信模板
        request.putQueryParameter("TemplateCode", templateCode);
        //构建一个短信验证码
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("code","516330");
        request.putQueryParameter("TemplateParam", JSONObject.toJSONString(map));

        //发送短信
        try {
            CommonResponse response = client.getCommonResponse(request);
            System.out.println(response.getData());
            return response.getHttpResponse().isSuccess();
        } catch (ServerException e) {
            e.printStackTrace();
        } catch (ClientException e) {
            e.printStackTrace();
        }
        return false;
    }
}
