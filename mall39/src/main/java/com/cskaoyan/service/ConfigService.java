package com.cskaoyan.service;

import com.cskaoyan.bean.vo.ExpressConfigVO;
import com.cskaoyan.bean.vo.MallConfigVO;
import com.cskaoyan.bean.vo.OrderConfigVO;
import com.cskaoyan.bean.vo.WxConfigVO;

public interface ConfigService {
    MallConfigVO queryMallConfig();

    ExpressConfigVO queryExpressConfig();

    OrderConfigVO queryOrderConfig();

    WxConfigVO queryWxConfig();

    void updateMallConfig(MallConfigVO mallConfigBO);

    void updateExpressConfig(ExpressConfigVO expressConfigBO);

    void updateOrderConfig(OrderConfigVO orderConfigBO);

    void updateWxConfig(WxConfigVO wxConfigBO);
}
