package com.cskaoyan.service;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.spread.AdCreateBo;
import com.cskaoyan.bean.bo.spread.CouponCreateBo;
import com.cskaoyan.bean.bo.spread.CouponUpdateBo;
import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.vo.spread.*;

public interface SpreadService {
    AdData queryAd(BaseParam param, String name, String content);

    AdCreate createAd(AdCreateBo adCreateBo);

    AdCreate updateAd(AdCreate adCreate);

    void deleteAd(Integer id);

    CouponData queryCoupon(BaseParam param, String name, Integer type, Integer status);

    CouponCreate createCoupon(CouponCreateBo couponCreateBo);

    CouponCreateType2 createCoupon2(CouponCreateBo couponCreateBo);

    CouponUpdate updateCoupon(CouponUpdateBo couponUpdateBo);

    void deleteCoupon(Integer id);

    Coupon queryCouponDetail(Integer id);

    ListuserData queryListuser(BaseParam param, Integer couponId, Integer userId, Integer status);

}
