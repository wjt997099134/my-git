package com.cskaoyan.service;

import com.cskaoyan.bean.bo.KeywordBO;
import com.cskaoyan.bean.bo.KeywordDetailBO;
import com.cskaoyan.bean.po.Keyword;
import com.cskaoyan.bean.po.KeywordExample;
import com.cskaoyan.bean.vo.KeywordBaseVO;
import com.cskaoyan.bean.vo.KeywordDetailBean;
import com.cskaoyan.bean.vo.KeywordVO;
import com.cskaoyan.bean.vo.KeywordsListVO;
import com.cskaoyan.mapper.KeywordMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @param
 * @Date: 2022/6/4 17:11
 * @Description:商场关键字模块 TODO
 * @Author: 李宇浩
 * @return
 */
@Service
public class MarketKeywordServiceImpl implements MarketKeywordService {

    @Autowired
    KeywordMapper keywordMapper;

    /**
     * 关键字列表条件查询并显示
     */
    @Override
    public KeywordsListVO querryKeywords(Integer page, Integer limit, String keyword, String url, String sort, String order) {

        // 分页插件参数
        PageHelper pageHelper = new PageHelper();
        pageHelper.startPage(page, limit);

        // 查询条件
        KeywordExample keywordExample = new KeywordExample();
        KeywordExample.Criteria criteria = keywordExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        if (keyword != null) {
            criteria.andKeywordLike("%" + keyword + "%");
        }
        if (url != null) {
            criteria.andUrlLike("%" + url + "%");
        }
        keywordExample.setOrderByClause(sort + " " + order);
        List<Keyword> keywords = keywordMapper.selectByExample(keywordExample);

        PageInfo<Keyword> keywordPageInfo = new PageInfo<>(keywords);
        long total = keywordPageInfo.getTotal();
        int pages = keywordPageInfo.getPages();

        // 封装展示类
        KeywordsListVO keywordsListVO = new KeywordsListVO();
        List<KeywordDetailBean> keywordDetailBeans = new LinkedList<>();
        keywordsListVO.setPage(page);
        keywordsListVO.setLimit(limit);
        keywordsListVO.setPages(pages);
        keywordsListVO.setTotal(Integer.valueOf(((int) total)));
        for (Keyword keyword1 : keywords) {
            KeywordDetailBean keywordDetailBean = new KeywordDetailBean();
            keywordDetailBean.setId(keyword1.getId());
            keywordDetailBean.setKeyword(keyword1.getKeyword());
            keywordDetailBean.setUrl(keyword1.getUrl());
            keywordDetailBean.setHot(keyword1.getIsHot());
            keywordDetailBean.setDefault(keyword1.getIsDefault());
            keywordDetailBean.setSortOrder(keyword1.getSortOrder());
            keywordDetailBean.setAddTime(keyword1.getAddTime());
            keywordDetailBean.setUpdateTime(keyword1.getUpdateTime());
            keywordDetailBean.setDeleted(keyword1.getDeleted());
            keywordDetailBeans.add(keywordDetailBean);
        }
        keywordsListVO.setList(keywordDetailBeans);

        return keywordsListVO;
    }

    /**
     * 关键字新增
     */
    @Override
    public KeywordVO addKeyword(KeywordBO keywordBO) {

        Keyword keyword = new Keyword();
        keyword.setKeyword(keywordBO.getKeyword());
        if (keywordBO.getUrl() != null) {
            keyword.setUrl(keywordBO.getUrl());
        }
        keyword.setIsHot(keywordBO.isHot());
        keyword.setIsDefault(keywordBO.isDefault());
        keyword.setSortOrder(100);
        keyword.setAddTime(new Date(System.currentTimeMillis()));
        keyword.setUpdateTime(new Date(System.currentTimeMillis()));
        keyword.setDeleted(false);

        int id = keywordMapper.insertSelective(keyword);

        Keyword keyword1 = keywordMapper.selectByPrimaryKey(id);

        KeywordVO keywordVO = new KeywordVO();
        keywordVO.setId(id);
        keywordVO.setKeyword(keyword1.getKeyword());
        keywordVO.setUrl(keyword1.getUrl());
        if (keyword1.getIsDefault() == true) {
            keywordVO.setIsDefault("true");
        } else {
            keywordVO.setIsDefault("false");
        }
        if (keyword1.getIsHot() == true) {
            keywordVO.setIsHot("true");
        } else {
            keywordVO.setIsHot("false");
        }
        keywordVO.setAddTime(keyword1.getAddTime());
        keywordVO.setUpdateTime(keyword1.getUpdateTime());

        return keywordVO;
    }

    /**
     * 关键字更新
     */
    @Override
    public KeywordVO updateKeyword(KeywordDetailBO keywordDetailBO) {

        // 查询条件
        KeywordExample keywordExample = new KeywordExample();
        KeywordExample.Criteria criteria = keywordExample.createCriteria();

        // 封装PO类
        Keyword keyword = new Keyword();
        keyword.setSortOrder(100);
        keyword.setUpdateTime(new Date(System.currentTimeMillis()));
        keyword.setDeleted(false);
        keyword.setAddTime(keywordDetailBO.getAddTime());
        keyword.setIsDefault(keywordDetailBO.isDefault());
        keyword.setIsHot(keywordDetailBO.isHot());
        keyword.setUrl(keywordDetailBO.getUrl());
        keyword.setKeyword(keywordDetailBO.getKeyword());
        criteria.andIdEqualTo(keywordDetailBO.getId());

        // 更新并取出更新后的数据
        keywordMapper.updateByExampleSelective(keyword, keywordExample);
        Keyword keyword1 = keywordMapper.selectByPrimaryKey(keywordDetailBO.getId());

        // 封装返回
        KeywordVO keywordVO = new KeywordVO();
        keywordVO.setId(keyword1.getId());
        keywordVO.setUpdateTime(keyword1.getUpdateTime());
        if (keyword1.getIsDefault() == true) {
            keywordVO.setIsDefault("true");
        } else {
            keywordVO.setIsDefault("false");
        }
        if (keyword1.getIsHot() == true) {
            keywordVO.setIsHot("true");
        } else {
            keywordVO.setIsHot("false");
        }
        keywordVO.setUrl(keyword1.getUrl());
        keywordVO.setKeyword(keyword1.getKeyword());
        keywordVO.setAddTime(keyword1.getAddTime());

        return keywordVO;
    }

    /**
     * 关键字删除
     */
    @Override
    public void delete(KeywordDetailBO keywordDetailBO) {

        Keyword keyword = keywordMapper.selectByPrimaryKey(keywordDetailBO.getId());
        keyword.setDeleted(true);
        keywordMapper.updateByPrimaryKeySelective(keyword);
    }
}
