package com.cskaoyan.service;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.spread.AdCreateBo;
import com.cskaoyan.bean.bo.spread.CouponCreateBo;
import com.cskaoyan.bean.bo.spread.CouponUpdateBo;
import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.vo.spread.*;
import com.cskaoyan.mapper.SpreadMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 推广管理Service接口实现类
 * @author CR
 * @since 2022/06/04 21:42
 */
@Service
public class SpreadServiceImpl implements SpreadService {
    @Autowired
    SpreadMapper spreadMapper;


    /**
     *  生成一个8位随机兑换码(大写字母和数字)
     */
    private String generateCode() {
        Random random = new Random();
        char[] str = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
        int count = 0;
        StringBuffer code = new StringBuffer();
        while (count < 8) {
            int i = Math.abs(random.nextInt(36));
            code.append(str[i]);
            count ++;
        }
        return code.toString();
    }

    @Override
    public AdData queryAd(BaseParam param, String name, String content) {
        PageHelper.startPage(param.getPage(), param.getLimit());
        List<AdDataListBean> ads = spreadMapper.queryAds(param.getSort(), param.getOrder(),name,content);
        PageInfo pageInfo = new PageInfo(ads);
        return AdData.data(pageInfo.getTotal(),pageInfo.getPages(),param.getLimit(), param.getPage(), ads);
    }

    @Override
    public AdCreate createAd(AdCreateBo adCreateBo) {
        AdCreate adCreate = null;
        if (adCreateBo.getLink() != null) {
            adCreate = new AdCreate(new Date(), adCreateBo.getName(), adCreateBo.getLink(), new Date(), null, adCreateBo.getPosition(), adCreateBo.getUrl(), adCreateBo.getContent(), adCreateBo.isEnabled());
            spreadMapper.insertAd(adCreate);
        } else {
            adCreate = new AdCreate(new Date(), adCreateBo.getName(),  new Date(), null, adCreateBo.getPosition(), adCreateBo.getUrl(), adCreateBo.getContent(), adCreateBo.isEnabled());
            spreadMapper.insertAd1(adCreate);
        }
        return adCreate;
    }

    @Override
    public AdCreate updateAd(AdCreate adCreate) {
        AdCreate adCreate1 = new AdCreate(adCreate.getAddTime(),adCreate.getName(),adCreate.getLink(),new Date(),adCreate.getId(),adCreate.getPosition(),adCreate.getUrl(),adCreate.getContent(),adCreate.isEnabled());
        spreadMapper.updateAd(adCreate1);
        return adCreate1;
    }

    @Override
    public void deleteAd(Integer id) {
        spreadMapper.deleteAd(id);
    }

    @Override
    public CouponData queryCoupon(BaseParam param, String name, Integer type, Integer status) {
        PageHelper.startPage(param.getPage(), param.getLimit());
        List<CouponDataListBean> coupons = spreadMapper.queryCoupons(param.getSort(),param.getOrder(),name,type,status);
        PageInfo pageInfo = new PageInfo(coupons);
        return CouponData.data(pageInfo.getTotal(),pageInfo.getPages(),param.getLimit(),param.getPage(),coupons);
    }

    @Override
    public CouponCreate createCoupon(CouponCreateBo couponCreateBo) {
        CouponCreate couponCreate = new CouponCreate(new Date(),couponCreateBo.getDiscount(),couponCreateBo.getTimeType(),couponCreateBo.getGoodsValue(),new Date(),couponCreateBo.getType(),couponCreateBo.getGoodsType(),couponCreateBo.getTotal(),couponCreateBo.getMin(),couponCreateBo.getName(),couponCreateBo.getLimit(),couponCreateBo.getDays(),null,couponCreateBo.getTag(),couponCreateBo.getDesc(),couponCreateBo.getStatus());
        /*
        * 有效期类型限制，
        * 选择领券相对天数，则赋值days，且time_type是0
        * 选择指定绝对时间，则赋值start_time和end_time，且time_type是1
         */
        if (couponCreateBo.getStartTime() == null) {
            spreadMapper.insertCoupon(couponCreate);
        } else {
            spreadMapper.insertCoupon1(couponCreate,couponCreateBo.getStartTime(),couponCreateBo.getEndTime());
        }
        return couponCreate;
    }

    @Override
    public CouponCreateType2 createCoupon2(CouponCreateBo couponCreateBo) {
        String code = generateCode();
        CouponCreateType2 couponCreateType2 = new CouponCreateType2(code,new Date(),couponCreateBo.getDiscount(),couponCreateBo.getTimeType(),couponCreateBo.getGoodsValue(),new Date(),couponCreateBo.getType(),couponCreateBo.getGoodsType(),couponCreateBo.getTotal(),couponCreateBo.getMin(),couponCreateBo.getName(),couponCreateBo.getLimit(),couponCreateBo.getDays(),null,couponCreateBo.getTag(),couponCreateBo.getDesc(),couponCreateBo.getStatus());
        /*
         * 有效期类型限制，
         * 选择领券相对天数，则赋值days，且time_type是0
         * 选择指定绝对时间，则赋值start_time和end_time，且time_type是1
         */
        if (couponCreateBo.getStartTime() == null) {
            spreadMapper.insertCoupon2(couponCreateType2);
        } else {
            spreadMapper.insertCoupon3(couponCreateType2,couponCreateBo.getStartTime(),couponCreateBo.getEndTime());
        }
        return couponCreateType2;
    }

    @Override
    public CouponUpdate updateCoupon(CouponUpdateBo couponUpdateBo) {
        CouponUpdate couponUpdate = new CouponUpdate(couponUpdateBo.getAddTime(),couponUpdateBo.getDiscount(),couponUpdateBo.getTimeType(),couponUpdateBo.getGoodsValue(),new Date(),couponUpdateBo.getType(),couponUpdateBo.getGoodsType(),couponUpdateBo.getTotal(),couponUpdateBo.getMin(),couponUpdateBo.isDeleted(),couponUpdateBo.getName(),couponUpdateBo.getLimit(),couponUpdateBo.getDays(),couponUpdateBo.getStartTime(),couponUpdateBo.getId(),couponUpdateBo.getTag(),couponUpdateBo.getEndTime(),couponUpdateBo.getDesc(),couponUpdateBo.getStatus());
        if (couponUpdateBo.getType() == 2) {
            String code = generateCode();
            spreadMapper.updateCoupon1(couponUpdate,code);
        }
        spreadMapper.updateCoupon(couponUpdate);
        return couponUpdate;
    }

    @Override
    public void deleteCoupon(Integer id) {
        spreadMapper.deleteCoupon(id);
    }

    @Override
    public Coupon queryCouponDetail(Integer id) {
        Coupon couponDetail = spreadMapper.queryCouponById(id);
        return couponDetail;
    }

    @Override
    public ListuserData queryListuser(BaseParam param, Integer couponId, Integer userId, Integer status) {
        PageHelper.startPage(param.getPage(), param.getLimit());
        List<ListuserDataListBean> listusers = spreadMapper.queryListusers(param.getSort(), param.getOrder(),couponId,userId,status);
        PageInfo pageInfo = new PageInfo(listusers);
        return ListuserData.data(pageInfo.getTotal(),pageInfo.getPages(),param.getLimit(),param.getPage(),listusers);
    }



}
