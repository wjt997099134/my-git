package com.cskaoyan.service;

import com.cskaoyan.bean.vo.DashboardData;
import com.cskaoyan.mapper.DashboardMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author yemingfei
 * @since 2022/06/05 21:10
 */
@Service
public class DashboardServiceImpl implements DashboardService {
    @Autowired
    DashboardMapper dashboardMapper;
    @Override
    public DashboardData dashboard() {
       int goodsTotal = dashboardMapper.queryGoodsTotal();
       int userTotal = dashboardMapper.queryUserTotal();
       int productTotal = dashboardMapper.queryProductTotal();
       int orderTotal = dashboardMapper.queryOrderTotal();
        DashboardData dashboardData = new DashboardData(
                goodsTotal,
                userTotal,
                productTotal,
                orderTotal
                );
        return dashboardData;
    }
}
