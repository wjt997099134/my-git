package com.cskaoyan.service;

import com.cskaoyan.bean.bo.KeywordBO;
import com.cskaoyan.bean.bo.KeywordDetailBO;
import com.cskaoyan.bean.vo.KeywordVO;
import com.cskaoyan.bean.vo.KeywordsListVO;

public interface MarketKeywordService {

    /**
     * 关键字列表显示
     */
    KeywordsListVO querryKeywords(Integer page, Integer limit, String keyword, String url, String sort, String order);

    /**
     * 关键字新增
     */
    KeywordVO addKeyword(KeywordBO keywordBO);

    /**
     * 关键字更新
     */
    KeywordVO updateKeyword(KeywordDetailBO keywordDetailBO);

    /**
     * 关键字删除
     */
    void delete(KeywordDetailBO keywordDetailBO);

}
