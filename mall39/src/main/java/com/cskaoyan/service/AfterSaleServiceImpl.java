package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxAfterSaleBO;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.WxAfterSaleDetailAftersaleVO;
import com.cskaoyan.bean.vo.WxAfterSaleDetailOrderVO;
import com.cskaoyan.bean.vo.WxAfterSaleDetailVO;
import com.cskaoyan.mapper.AftersaleMapper;
import com.cskaoyan.mapper.OrderGoodsMapper;
import com.cskaoyan.mapper.OrderMapper;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import sun.net.www.http.HttpClient;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: AfterSaleServiceImpl
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/9 9:52
 */

@Service
public class AfterSaleServiceImpl implements AfterSaleService {

    @Autowired
    AftersaleMapper aftersaleMapper;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    OrderGoodsMapper orderGoodsMapper;

    @Override
    public Integer submitAfterSale(WxAfterSaleBO wxAfterSaleBO) {

        Subject subject = SecurityUtils.getSubject();
        User primaryPrincipal = (User) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();

        Aftersale aftersale = new Aftersale(null, "1000" + wxAfterSaleBO.getOrderId(), wxAfterSaleBO.getOrderId(), userId, wxAfterSaleBO.getType(), wxAfterSaleBO.getReason(), wxAfterSaleBO.getAmount(), wxAfterSaleBO.getPictures(), wxAfterSaleBO.getComment(), (short) 1, null, new Date(), new Date(), false);

        Integer affectedRows = aftersaleMapper.insert(aftersale);

        Order order = orderMapper.selectByPrimaryKey(wxAfterSaleBO.getOrderId());
        order.setAftersaleStatus((short) 1);
        // order.setOrderStatus((short) 202);
        order.setUpdateTime(aftersale.getUpdateTime());
        orderMapper.updateByPrimaryKey(order);

        return affectedRows;
    }

    @Override
    public WxAfterSaleDetailVO afterSaleDetail(Integer orderId) {
        AftersaleExample aftersaleExample = new AftersaleExample();
        AftersaleExample.Criteria criteria = aftersaleExample.createCriteria();
        // 查看该订单是否有售后信息，可能是查不出来的
        criteria.andOrderIdEqualTo(orderId);
        criteria.andDeletedEqualTo(false);
        List<Aftersale> aftersales = aftersaleMapper.selectByExample(aftersaleExample);
        if (aftersales.size() != 0) {
            // 直接返回信息
            Aftersale aftersale = aftersales.get(0);
            WxAfterSaleDetailAftersaleVO wxAfterSaleDetailAftersaleVO = new WxAfterSaleDetailAftersaleVO(aftersale.getId(), aftersale.getAftersaleSn(), aftersale.getOrderId(), aftersale.getUserId(), aftersale.getType(), aftersale.getReason(), aftersale.getAmount(), aftersale.getPictures(), aftersale.getComment(), aftersale.getStatus(), aftersale.getAddTime(), aftersale.getUpdateTime(), aftersale.getDeleted());

            Order order = orderMapper.selectByPrimaryKey(orderId);
            WxAfterSaleDetailOrderVO wxAfterSaleDetailOrderVO = new WxAfterSaleDetailOrderVO(order.getId(), order.getUserId(), order.getOrderSn(), order.getOrderStatus(), order.getAftersaleStatus(), order.getConsignee(), order.getMobile(), order.getAddress(), order.getMessage(), order.getGoodsPrice(), order.getFreightPrice(), order.getCouponPrice(), order.getIntegralPrice(), order.getGrouponPrice(), order.getOrderPrice(), order.getActualPrice(), order.getPayTime(), order.getShipSn(), order.getShipChannel(), order.getShipTime(), order.getConfirmTime(), order.getComments(), order.getAddTime(), order.getUpdateTime(), order.getDeleted());

            OrderGoodsExample orderGoodsExample = new OrderGoodsExample();
            OrderGoodsExample.Criteria criteria1 = orderGoodsExample.createCriteria();
            criteria1.andOrderIdEqualTo(orderId);
            criteria1.andDeletedEqualTo(false);
            List<OrderGoods> orderGoods = orderGoodsMapper.selectByExample(orderGoodsExample);

            return new WxAfterSaleDetailVO(wxAfterSaleDetailAftersaleVO, wxAfterSaleDetailOrderVO, orderGoods);
        }

        return null;
    }
}