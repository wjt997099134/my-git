package com.cskaoyan.service;

import com.cskaoyan.bean.GoodsListBaseParam;
import com.cskaoyan.bean.bo.WxBaseParam;
import com.cskaoyan.bean.vo.*;

public interface WxGoodsService {
    WxHomeListVo homeList();

    WxCategoryInfoVo categoryInfo(Integer id);

    WxGoodsListVo goodsList(GoodsListBaseParam goodsListBaseParam);

    int countGoods();

    WxGoodsDetailVo goodsDetail(Integer id);

    WxGoodsRelatedData goodsRelated(Integer id);

    WxCommentData commentList(WxBaseParam wxBaseParam, Integer valueId, Integer type);

    WxCommentCountData commentCount(Integer valueId, Integer type);
}
