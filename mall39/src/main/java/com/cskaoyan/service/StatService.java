package com.cskaoyan.service;

import com.cskaoyan.bean.vo.GoodsStatVO;
import com.cskaoyan.bean.vo.OrderStatVO;
import com.cskaoyan.bean.vo.UserStatVO;

public interface StatService {
    UserStatVO queryUserStat();

    OrderStatVO queryOrderStat();

    GoodsStatVO queryGoodsStat();
}
