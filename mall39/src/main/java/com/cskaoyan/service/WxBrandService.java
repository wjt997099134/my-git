package com.cskaoyan.service;

import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.vo.spread.WxBrandData;

public interface WxBrandService {

    WxBrandData queryBrands(Integer page, Integer limit);

    Brand queryBrandById(Integer id);
}
