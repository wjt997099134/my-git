package com.cskaoyan.service;


import com.cskaoyan.bean.bo.*;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.lang.System;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

/**
 * @param
 * @Date: 2022/6/4 17:13
 * @Description:商品管理 Doing
 * @Author: 李宇浩
 * @return
 */
@Service
public class GoodsServiceImpl implements GoodsService {

    @Autowired
    GoodsMapper goodsMapper;

    @Autowired
    GoodsSpecificationMapper goodsSpecificationMapper;

    @Autowired
    GoodsProductMapper goodsProductMapper;

    @Autowired
    GoodsAttributeMapper goodsAttributeMapper;

    @Autowired
    CategoryMapper categoryMapper;

    @Autowired
    BrandMapper brandMapper;

    @Autowired
    ConstantProperties constant;

    /**
     * 获取商品列表
     */
    @Override
    public GoodsData querryAll(String page, String limit, String goodsSn, String name, String sort, String order, Integer goodsId) {
        // 参数类型转换
        int pageVO = 0;
        if (page != null) {
            pageVO = Integer.parseInt(page);
        }
        int limitVO = 0;
        if (limit != null) {
            limitVO = Integer.parseInt(limit);
        }
        //分页插件
        PageHelper.startPage(pageVO, limitVO);
        // 逆向参数设置
        GoodsExample goodsExample = new GoodsExample();
        GoodsExample.Criteria criteria = goodsExample.createCriteria();
        criteria.andDeletedEqualTo(false);

        if (goodsSn != null) {
            criteria.andGoodsSnLike("%" + goodsSn + "%");
        }

        if (name != null) {
            criteria.andNameLike("%" + name + "%");
        }

        if (goodsId != null) {
            criteria.andIdEqualTo(goodsId);
        }

        if (sort != null && order != null) {
            goodsExample.setOrderByClause(sort + " " + order);
        }

        List<Goods> goods = goodsMapper.selectByExample(goodsExample);

        PageInfo<Goods> pageInfo = new PageInfo<>(goods);

        int total = ((int) pageInfo.getTotal());
        int pages = pageInfo.getPages();

        // 商品封装
        List<GoodsDataListBean> list = new LinkedList<>();
        GoodsData goodsData = new GoodsData();
        goodsData.setPage(pageVO);
        goodsData.setLimit(limitVO);
        goodsData.setPages(pages);
        goodsData.setTotal(total);
        for (Goods good : goods) {
            GoodsDataListBean goodData = new GoodsDataListBean();
            goodData.setId(good.getId());
            goodData.setGoodsSn(good.getGoodsSn());
            goodData.setName(good.getName());
            goodData.setCategoryId(good.getCategoryId());
            goodData.setBrandId(good.getBrandId());
            String[] gallery = good.getGallery();
            // 拆分图片字符串封装
            goodData.setGallery(gallery);
            goodData.setKeywords(good.getKeywords());
            goodData.setBrief(good.getBrief());
            goodData.setIsOnSale(good.getIsOnSale());
            goodData.setSortOrder(Integer.parseInt(good.getSortOrder().toString()));
            goodData.setPicUrl(good.getPicUrl());
            goodData.setIsNew(good.getIsNew());
            goodData.setIsHot(good.getIsHot());
            goodData.setShareUrl(good.getShareUrl());
            goodData.setUnit(good.getUnit());
            goodData.setCounterPrice(Double.parseDouble(good.getCounterPrice().toString()));
            goodData.setRetailPrice(Double.parseDouble(good.getRetailPrice().toString()));
            goodData.setAddTime(good.getAddTime());
            goodData.setUpdateTime(good.getUpdateTime());
            goodData.setDeleted(good.getDeleted());
            goodData.setDetail(good.getDetail());
            list.add(goodData);
        }
        goodsData.setList(list);
        return goodsData;
    }

    /**
     * 添加商品信息
     * 返回参数
     * 200：成功
     * 404：失败
     */
    @Override
    public int addGoods(GoodsCreateBO goodsCreateBO) {
        // 接收数据提取
        GoodsDetail goods = goodsCreateBO.getGoods();
        List<Specifications> specifications = goodsCreateBO.getSpecifications();
        List<Products> products = goodsCreateBO.getProducts();
        List<AttributesDetail> attributes = goodsCreateBO.getAttributes();

        // 数据库交互对象
        Goods goodsPO = new Goods();
        GoodsSpecification specificationPO = new GoodsSpecification();
        GoodsProduct productPO = new GoodsProduct();
        GoodsAttribute attributePO = new GoodsAttribute();

        // example类
        GoodsExample goodsExample = new GoodsExample();
        GoodsExample.Criteria criteria = goodsExample.createCriteria();

        // 名字和商品编号
        String goodsSn = goods.getGoodsSn();
        String name = goods.getName();

        // 对接收数据进行校验
        //商品编号不是数字
        try {
            if (goods.getCounterPrice() != null) {
                Integer.parseInt(goodsSn);
                BigDecimal.valueOf(goods.getCounterPrice());
            }
        } catch (NumberFormatException e) {
            return 404;
        }
        // 商品单位非法
        if (goods.getUnit() != null) {
            if (!("件".equals(goods.getUnit()) || "个".equals(goods.getUnit()) || "盒".equals(goods.getUnit()))) {
                return 404;
            }
        }

        // 根据商品编号和名称查找商品信息，如果存在信息，检查deleted，若为1则改成0，否则检查后三个表并添加参数建立联系
        criteria.andGoodsSnEqualTo(goodsSn);
        criteria.andNameEqualTo(name);
        List<Goods> goodsInit = goodsMapper.selectByExample(goodsExample);
        if (goodsInit.isEmpty()) {
            // 封装商品信息
            goodsPO.setId(Integer.parseInt(goodsSn));
            goodsPO.setPicUrl(goods.getPicUrl());
            goodsPO.setGallery(goods.getGallery());
            goodsPO.setIsHot(goods.getIsHot());
            goodsPO.setIsNew(goods.getIsNew());
            goodsPO.setIsOnSale(goods.getIsOnSale());
            goodsPO.setGoodsSn(goodsSn);
            goodsPO.setName(name);
            // 下列参数无接收则不加入数据库
            if (goods.getCounterPrice() != null) {
                goodsPO.setCounterPrice(BigDecimal.valueOf(goods.getCounterPrice()));
                goodsPO.setRetailPrice(BigDecimal.valueOf(goods.getCounterPrice()));
            }
            if (goods.getUnit() != null) {
                goodsPO.setUnit(goods.getUnit());
            }
            if (goods.getKeywords() != null) {
                goodsPO.setKeywords(goods.getKeywords());
            }
            if (goods.getCategoryId() != null) {
                goodsPO.setCategoryId(goods.getCategoryId());
            }
            if (goods.getBrandId() != null) {
                goodsPO.setBrandId(goods.getBrandId());
            }
            if (goods.getBrief() != null) {
                goodsPO.setBrief(goods.getBrief());
            }
            if (goods.getDetail() != null) {
                goodsPO.setDetail(goods.getDetail());
            }
            // 删除位和创建时间需要额外管理
            goodsPO.setAddTime(new Date(System.currentTimeMillis()));
            goodsPO.setUpdateTime(new Date(System.currentTimeMillis()));
            goodsPO.setDeleted(false);
            // 插入商品数据
            goodsMapper.insertSelective(goodsPO);
        } else {
            if (goodsInit.get(0).getDeleted().equals("false")) {
                return 404;
            }
            goodsMapper.updateByPrimaryKey(goodsPO);
        }

        // 规格表封装
        // 判空,不空则维护规格表
        if (!specifications.isEmpty()) {
            for (Specifications specification : specifications) {
                // 如果相同规格具有相同参数,且规格不是删除状态，直接返回404报错
                GoodsSpecificationExample specificationExample = new GoodsSpecificationExample();
                GoodsSpecificationExample.Criteria specficationCriteria1 = specificationExample.createCriteria();
                specficationCriteria1.andSpecificationEqualTo(specification.getSpecification());
                specficationCriteria1.andValueEqualTo(specification.getValue());
                specficationCriteria1.andGoodsIdEqualTo(Integer.parseInt(goodsSn));
                List<GoodsSpecification> goodsSpecifications = goodsSpecificationMapper.selectByExample(specificationExample);
                if ((!goodsSpecifications.isEmpty()) && goodsSpecifications.get(0).getDeleted().equals(false)) {
                    return 404;
                }

                // 封装规格
                specificationPO.setSpecification(specification.getSpecification());
                specificationPO.setValue(specification.getValue());
                specificationPO.setPicUrl(specification.getPicUrl());
                specificationPO.setAddTime(new Date(System.currentTimeMillis()));
                specificationPO.setDeleted(false);
                specificationPO.setGoodsId(Integer.parseInt(goodsSn));

                // 如果相同规格参数删除位为true，则修改规格对应数据,否则新插入数据
                if ((!goodsSpecifications.isEmpty()) && goodsSpecifications.get(0).getDeleted().equals(true)) {
                    GoodsSpecificationExample example = new GoodsSpecificationExample();
                    GoodsSpecificationExample.Criteria criteria1 = specificationExample.createCriteria();
                    criteria1.andIdEqualTo(goodsSpecifications.get(0).getId());
                    // 更新规格
                    goodsSpecificationMapper.updateByExampleSelective(specificationPO, example);
                } else {
                    // 插入规格数据
                    goodsSpecificationMapper.insertSelective(specificationPO);
                }
            }
        }

        // 货品表封装
        if (!products.isEmpty()) {
            for (Products product : products) {
                // 货品example
                GoodsProductExample goodsProductExample = new GoodsProductExample();
                GoodsProductExample.Criteria criteria1 = goodsProductExample.createCriteria();
                // 校验货品价格和数量
                try {
                    Double.parseDouble(product.getPrice());
                    Integer.parseInt(product.getNumber());
                } catch (NumberFormatException e) {
                    return 404;
                }
                // 封装数据
                productPO.setId(product.getId());
                productPO.setSpecifications(product.getSpecifications());
                productPO.setPrice(BigDecimal.valueOf(Double.parseDouble(product.getPrice())));
                productPO.setNumber(Integer.parseInt(product.getNumber()));
                productPO.setUrl(product.getUrl());
                productPO.setAddTime(new Date(System.currentTimeMillis()));
                productPO.setDeleted(false);
                productPO.setGoodsId(Integer.parseInt(goodsSn));
                goodsProductMapper.insertSelective(productPO);
            }
        }

        // 参数封装，规格不能有重复的
        // 用map去重
        HashMap<String, String> list = new HashMap<>();
        if (!attributes.isEmpty()) {
            for (AttributesDetail attribute : attributes) {
                // 判断list中有相同的attribute参数名直接返回404
                if (list.containsValue(attribute.getAttribute())) {
                    return 404;
                }
                // 没有重复参数则加入图
                list.put(attribute.getAttribute(), attribute.getAttribute());

                // 封装attribute
                attributePO.setAttribute(attribute.getAttribute());
                attributePO.setValue(attribute.getValue());
                attributePO.setAddTime(new Date(System.currentTimeMillis()));
                attributePO.setDeleted(false);
                attributePO.setGoodsId(Integer.parseInt(goodsSn));
                goodsAttributeMapper.insertSelective(attributePO);
            }
        }
        return 200;
    }

    /**
     * 更新商品
     */
    @Override
    public void updateGoods(GoodsDetailBO goodsDetailBO) {
        GoodsInfo goodsInfo = goodsDetailBO.getGoods();
        List<ProductsInfo> productsInfos = goodsDetailBO.getProducts();
        List<SpecificationsInfo> specificationsInfos = goodsDetailBO.getSpecifications();
        List<AttributesInfo> attributesInfos = goodsDetailBO.getAttributes();

        // 封装并更新商品类
        Goods goods = new Goods();
        goods.setId(goodsInfo.getId());
        goods.setGoodsSn(goodsInfo.getGoodsSn());
        goods.setName(goodsInfo.getName());
        goods.setCategoryId(goodsInfo.getCategoryId());
        goods.setBrandId(goodsInfo.getBrandId());
        goods.setGallery(goodsInfo.getGallery());
        goods.setKeywords(goodsInfo.getKeywords());
        goods.setBrief(goodsInfo.getBrief());
        goods.setIsOnSale(goodsInfo.getIsOnSale());
        goods.setSortOrder(Short.valueOf(String.valueOf(goodsInfo.getSortOrder())));
        goods.setPicUrl(goodsInfo.getPicUrl());
        goods.setIsNew(goodsInfo.getIsNew());
        goods.setIsHot(goodsInfo.getIsHot());
        goods.setUnit(goodsInfo.getUnit());
        goods.setCounterPrice(BigDecimal.valueOf(goodsInfo.getCounterPrice()));
        goods.setRetailPrice(BigDecimal.valueOf(goodsInfo.getRetailPrice()));
        goods.setAddTime(goodsInfo.getAddTime());
        goods.setUpdateTime(goodsInfo.getUpdateTime());
        goods.setDeleted(goodsInfo.getDeleted());
        goods.setDetail(goodsInfo.getDetail());
        goodsMapper.updateByPrimaryKey(goods);

        // 封装并更新货品类
        GoodsProduct goodsProduct = new GoodsProduct();
        GoodsProductExample goodsProductExample = new GoodsProductExample();
        GoodsProductExample.Criteria criteria = goodsProductExample.createCriteria();
        for (ProductsInfo productsInfo : productsInfos) {
            goodsProduct.setId(productsInfo.getId());
            goodsProduct.setGoodsId(productsInfo.getGoodsId());
            goodsProduct.setSpecifications(productsInfo.getSpecifications());
            goodsProduct.setPrice(BigDecimal.valueOf(Double.parseDouble(productsInfo.getPrice())));
            goodsProduct.setNumber(Integer.parseInt(productsInfo.getNumber()));
            goodsProduct.setUrl(productsInfo.getUrl());
            goodsProduct.setAddTime(productsInfo.getAddTime());
            goodsProduct.setUpdateTime(productsInfo.getUpdateTime());
            goodsProduct.setDeleted(productsInfo.getDeleted());
            criteria.andIdEqualTo(goodsProduct.getId());
            // 更新对应货品数据
            goodsProductMapper.updateByExampleSelective(goodsProduct, goodsProductExample);
        }

        // 封装并更新规格类
        GoodsSpecification goodsSpecification = new GoodsSpecification();
        GoodsSpecificationExample goodsSpecificationExample = new GoodsSpecificationExample();
        GoodsSpecificationExample.Criteria criteria1 = goodsSpecificationExample.createCriteria();
        for (SpecificationsInfo specificationsInfo : specificationsInfos) {
            goodsSpecification.setId(specificationsInfo.getId());
            goodsSpecification.setGoodsId(specificationsInfo.getGoodsId());
            goodsSpecification.setSpecification(specificationsInfo.getSpecification());
            goodsSpecification.setValue(specificationsInfo.getValue());
            goodsSpecification.setPicUrl(specificationsInfo.getPicUrl());
            goodsSpecification.setAddTime(specificationsInfo.getAddTime());
            goodsSpecification.setUpdateTime(specificationsInfo.getUpdateTime());
            goodsSpecification.setDeleted(specificationsInfo.getDeleted());
            criteria1.andIdEqualTo(goodsSpecification.getId());
            // 更新规格数据
            goodsSpecificationMapper.updateByExampleSelective(goodsSpecification, goodsSpecificationExample);
        }

        // 封装并更新参数类
        GoodsAttribute goodsAttribute = new GoodsAttribute();
        GoodsAttributeExample goodsAttributeExample = new GoodsAttributeExample();
        GoodsAttributeExample.Criteria criteria2 = goodsAttributeExample.createCriteria();
        for (AttributesInfo attributesInfo : attributesInfos) {
            goodsAttribute.setId(attributesInfo.getId());
            goodsAttribute.setGoodsId(attributesInfo.getGoodsId());
            goodsAttribute.setAttribute(attributesInfo.getAttribute());
            goodsAttribute.setValue(attributesInfo.getValue());
            goodsAttribute.setAddTime(attributesInfo.getAddTime());
            goodsAttribute.setUpdateTime(attributesInfo.getUpdateTime());
            goodsAttribute.setDeleted(attributesInfo.getDeleted());
            criteria2.andIdEqualTo(goodsAttribute.getId());
            // 更新商品参数数据
            goodsAttributeMapper.updateByExampleSelective(goodsAttribute, goodsAttributeExample);
        }
    }

    /**
     * 获取商品详情页面
     */
    @Override
    public GoodsDetailVO selectDetail(Integer id) {

        GoodsDetailVO goodsDetailVO = new GoodsDetailVO();
        GoodsInfo goodsInfo = new GoodsInfo();
        List<ProductsInfo> productsInfos = new LinkedList<>();
        List<SpecificationsInfo> specificationsInfos = new LinkedList<>();
        List<AttributesInfo> attributesInfos = new LinkedList<>();

        Goods goods = goodsMapper.selectByPrimaryKey(id);
        Category category = null;
        Integer[] integers = new Integer[2];
        if (goods.getCategoryId() != constant.getCategoryId()) {
            category = categoryMapper.selectByPrimaryKey(goods.getCategoryId());
            integers[0] = category.getPid();
            integers[1] = category.getId();
        }
        // 封装商品类目
        goodsDetailVO.setCategoryIds(integers);

        // 封装商品详情
        goodsInfo.setId(goods.getId());
        goodsInfo.setGoodsSn(goods.getGoodsSn());
        goodsInfo.setName(goods.getName());
        goodsInfo.setCategoryId(integers[1]);
        goodsInfo.setBrandId(goods.getBrandId());
        goodsInfo.setGallery(goods.getGallery());
        goodsInfo.setKeywords(goods.getKeywords());
        goodsInfo.setBrief(goods.getBrief());
        goodsInfo.setIsOnSale(goods.getIsOnSale());
        goodsInfo.setSortOrder(Integer.valueOf(goods.getSortOrder()));
        goodsInfo.setPicUrl(goods.getPicUrl());
        goodsInfo.setIsNew(goods.getIsNew());
        goodsInfo.setIsHot(goods.getIsHot());
        goodsInfo.setUnit(goods.getUnit());
        goodsInfo.setCounterPrice(Double.parseDouble(goods.getCounterPrice().toString()));
        goodsInfo.setRetailPrice(Double.parseDouble(goods.getRetailPrice().toString()));
        goodsInfo.setAddTime(goods.getAddTime());
        goodsInfo.setUpdateTime(goods.getUpdateTime());
        goodsInfo.setDeleted(goods.getDeleted());
        goodsInfo.setDetail(goods.getDetail());

        // 封装货品详情
        GoodsProductExample goodsProductExample = new GoodsProductExample();
        GoodsProductExample.Criteria criteria = goodsProductExample.createCriteria();
        criteria.andGoodsIdEqualTo(goods.getId());
        List<GoodsProduct> goodsProducts = goodsProductMapper.selectByExample(goodsProductExample);
        for (GoodsProduct goodsProduct : goodsProducts) {
            ProductsInfo productsInfo = new ProductsInfo();
            productsInfo.setId(goodsProduct.getId());
            productsInfo.setGoodsId(goodsProduct.getGoodsId());
            productsInfo.setSpecifications(goodsProduct.getSpecifications());
            productsInfo.setPrice(goodsProduct.getPrice().toString());
            productsInfo.setNumber(String.valueOf(goodsProduct.getNumber()));
            productsInfo.setUrl(goodsProduct.getUrl());
            productsInfo.setAddTime(goodsProduct.getAddTime());
            productsInfo.setUpdateTime(goodsProduct.getUpdateTime());
            productsInfo.setDeleted(goodsProduct.getDeleted());
            productsInfos.add(productsInfo);
        }

        // 封装货品参数
        GoodsAttributeExample goodsAttributeExample = new GoodsAttributeExample();
        GoodsAttributeExample.Criteria criteria1 = goodsAttributeExample.createCriteria();
        criteria1.andGoodsIdEqualTo(goods.getId());
        List<GoodsAttribute> goodsAttributes = goodsAttributeMapper.selectByExample(goodsAttributeExample);
        for (GoodsAttribute goodsAttribute : goodsAttributes) {
            AttributesInfo attributesInfo = new AttributesInfo();
            attributesInfo.setId(goodsAttribute.getId());
            attributesInfo.setGoodsId(goodsAttribute.getGoodsId());
            attributesInfo.setAttribute(goodsAttribute.getAttribute());
            attributesInfo.setValue(goodsAttribute.getValue());
            attributesInfo.setAddTime(goodsAttribute.getAddTime());
            attributesInfo.setUpdateTime(goodsAttribute.getUpdateTime());
            attributesInfo.setDeleted(goodsAttribute.getDeleted());
            attributesInfos.add(attributesInfo);
        }

        // 商品规格封装
        GoodsSpecificationExample goodsSpecificationExample = new GoodsSpecificationExample();
        GoodsSpecificationExample.Criteria criteria2 = goodsSpecificationExample.createCriteria();
        criteria2.andGoodsIdEqualTo(goods.getId());
        List<GoodsSpecification> goodsSpecifications = goodsSpecificationMapper.selectByExample(goodsSpecificationExample);
        for (GoodsSpecification goodsSpecification : goodsSpecifications) {
            SpecificationsInfo specificationsInfo = new SpecificationsInfo();
            specificationsInfo.setId(goodsSpecification.getId());
            specificationsInfo.setGoodsId(goodsSpecification.getGoodsId());
            specificationsInfo.setSpecification(goodsSpecification.getSpecification());
            specificationsInfo.setValue(goodsSpecification.getValue());
            specificationsInfo.setPicUrl(goodsSpecification.getPicUrl());
            specificationsInfo.setAddTime(goodsSpecification.getAddTime());
            specificationsInfo.setUpdateTime(goodsSpecification.getUpdateTime());
            specificationsInfo.setDeleted(goodsSpecification.getDeleted());
            specificationsInfos.add(specificationsInfo);
        }

        // 封装返回类
        goodsDetailVO.setGoods(goodsInfo);
        goodsDetailVO.setProducts(productsInfos);
        goodsDetailVO.setAttributes(attributesInfos);
        goodsDetailVO.setCategoryIds(integers);
        goodsDetailVO.setSpecifications(specificationsInfos);

        return goodsDetailVO;
    }

    /**
     * 删除商品
     */
    @Override
    public void deleteGoods(Goods goods) {
        GoodsExample goodsExample = new GoodsExample();
        GoodsExample.Criteria criteria = goodsExample.createCriteria();
        GoodsProductExample goodsProductExample = new GoodsProductExample();
        GoodsProductExample.Criteria criteria1 = goodsProductExample.createCriteria();
        GoodsSpecificationExample goodsSpecificationExample = new GoodsSpecificationExample();
        GoodsSpecificationExample.Criteria criteria2 = goodsSpecificationExample.createCriteria();
        GoodsAttributeExample goodsAttributeExample = new GoodsAttributeExample();
        GoodsAttributeExample.Criteria criteria3 = goodsAttributeExample.createCriteria();

        // 删除商品
        Goods goodsPO = goodsMapper.selectByPrimaryKey(goods.getId());
        goodsPO.setDeleted(true);
        criteria.andIdEqualTo(goods.getId());
        goodsMapper.updateByExampleSelective(goodsPO, goodsExample);

        // 删除货品
        criteria1.andGoodsIdEqualTo(goods.getId());
        List<GoodsProduct> goodsProducts = goodsProductMapper.selectByExample(goodsProductExample);
        for (GoodsProduct goodsProduct : goodsProducts) {
            goodsProduct.setDeleted(true);
            goodsProductMapper.updateByPrimaryKey(goodsProduct);
        }

        // 删除规格
        criteria2.andGoodsIdEqualTo(goods.getId());
        List<GoodsSpecification> goodsSpecifications = goodsSpecificationMapper.selectByExample(goodsSpecificationExample);
        List<GoodsSpecification> specifications = goodsSpecifications;
        for (GoodsSpecification specification : specifications) {
            specification.setDeleted(true);
            goodsSpecificationMapper.updateByPrimaryKey(specification);
        }

        // 删除参数
        criteria3.andGoodsIdEqualTo(goods.getId());
        List<GoodsAttribute> goodsAttributes = goodsAttributeMapper.selectByExample(goodsAttributeExample);
        for (GoodsAttribute goodsAttribute : goodsAttributes) {
            goodsAttribute.setDeleted(true);
            goodsAttributeMapper.updateByPrimaryKey(goodsAttribute);
        }
    }

    /**
     * 显示商品品牌和商标信息
     */
    @Override
    public CatAndBrandVO catAndBrand() {
        CatAndBrandVO catAndBrandVO = new CatAndBrandVO();

        // 获取封装商标信息
        BrandExample brandExample = new BrandExample();
        BrandExample.Criteria criteria = brandExample.createCriteria();
        criteria.andDeletedEqualTo(false);
        List<Brand> brands = brandMapper.selectByExample(brandExample);
        List<BrandListBean> brandListBeans = new LinkedList<>();
        for (Brand brand : brands) {
            BrandListBean brandListBean = new BrandListBean();
            brandListBean.setValue(brand.getId());
            brandListBean.setLabel(brand.getName());
            brandListBeans.add(brandListBean);
        }

        // 封装获取品牌信息
        CategoryExample categoryExample = new CategoryExample();
        CategoryExample.Criteria criterial = categoryExample.createCriteria();
        criterial.andDeletedEqualTo(false);
        criterial.andLevelEqualTo("L1");
        List<Category> categoriesL1 = categoryMapper.selectByExample(categoryExample);
        List<CategoryListBean> categoryListBeans = new LinkedList<>();
        for (Category category : categoriesL1) {
            CategoryListBean categoryListBean = new CategoryListBean();
            categoryExample.clear();
            CategoryExample.Criteria criteria2 = categoryExample.createCriteria();
            criteria2.andDeletedEqualTo(false);
            criteria2.andLevelEqualTo("L2");
            criteria2.andPidEqualTo(category.getId());
            List<Category> categoriesL2 = categoryMapper.selectByExample(categoryExample);
            List<Children> children = new LinkedList<>();
            categoryListBean.setValue(category.getId());
            categoryListBean.setLabel(category.getName());
            for (Category categoryL2 : categoriesL2) {
                Children child = new Children();
                child.setValue(categoryL2.getId());
                child.setLabel(categoryL2.getName());
                children.add(child);
            }
            categoryListBean.setChildren(children);
            categoryListBeans.add(categoryListBean);
        }

        // 封装显示类
        catAndBrandVO.setBrandList(brandListBeans);
        catAndBrandVO.setCategoryList(categoryListBeans);
        return catAndBrandVO;
    }
}
