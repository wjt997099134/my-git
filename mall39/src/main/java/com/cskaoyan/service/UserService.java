package com.cskaoyan.service;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.bo.RegisterBO;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.bean.vo.user.*;

public interface UserService {


    CollectData collectList(BaseParam param, Integer userId, Integer valueId);

    FootprintData footprintList(BaseParam param, Integer userId, Integer goodsId);

    SearchHistoryData searchHistoryList(BaseParam param, Integer userId, String keyword);

    FeedbackData feedbackList(BaseParam param, String username, Integer id);

    MemberData memberList(BaseParam param, String username, Integer userId, String mobile);

    void update(MemberDataListBean memberDataListBean);

    AddressData addressList(BaseParam param, String name, Integer userId);

    MemberDataListBean memberDetail(Integer id);

    int memberIdIsExist(Integer id);

    // 用户注册
    BaseRespVo register(RegisterBO registerBO);
}
