package com.cskaoyan.service;

import com.cskaoyan.bean.bo.IssueDetailBO;
import com.cskaoyan.bean.po.Issue;
import com.cskaoyan.bean.vo.IssueVO;
import com.cskaoyan.bean.vo.MarketIssueVO;

public interface MarketIssueService {

    /**
     * 通用问题列表显示
     */
    MarketIssueVO querryList(Integer page, Integer limit, String question, String sort, String order);

    /**
     * 新建问题
     */
    IssueVO create(String question, String answer);

    /**
     * 更新通用问题
     */
    IssueVO update(IssueDetailBO issueDetailBO);

    /**
     * 删除通用问题
     */
    void delete(IssueDetailBO issueDetailBO);

}
