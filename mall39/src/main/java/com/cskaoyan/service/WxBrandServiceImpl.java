package com.cskaoyan.service;

import com.cskaoyan.bean.po.Brand;
import com.cskaoyan.bean.vo.WxHomeListVo;
import com.cskaoyan.bean.vo.spread.WxBrandData;
import com.cskaoyan.bean.vo.spread.WxBrandDataListBean;
import com.cskaoyan.mapper.BrandMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 11:13
 */
@Service
public class WxBrandServiceImpl implements WxBrandService{
    @Autowired
    BrandMapper brandMapper;

    @Override
    public WxBrandData queryBrands(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<WxBrandDataListBean> brands = brandMapper.queryBrands();
        PageInfo pageInfo = new PageInfo(brands);
        return WxBrandData.data(pageInfo.getTotal(), pageInfo.getPages(), limit, page, brands);
    }

    @Override
    public Brand queryBrandById(Integer id) {
        Brand brand = brandMapper.selectByPrimaryKey(id);
        return brand;
    }
}
