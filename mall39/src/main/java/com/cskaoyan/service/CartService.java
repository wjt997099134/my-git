package com.cskaoyan.service;

import com.cskaoyan.bean.bo.cartbo.CartCheckedBO;
import com.cskaoyan.bean.bo.cartbo.CartCreateBO;
import com.cskaoyan.bean.bo.cartbo.CartIdsBO;
import com.cskaoyan.bean.bo.cartbo.CartUpdateBO;
import com.cskaoyan.bean.vo.cartvo.CartIndexVO;
import com.cskaoyan.bean.vo.cartvo.CartOrderVO;

import javax.servlet.http.HttpServletRequest;

/**
 * @param
 * @Date: 2022/6/7 11:12
 * @Description:购物车模块服务
 * @Author: 李宇浩
 * @return
 */
public interface CartService {

    /**
     * 购物车商品数量红点回显
     */
    int goodsCount(String username);

    /**
     * 添加购物车
     */
    int addCart(CartCreateBO cartCreateBO, String username);

    /**
     * 购物车列表展示
     */
    CartIndexVO getIndex(String username);

    /**
     * 购物车数据更新
     */
    int updateCart(CartUpdateBO cartUpdateBO);

    /**
     * 快速添加商品购物车(直接购买下的操作)
     */
    int fastAdd(CartCreateBO cartCreateBO, String username);

    /**
     * 购物车商品选中状态更新
     */
    CartIndexVO checked(CartCheckedBO checkedBO, String username);

    /**
     * 订单修改界面回显
     */
    CartOrderVO getCartOrder(Integer cartId, Integer addressId, Integer couponId, Integer userCouponId, Integer grouponRulesId);

    /**
     * 购物车删除
     */
    CartIndexVO delete(CartIdsBO cartIdsBO, String username);
}
