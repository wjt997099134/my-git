package com.cskaoyan.service;

import com.cskaoyan.bean.bo.StorageListBaseParam;
import com.cskaoyan.bean.po.Storage;
import com.cskaoyan.bean.vo.StorageDataVO;

/**
 * @author karlfu
 * @since 2022/06/05 20:26
 */

public interface StorageService {
    void storageCreate(Storage storage);

    StorageDataVO queryAll(StorageListBaseParam param);

    void deleteById(Storage id);

    void updateById(Storage storage);
}
