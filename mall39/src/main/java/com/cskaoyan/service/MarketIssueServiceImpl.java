package com.cskaoyan.service;

import com.cskaoyan.bean.bo.IssueDetailBO;
import com.cskaoyan.bean.po.Issue;
import com.cskaoyan.bean.po.IssueExample;
import com.cskaoyan.bean.vo.IssueListBean;
import com.cskaoyan.bean.vo.IssueVO;
import com.cskaoyan.bean.vo.MarketIssueVO;
import com.cskaoyan.mapper.IssueMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

/**
 * @param
 * @Date: 2022/6/4 17:11
 * @Description:商场通用问题模块 TODO
 * @Author: 李宇浩
 * @return
 */
@Service
public class MarketIssueServiceImpl implements MarketIssueService {

    @Autowired
    IssueMapper issueMapper;


    /**
     * 通用问题展示
     */
    @Override
    public MarketIssueVO querryList(Integer page, Integer limit, String question, String sort, String order) {

        // 分页
        PageHelper pageHelper = new PageHelper();
        pageHelper.startPage(page, limit);

        IssueExample issueExample = new IssueExample();
        IssueExample.Criteria criteria = issueExample.createCriteria();
        if (question != null) {
            criteria.andQuestionLike("%" + question + "%");
        }

        criteria.andDeletedEqualTo(false);

        if (sort != null && order != null) {
            issueExample.setOrderByClause(sort + " " + order);
        }

        // 查询并封装问题列表
        List<IssueListBean> issueListBeans = new LinkedList<>();
        List<Issue> issues = issueMapper.selectByExample(issueExample);
        for (Issue issue : issues) {
            IssueListBean issueListBean = new IssueListBean();
            issueListBean.setId(issue.getId());
            issueListBean.setQuestion(issue.getQuestion());
            issueListBean.setAnswer(issue.getAnswer());
            issueListBean.setAddTime(issue.getAddTime());
            issueListBean.setUpdateTime(issue.getUpdateTime());
            issueListBean.setDeleted(issue.getDeleted());
            issueListBeans.add(issueListBean);
        }

        PageInfo<Issue> issuePageInfo = new PageInfo<>(issues);
        int total = ((int) issuePageInfo.getTotal());
        int pages = issuePageInfo.getPages();

        // 封装展示类
        MarketIssueVO marketIssueVO = new MarketIssueVO();
        marketIssueVO.setTotal(total);
        marketIssueVO.setPages(pages);
        marketIssueVO.setLimit(limit);
        marketIssueVO.setPage(page);
        marketIssueVO.setList(issueListBeans);
        return marketIssueVO;
    }

    /**
     * 新建问题
     */
    @Override
    public IssueVO create(String question, String answer) {

        Issue issue = new Issue();
        issue.setQuestion(question);
        issue.setAnswer(answer);
        Date currentTime = new Date(System.currentTimeMillis());
        issue.setAddTime(currentTime);
        issue.setUpdateTime(currentTime);
        issue.setDeleted(false);
        int id = issueMapper.insertByType(issue);
        IssueVO issueVO = new IssueVO();
        issueVO.setId(id);
        issueVO.setQuestion(question);
        issueVO.setAnswer(answer);
        issueVO.setAddTime(currentTime);
        issueVO.setUpdateTime(currentTime);

        return issueVO;
    }

    /**
     * 更新通用问题
     */
    @Override
    public IssueVO update(IssueDetailBO issueDetailBO) {

        // 封装更新数据
        Issue issue = new Issue();
        IssueExample issueExample = new IssueExample();
        IssueExample.Criteria criteria = issueExample.createCriteria();
        criteria.andIdEqualTo(issueDetailBO.getId());
        issue.setId(issueDetailBO.getId());
        issue.setQuestion(issueDetailBO.getQuestion());
        issue.setAnswer(issueDetailBO.getAnswer());
        issue.setAddTime(issueDetailBO.getAddTime());
        issue.setUpdateTime(new Date(System.currentTimeMillis()));
        issue.setDeleted(false);
        issueMapper.updateByExampleSelective(issue, issueExample);

        IssueVO issueVO = new IssueVO();
        issueVO.setId(issue.getId());
        issueVO.setQuestion(issue.getQuestion());
        issueVO.setAnswer(issue.getAnswer());
        issueVO.setAddTime(issue.getAddTime());
        issueVO.setUpdateTime(issue.getUpdateTime());

        return issueVO;
    }

    /**
     * 删除通用问题
     */
    @Override
    public void delete(IssueDetailBO issueDetailBO) {

        Issue issue = new Issue();
        issue.setId(issueDetailBO.getId());
        issue.setQuestion(issueDetailBO.getQuestion());
        issue.setAnswer(issueDetailBO.getAnswer());
        issue.setAddTime(issueDetailBO.getAddTime());
        issue.setUpdateTime(issueDetailBO.getUpdateTime());
        issue.setDeleted(true);
        issueMapper.updateByPrimaryKey(issue);
    }
}
