package com.cskaoyan.service;

import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.StatMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/06 22:06
 */

@Service
public class StatServiceImpl implements StatService{
    @Autowired
    StatMapper statMapper;
    @Override
    public UserStatVO queryUserStat() {
        UserStatVO userStatVO = new UserStatVO();
        List<UserStatVORow> rows = statMapper.queryUserStat();

        userStatVO.setRows(rows);
        return userStatVO;
    }

    @Override
    public OrderStatVO queryOrderStat() {
        OrderStatVO orderStatVO = new OrderStatVO();
        List<OrderStatVORow> rows = statMapper.queryOrderStat();

        orderStatVO.setRows(rows);
        return orderStatVO;
    }

    @Override
    public GoodsStatVO queryGoodsStat() {
        GoodsStatVO goodsStatVO = new GoodsStatVO();
        List<GoodsStatVORow> rows = statMapper.queryGoodsStat();

        goodsStatVO.setRows(rows);
        return goodsStatVO;
    }
}
