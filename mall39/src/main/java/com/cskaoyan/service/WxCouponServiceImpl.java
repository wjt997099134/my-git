package com.cskaoyan.service;

import com.cskaoyan.bean.po.Cart;
import com.cskaoyan.bean.po.CartExample;
import com.cskaoyan.bean.po.Coupon;
import com.cskaoyan.bean.po.CouponUser;
import com.cskaoyan.bean.vo.cartvo.CartOrderVO;
import com.cskaoyan.bean.vo.spread.*;
import com.cskaoyan.mapper.CartMapper;
import com.cskaoyan.mapper.CouponMapper;
import com.cskaoyan.mapper.CouponUserMapper;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 14:26
 */
@Service
public class WxCouponServiceImpl implements WxCouponService {
    @Autowired
    CouponMapper couponMapper;
    @Autowired
    CouponUserMapper couponUserMapper;
    @Autowired
    ConstantProperties CONSTANT;
    @Autowired
    CartMapper cartMapper;

    @Override
    public WxCouponData queryAllCoupons(Integer page, Integer limit) {
        PageHelper.startPage(page,limit);
        List<WxCouponDataListBean> coupons = couponMapper.queryCoupons();
        PageInfo pageInfo = new PageInfo(coupons);
        return WxCouponData.data(pageInfo.getTotal(), pageInfo.getPages(), limit, page, coupons);
    }

    @Override
    public WxMyCouponData queryMyCoupons(Integer status, Integer page, Integer limit,Integer userId) {
        PageHelper.startPage(page,limit);

        List<CouponUser> couponUsers = couponUserMapper.queryMyCoupons(status,userId);

        List<WxMyCouponDataListBean> myCoupons = new ArrayList<>();
        for (CouponUser couponUser : couponUsers) {
            Coupon coupon = couponMapper.selectByPrimaryKey(couponUser.getCouponId());
            WxMyCouponDataListBean myCouponDataListBean = null;
            // 如果time_type为1（start_time非空），则优惠券的start_time和end_time是优惠券有效期
            // 否则优惠券有效期为 用户领取时间 + 有效天数
            if (coupon.getStartTime() != null) {
                // 如果优惠券结束时间end_time小于当前时间，则优惠券已过期，更新优惠券status为2
                if ((coupon.getEndTime().getTime() - new Date().getTime()) < 0) {
                    // couponMapper.updateById(coupon.getId());
                    couponUserMapper.updateStatus(couponUser.getId());
                }
                myCouponDataListBean = new WxMyCouponDataListBean(couponUser.getId(),couponUser.getCouponId(),coupon.getName(),coupon.getDesc(),coupon.getTag(),coupon.getMin(),coupon.getDiscount(),coupon.getStartTime(),coupon.getEndTime(),false);

            } else {
                // 由用户领取时间 + 有效天数得到结束时间
                Calendar c = Calendar.getInstance();
                c.setTime(couponUser.getAddTime());
                c.add(Calendar.DATE, coupon.getDays());
                Date date = c.getTime();

                // 如果优惠券结束时间date小于当前时间，则优惠券已过期，更新优惠券status为2
                if ((date.getTime() - new Date().getTime()) < 0) {
                    // couponMapper.updateById(coupon.getId());
                    couponUserMapper.updateStatus(couponUser.getId());
                }

                myCouponDataListBean = new WxMyCouponDataListBean(couponUser.getId(),couponUser.getCouponId(),coupon.getName(),coupon.getDesc(),coupon.getTag(),coupon.getMin(),coupon.getDiscount(),couponUser.getAddTime(),date,false);
            }
            myCoupons.add(myCouponDataListBean);
        }

        PageInfo pageInfo = new PageInfo(myCoupons);
        return WxMyCouponData.data(pageInfo.getTotal(),pageInfo.getPages(),limit,page,myCoupons);
    }

    @Override
    public WxCouponSelectData queryCouponsOfCart(Integer cartId, Integer grouponRulesId,Integer userId) {
        // 获取我的未使用优惠券列表
        List<CouponUser> couponUsers = couponUserMapper.queryAllMyCoupons(userId);
        // 前台available默认值为true
        boolean available = true;
        // 创建list集合存放WxCouponSelectDataListBean对象
        List<WxCouponSelectDataListBean> couponSelectDataListBeans = new ArrayList<>();
        // 遍历我的优惠券列表
        for (CouponUser couponUser : couponUsers) {
            // 创建VO对象
            WxCouponSelectDataListBean couponSelectDataListBean = null;

            // 根据couponId获取优惠券信息
            Coupon coupon = couponMapper.queryCouponById(couponUser.getCouponId());

            // 获取该购物车信息
            CartExample cartExample = new CartExample();
            CartExample.Criteria criteria = cartExample.createCriteria();
            criteria.andUserIdEqualTo(userId);
            criteria.andCheckedEqualTo(true);
            criteria.andDeletedEqualTo(false);
            List<Cart> carts = cartMapper.selectByExample(cartExample);
            // 计算该购物车总价格(小程序运费默认为100)
            Double price = CONSTANT.getFreightPrice().doubleValue();
            for (Cart cart : carts) {
                Double goodsPrice = cart.getPrice().doubleValue() * cart.getNumber();
                price = price + goodsPrice;
            }

            // 如果start_time非空，则time_type为1,以优惠券的start_time和end_time为有效期
            if (coupon.getStartTime() != null) {
                // 如果优惠券的开始时间大于当前时间或者结束时间小于当前时间；或者购物车价格小于最少消费金额min
                // 则优惠券不可用，设置available为false
                if ((coupon.getStartTime().getTime() - new Date().getTime()) > 0 || (coupon.getEndTime().getTime() - new Date().getTime()) < 0 || price < coupon.getMin().doubleValue() ) {
                    available = false;
                }
                // 给VO对象赋值
                couponSelectDataListBean = new WxCouponSelectDataListBean(couponUser.getId(),couponUser.getCouponId(),coupon.getName(),coupon.getDesc(),coupon.getTag(),coupon.getMin().doubleValue(),coupon.getDiscount().doubleValue(),coupon.getStartTime(),coupon.getEndTime(),available);
            } else {
                // time_type为0，则优惠券有效期为 用户领取时间 + 有效天数
                Calendar c = Calendar.getInstance();
                // 获取领取时间
                c.setTime(couponUser.getAddTime());
                // 增加有效天数得到结束时间
                c.add(Calendar.DATE, coupon.getDays());
                Date date = c.getTime();
                // 如果优惠券的结束时间小于当前时间；或购物车价格小于最少消费金额min
                // 则优惠券不可用，设置available为false
                if ((date.getTime() - new Date().getTime()) < 0 || price < coupon.getMin().doubleValue() ) {
                    available = false;
                }
                // 给VO对象赋值
                couponSelectDataListBean = new WxCouponSelectDataListBean(couponUser.getId(),couponUser.getCouponId(),coupon.getName(),coupon.getDesc(),coupon.getTag(),coupon.getMin().doubleValue(),coupon.getDiscount().doubleValue(),couponUser.getAddTime(),date,available);
            }
            // 将赋值后的VO对象存入到集合中
            couponSelectDataListBeans.add(couponSelectDataListBean);
        }

        PageInfo pageInfo = new PageInfo(couponSelectDataListBeans);
        return WxCouponSelectData.data(pageInfo.getTotal(),pageInfo.getPages(),pageInfo.getTotal(),pageInfo.getPages(),couponSelectDataListBeans);
    }

    @Override
    public int receive(Integer couponId,Integer userId) {
        // 根据userId查询我的优惠券
        // List<CouponUser> couponUsers = couponUserMapper.queryAllMyCoupons(userId);

        // 根据couponId查询该优惠券信息
        Coupon coupon = couponMapper.queryCouponById(couponId);
        // 根据couponId查询用户已有该优惠券数量
        int num = couponUserMapper.queryMyCouponsByCouponId(couponId,userId);
        // 1.如果limit为0,且total为0则无限制，可以一直领取
        if (coupon.getLimit() == 0 && coupon.getTotal() == 0) {
            // 领取优惠券后向market_coupon_user表中插入数据
            couponUserMapper.insert(new CouponUser(null,userId,couponId,coupon.getStatus(),null,coupon.getStartTime(),coupon.getEndTime(),null,new Date(),new Date(),false));
            return CONSTANT.getSuccessCode();
        } else if (coupon.getLimit() == 0 && coupon.getTotal() != 0) {
            // 2.limit为0，total不为0则最多可领取total次
            if (num < coupon.getTotal()) {
                // 领取优惠券后向market_coupon_user表中插入数据
                couponUserMapper.insert(new CouponUser(null, userId, couponId, coupon.getStatus(), null, coupon.getStartTime(), coupon.getEndTime(), null, new Date(), new Date(), false));
                return CONSTANT.getSuccessCode();
            }
            return CONSTANT.getFailCode4();
        } else {
            // 3.limit不为0，则可领取limit次
            if (num < coupon.getLimit()) {
                // 领取优惠券后向market_coupon_user表中插入数据
                couponUserMapper.insert(new CouponUser(null, userId, couponId, coupon.getStatus(), null, coupon.getStartTime(), coupon.getEndTime(), null, new Date(), new Date(), false));
                return CONSTANT.getSuccessCode();
            }
            return CONSTANT.getFailCode4();
        }
    }

    @Override
    public int exchange(String couponCode,Integer userId) {
        // 根据输入兑换码查询coupon表中是否存在
        int num = couponMapper.queryCouponsByCode(couponCode);
        if (num == 0) {
            return CONSTANT.getFailCode5();
        }
        // 根据输入兑换码获取couponId
        Integer couponId = couponMapper.queryCouponIdByCode(couponCode);


        // 根据userId查询我的优惠券
        // List<CouponUser> couponUsers = couponUserMapper.queryAllMyCoupons(userId);
        // 如果我的列表中已存在该优惠券，则不可兑换
        // for (CouponUser couponUser : couponUsers) {
        //     if (couponId == couponUser.getCouponId()) {
        //         return CONSTANT.getFailCode4();
        //     }
        // }


        // 根据couponId查询该优惠券信息
        Coupon coupon = couponMapper.queryCouponById(couponId);
        // 根据couponId查询用户已有该优惠券数量
        int count = couponUserMapper.queryMyCouponsByCouponId(couponId,userId);
        // 1.如果limit为0,且total为0则无限制，可以一直领取
        if (coupon.getLimit() == 0 && coupon.getTotal() == 0) {
            // 领取优惠券后向market_coupon_user表中插入数据
            couponUserMapper.insert(new CouponUser(null,userId,couponId,(short)0,null,coupon.getStartTime(),coupon.getEndTime(),null,new Date(),new Date(),false));
            return CONSTANT.getSuccessCode();
        } else if (coupon.getLimit() == 0 && coupon.getTotal() != 0) {
            // 2.limit为0，total不为0则最多可领取total次
            if (count < coupon.getTotal()) {
                // 领取优惠券后向market_coupon_user表中插入数据
                couponUserMapper.insert(new CouponUser(null, userId, couponId, (short)0, null, coupon.getStartTime(), coupon.getEndTime(), null, new Date(), new Date(), false));
                return CONSTANT.getSuccessCode();
            }
            return CONSTANT.getFailCode4();
        } else {
            // 3.limit不为0，则可领取limit次
            if (count < coupon.getLimit()) {
                // 领取优惠券后向market_coupon_user表中插入数据
                couponUserMapper.insert(new CouponUser(null, userId, couponId, (short)0, null, coupon.getStartTime(), coupon.getEndTime(), null, new Date(), new Date(), false));
                return CONSTANT.getSuccessCode();
            }
            return CONSTANT.getFailCode4();
        }


        // 我的列表中不存在，则遍历数据库优惠券表判断是否存在相等的输入的couponCode，若存在则可兑换，并插入到数据库我的优惠券列表中，否则返回兑换码不正确
        // List<Coupon> coupons = couponMapper.queryAllCoupons();
        // for (Coupon coupon : coupons) {
        //     if (coupon.getCode() != null && coupon.getCode().equals(couponCode)) {
        //         Coupon coupon1 = couponMapper.queryCouponById(couponId);
        //         couponUserMapper.insert(new CouponUser(null,userId,couponId,coupon1.getStatus(),null,coupon1.getStartTime(),coupon1.getEndTime(),null,new Date(),new Date(),false));
        //         return CONSTANT.getSuccessCode();
        //     }
        // }
        // return CONSTANT.getFailCode5();
    }
}
