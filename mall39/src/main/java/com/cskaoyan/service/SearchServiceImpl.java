package com.cskaoyan.service;

import com.cskaoyan.bean.po.Keyword;
import com.cskaoyan.bean.po.KeywordExample;
import com.cskaoyan.bean.po.SearchHistory;
import com.cskaoyan.bean.po.SearchHistoryExample;
import com.cskaoyan.bean.vo.SearchIndexVO;
import com.cskaoyan.mapper.KeywordMapper;
import com.cskaoyan.mapper.SearchHistoryMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/09 10:01
 */

@Service
public class SearchServiceImpl implements SearchService {
    @Autowired
    SearchHistoryMapper searchHistoryMapper;

    @Autowired
    KeywordMapper keywordMapper;

    @Override
    public List<SearchIndexVO.HistoryKeyword> getHistoryKeywordList(Integer userId) {
        SearchHistoryExample example = new SearchHistoryExample();
        SearchHistoryExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andDeletedEqualTo(false);
        // 也可以用add_time 看商品部分放搜索记录到market_user_history里的逻辑
        example.setOrderByClause("update_time desc");
        example.setDistinct(true);

        return searchHistoryMapper.selectHistoryKeywordByExample(example);
    }

    @Override
    public List<Keyword> getDefaultKeywordList() {
        KeywordExample example = new KeywordExample();
        KeywordExample.Criteria criteria = example.createCriteria();

        criteria.andIsDefaultEqualTo(true);
        criteria.andDeletedEqualTo(false);

        // sort_order是优先级 优先级大的放上面
        example.setOrderByClause("sort_order desc");

        return keywordMapper.selectByExample(example);
    }

    @Override
    public List<Keyword> getHotKeywordList() {
        KeywordExample example = new KeywordExample();
        KeywordExample.Criteria criteria = example.createCriteria();

        criteria.andIsHotEqualTo(true);
        criteria.andDeletedEqualTo(false);

        // sort_order是优先级 优先级大的放上面
        example.setOrderByClause("sort_order desc");

        return keywordMapper.selectByExample(example);
    }

    @Override
    public void clearHistoryByUserId(Integer userId) {
        SearchHistoryExample example = new SearchHistoryExample();
        SearchHistoryExample.Criteria criteria = example.createCriteria();

        criteria.andUserIdEqualTo(userId);
        criteria.andDeletedEqualTo(false);

        SearchHistory searchHistory = new SearchHistory();
        searchHistory.setDeleted(true);

        searchHistoryMapper.updateByExampleSelective(searchHistory, example);
    }

    @Override
    public List<String> getHistoryKeywordHelperList(Integer userId, String inputTry) {
        SearchHistoryExample example = new SearchHistoryExample();
        SearchHistoryExample.Criteria criteria = example.createCriteria();
        criteria.andUserIdEqualTo(userId);
        criteria.andDeletedEqualTo(false);
        criteria.andKeywordLike("%" + inputTry + "%");
        // 也可以用add_time 看商品部分放搜索记录到market_user_history里的逻辑
        example.setOrderByClause("update_time desc");
        example.setDistinct(true);

        return searchHistoryMapper.selectHistoryKeywordHelperByExample(example);

    }
}
