package com.cskaoyan.service;

import com.cskaoyan.bean.bo.TopicBachDeleteBO;
import com.cskaoyan.bean.bo.TopicCreateBO;
import com.cskaoyan.bean.bo.TopicUpdateBO;
import com.cskaoyan.bean.po.Topic;
import com.cskaoyan.bean.vo.TopicListVO;
import com.cskaoyan.bean.vo.TopicReadVO;
import com.cskaoyan.bean.vo.topic.WxTopicDetailVO;
import com.cskaoyan.bean.vo.topic.WxTopicListVO;

public interface TopicService {
    TopicListVO getTopicList(Integer page, Integer limit, String sort, String order, String title, String subtitle);

    Topic createTopic(TopicCreateBO topicCreateBO);

    void deleteTopic(Topic topic);

    TopicReadVO getTopicInfo(Integer id);

    Topic updateTopic(TopicUpdateBO topicUpdateBO);

    void batchDelete(TopicBachDeleteBO topicBachDeleteBO);

    WxTopicDetailVO getTopicDetail(Integer id);

    WxTopicListVO getWxTopicList(Integer page, Integer limit);

    TopicListVO getRelateTopic(Integer id);
}
