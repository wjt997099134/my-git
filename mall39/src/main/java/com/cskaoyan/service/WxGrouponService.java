package com.cskaoyan.service;

import com.cskaoyan.bean.vo.*;

import java.util.List;

public interface WxGrouponService {
    GrouponData grouponList(Integer page, Integer limit);

    GrouponDetailData grouponDetail(Integer grouponId);

    MyGrouponData myGroupon(Integer showType);

    WxGoodsDetailVo joinGroupon(Integer grouponId);
}
