package com.cskaoyan.service;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.vo.FootprintListVO;

public interface FootprintService {
    FootprintListVO footprintList(BaseParam baseParam, Integer userId);

    void delFootPrint(Integer toDelFootprintId);
}
