package com.cskaoyan.service;

import com.cskaoyan.bean.bo.BaseParam;
import com.cskaoyan.bean.po.Footprint;
import com.cskaoyan.bean.po.FootprintExample;
import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.vo.FootprintListVO;
import com.cskaoyan.mapper.FootprintMapper;
import com.cskaoyan.mapper.GoodsMapper;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author karlfu
 * @since 2022/06/08 19:54
 */

@Service
public class FootprintServiceImpl implements FootprintService {
    @Autowired
    FootprintMapper footprintMapper;

    @Autowired
    GoodsMapper goodsMapper;

    @Override
    public FootprintListVO footprintList(BaseParam param, Integer userId) {
        // limit前端给的是10
        PageHelper.startPage(param.getPage(), param.getLimit());
        FootprintExample example = new FootprintExample();
        FootprintExample.Criteria criteria = example.createCriteria();

        // 前端默认不传
        if (StringUtils.isNotEmpty(param.getSort()) && StringUtils.isNotEmpty(param.getOrder())) {
            example.setOrderByClause(param.getSort() + " " + param.getOrder());
        } else {
            example.setOrderByClause("add_time desc");
        }

        criteria.andDeletedEqualTo(false);
        criteria.andUserIdEqualTo(userId);

        List<FootprintListVO.FootprintDetail> footprintDetailList = new ArrayList<>();

        List<Footprint> footprintList = footprintMapper.selectByExample(example);

        for (Footprint footprint : footprintList) {
            FootprintListVO.FootprintDetail footprintDetail = new FootprintListVO.FootprintDetail();
            footprintDetail.setId(footprint.getId());
            footprintDetail.setAddTime(footprint.getAddTime());

            Integer goodsId = footprint.getGoodsId();
            footprintDetail.setGoodsId(goodsId);

            Goods goods = goodsMapper.selectByPrimaryKey(goodsId);
            footprintDetail.setRetailPrice(goods.getRetailPrice());
            footprintDetail.setBrief(goods.getBrief());
            footprintDetail.setPicUrl(goods.getPicUrl());
            footprintDetail.setName(goods.getName());

            footprintDetailList.add(footprintDetail);
        }


        PageInfo pageInfo = new PageInfo(footprintList);

        // total是总的数据量 → 是否等于users.length?不是 → 指的是如果不分页的情况下最多会查询出来多少条记录
        // 按照上面的查询的查询条件执行的select count(*)
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();

        return FootprintListVO.data(total, pages, param.getLimit(), param.getPage(), footprintDetailList);
    }

    @Override
    public void delFootPrint(Integer toDelFootprintId) {
        Footprint footprint = new Footprint();
        footprint.setDeleted(true);
        FootprintExample example = new FootprintExample();
        FootprintExample.Criteria criteria = example.createCriteria();
        criteria.andIdEqualTo(toDelFootprintId);
        footprintMapper.updateByExampleSelective(footprint, example);
    }
}
