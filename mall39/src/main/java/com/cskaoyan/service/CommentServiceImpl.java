package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxCommentPostBO;
import com.cskaoyan.bean.po.Comment;
import com.cskaoyan.bean.po.CommentExample;
import com.cskaoyan.bean.po.User;
import com.cskaoyan.bean.vo.CommentListVO;
import com.cskaoyan.bean.vo.WxCommentPostVO;
import com.cskaoyan.mapper.CommentMapper;
import com.cskaoyan.mapper.OrderMapper;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * @ClassName: CommentServiceImpl
 * @Description:
 * @Author: 瞳
 * @Date: 2022/6/5 22:03
 */

@Service
public class CommentServiceImpl implements CommentService {

    @Autowired
    CommentMapper commentMapper;

    @Autowired
    OrderMapper orderMapper;

    @Autowired
    ConstantProperties CONSTANT;

    @Override
    public CommentListVO getCommentList(Integer page, Integer limit, String sort, String order, Integer userId, Integer valueId) {
        CommentListVO commentListVO = new CommentListVO();
        commentListVO.setPage(page);
        commentListVO.setLimit(limit);

        // 分页插件PageHelper，辅助做分页以及分页信息的获得
        PageHelper.startPage(page, limit);
        // 执行查询过程中拼接分页信息
        CommentExample example = new CommentExample();
        CommentExample.Criteria criteria = example.createCriteria();

        // 未被删除
        criteria.andDeletedEqualTo(CONSTANT.getNotDeleted());
        // 按照更新时间降序
        example.setOrderByClause(sort + " " + order);

        // 查找用户id
        if (userId != null && !("").equals(userId + "")) {
            criteria.andUserIdEqualTo(userId);
        }

        // 查找商品id
        if (valueId != null && !("").equals(valueId + "")) {
            criteria.andValueIdEqualTo(valueId);
        }

        List<Comment> commentList = commentMapper.selectByExample(example);

        PageInfo<Comment> commentPageInfo = new PageInfo<>(commentList);
        commentListVO.setList(commentList);

        long count = commentPageInfo.getTotal();
        commentListVO.setTotal((int) count);

        commentListVO.setPages((int) Math.ceil(count * 1.0 / limit));

        return commentListVO;
    }

    @Override
    public void deleteComment(Comment comment) {
        // 根据用户id和更新时间来锁定评论，将其删除
        Integer userId = comment.getUserId();
        Date updateTime = comment.getUpdateTime();

        CommentExample example = new CommentExample();
        CommentExample.Criteria criteria = example.createCriteria();
        criteria.andUpdateTimeEqualTo(updateTime);
        criteria.andUserIdEqualTo(userId);
        Comment comment1 = comment;
        comment1.setDeleted(true);
        commentMapper.updateByExample(comment1, example);
    }

    @Override
    public WxCommentPostVO postComment(WxCommentPostBO wxCommentPostBO) {
        // 增加
        Subject subject = SecurityUtils.getSubject();
        User primaryPrincipal = (User) subject.getPrincipals().getPrimaryPrincipal();
        Integer userId = primaryPrincipal.getId();

        Comment comment = new Comment(null, wxCommentPostBO.getValueId(), wxCommentPostBO.getType(), wxCommentPostBO.getContent(), "", userId, wxCommentPostBO.getHasPicture(), wxCommentPostBO.getPicUrls(), wxCommentPostBO.getStar(), new Date(), new Date(), CONSTANT.getNotDeleted());

        commentMapper.insert(comment);

        WxCommentPostVO wxCommentPostVO = new WxCommentPostVO(comment.getId(), comment.getValueId(), comment.getType(), comment.getContent(), comment.getUserId(), comment.getHasPicture(), comment.getPicUrls(), comment.getStar(), comment.getAddTime(), comment.getUpdateTime());

        // 商品的待评价数也要减响应的数量
        // 利用userId和valueId锁定

        return wxCommentPostVO;
    }
}