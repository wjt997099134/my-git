package com.cskaoyan.service;

import com.cskaoyan.bean.po.Keyword;
import com.cskaoyan.bean.vo.SearchIndexVO;

import java.util.List;

public interface SearchService {
    List<SearchIndexVO.HistoryKeyword> getHistoryKeywordList(Integer userId);

    List<Keyword> getDefaultKeywordList();

    List<Keyword> getHotKeywordList();

    void clearHistoryByUserId(Integer userId);

    List<String> getHistoryKeywordHelperList(Integer id, String inputTry);
}
