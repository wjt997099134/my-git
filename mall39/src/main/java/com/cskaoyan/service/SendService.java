package com.cskaoyan.service;

import java.util.Map;

/**
 * @author tll
 * @create 2020/8/12 8:30
 */
public interface SendService {

    /**
     * 发送验证码方法
     * @param phone 手机号
     * @param templateCode 验证码模板
     * @param map   验证码
     * @return
     */
    boolean sendMsg(String phone, String templateCode, Map<String,Object> map);
}
