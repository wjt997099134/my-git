package com.cskaoyan.service;

import com.cskaoyan.bean.po.Category;
import com.cskaoyan.bean.vo.spread.WxCategoryData;
import com.cskaoyan.bean.vo.spread.WxCurrentCategoryData;
import com.cskaoyan.mapper.CategoryMapper;
import com.cskaoyan.util.ConstantProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author CR
 * @since 2022/06/07 16:22
 */
@Service
public class WxCategoryServiceImpl implements WxCategoryService {
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    ConstantProperties CONSTANT;

    @Override
    public WxCategoryData queryCategories() {
        WxCategoryData categoryData = new WxCategoryData();
        List<Category> categoryList = categoryMapper.queryCategoriesByLevel(CONSTANT.getCategoryFatherLevel());
        categoryData.setCategoryList(categoryList);
        Category category = categoryMapper.selectByPrimaryKey(categoryList.get(0).getId());
        categoryData.setCurrentCategory(category);
        List<Category> subCategoryList = categoryMapper.queryCategoriesByPid(category.getId());
        categoryData.setCurrentSubCategory(subCategoryList);
        return categoryData;
    }

    @Override
    public WxCurrentCategoryData queryCurrentCategories(Integer id) {
        WxCurrentCategoryData currentCategoryData = new WxCurrentCategoryData();
        Category currentCategory = categoryMapper.selectByPrimaryKey(id);
        currentCategoryData.setCurrentCategory(currentCategory);
        List<Category> currentSubCategory = categoryMapper.queryCategoriesByPid(currentCategory.getId());
        currentCategoryData.setCurrentSubCategory(currentSubCategory);
        return currentCategoryData;
    }
}
