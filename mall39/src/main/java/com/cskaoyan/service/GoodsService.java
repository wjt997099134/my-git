package com.cskaoyan.service;

import com.cskaoyan.bean.bo.GoodsCreateBO;
import com.cskaoyan.bean.bo.GoodsDetailBO;
import com.cskaoyan.bean.po.Goods;
import com.cskaoyan.bean.vo.CatAndBrandVO;
import com.cskaoyan.bean.vo.GoodsData;
import com.cskaoyan.bean.vo.GoodsDetailVO;

public interface GoodsService {

    /**
     * 按页获取商品列表
     */
    GoodsData querryAll(String page, String limit, String goodsSn, String name, String sort, String order, Integer goodsId);

    /**
     * 添加商品信息
     */
    int addGoods(GoodsCreateBO goodsCreateBO);

    /**
     * 商品详情
     */
    GoodsDetailVO selectDetail(Integer id);

    /**
     * 更新商品
     */
    void updateGoods(GoodsDetailBO goodsDetailBO);

    /**
     * 删除商品
     */
    void deleteGoods(Goods goods);

    /**
     * 显示商品品牌信息和商标列表
     */
    CatAndBrandVO catAndBrand();
}
