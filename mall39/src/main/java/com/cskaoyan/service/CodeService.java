package com.cskaoyan.service;

import com.cskaoyan.mapper.CodeMapper;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @param
 * @Date: 2022/6/9 9:55
 * @Description:验证码服务
 * @Author: 李宇浩
 * @return
 */
public interface CodeService {

    /**
     * 处理验证码
     */
    void codeHandle(String phone, String code);

    /**
     * 删除过期验证码
     */
    void removeCode(String phone);

    /**
     * 获取验证码用于用户校验
     */
    String[] getCode(String phone);
}
