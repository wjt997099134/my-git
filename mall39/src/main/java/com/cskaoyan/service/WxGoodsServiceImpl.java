package com.cskaoyan.service;

import com.cskaoyan.bean.GoodsListBaseParam;
import com.cskaoyan.bean.bo.WxBaseParam;
import com.cskaoyan.bean.po.*;
import com.cskaoyan.bean.vo.*;
import com.cskaoyan.mapper.*;
import com.cskaoyan.util.ConstantProperties;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class WxGoodsServiceImpl implements WxGoodsService {
    @Autowired
    AdMapper adMapper;
    @Autowired
    BrandMapper brandMapper;
    @Autowired
    CategoryMapper categoryMapper;
    @Autowired
    CouponMapper couponMapper;
    @Autowired
    GoodsMapper goodsMapper;
    @Autowired
    TopicMapper topicMapper;
    @Autowired
    GoodsAttributeMapper goodsAttributeMapper;
    @Autowired
    CommentMapper commentMapper;
    @Autowired
    IssueMapper issueMapper;
    @Autowired
    GoodsProductMapper goodsProductMapper;
    @Autowired
    GoodsSpecificationMapper goodsSpecificationMapper;
    @Autowired
    CollectMapper collectMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    GrouponRulesMapper grouponRulesMapper;
    @Autowired
    GrouponMapper grouponMapper;
    @Autowired
    SearchHistoryMapper searchHistoryMapper;
    @Autowired
    FootprintMapper footprintMapper;
    @Autowired
    ConstantProperties CONSTANT;

    @Override
    public WxHomeListVo homeList() {
        WxHomeListVo wxHomeListVo = new WxHomeListVo();
        //private List<WxHomeAdVo> banner;
        List<WxHomeListVo.WxHomeAdVo> banner = adMapper.queryWxHomeAd();
        wxHomeListVo.setBanner(banner);
        //private List<WxHomeBrandVo> brandList;
        List<WxHomeListVo.WxHomeBrandVo> brandList = brandMapper.queryWxHomeBrand();
        wxHomeListVo.setBrandList(brandList);
        //private List<WxHomeCategoryVo> channel;
        List<WxHomeListVo.WxHomeCategoryVo> channel = categoryMapper.queryWxHomeCategory();
        wxHomeListVo.setChannel(channel);
        //    private List<WxHomeCouponVo> couponList;
        List<WxHomeListVo.WxHomeCouponVo> couponList = couponMapper.queryWxHomeCoupon();
        wxHomeListVo.setCouponList(couponList);
        //    private List<WxHomeFloorGoodsVo> floorGoodsList;
        List<WxHomeListVo.WxHomeFloorGoodsVo> floorGoodsList = categoryMapper.queryWxHomeFloorGoods();
        for (WxHomeListVo.WxHomeFloorGoodsVo wxHomeFloorGoodsVo : floorGoodsList) {
            List<WxHomeListVo.WxHomeGoodsVo> newGoodsList = goodsMapper.queryWxHomeFloorGoodsBycategoryId(wxHomeFloorGoodsVo.getId());
            wxHomeFloorGoodsVo.setGoodsList(newGoodsList);
        }
        wxHomeListVo.setFloorGoodsList(floorGoodsList);
        //    private List<WxHomeGoodsVo> hotGoodsList;
        List<WxHomeListVo.WxHomeGoodsVo> hotGoodsList = goodsMapper.queryWxHotGoods();
        wxHomeListVo.setHotGoodsList(hotGoodsList);
        //    private List<WxHomeGoodsVo> newGoodsList;
        List<WxHomeListVo.WxHomeGoodsVo> newGoodsList = goodsMapper.queryWxNewGoods();
        wxHomeListVo.setNewGoodsList(newGoodsList);
        //    private List<WxHomeTopicVo> topicList;
        List<WxHomeListVo.WxHomeTopicVo> topicList = topicMapper.queryWxHomeTopic();
        wxHomeListVo.setTopicList(topicList);
        return wxHomeListVo;
    }

    @Override
    public WxCategoryInfoVo categoryInfo(Integer id) {
        WxCategoryInfoVo wxCategoryInfoVo = new WxCategoryInfoVo();
        Category category = categoryMapper.selectByPrimaryKey(id);
        Category parent = null;
        CategoryExample categoryExample = new CategoryExample();
        if (category.getPid() == 0) {
            categoryExample.createCriteria().andPidEqualTo(id);
            parent = category;
        } else {
            categoryExample.createCriteria().andPidEqualTo(category.getPid());
            parent = categoryMapper.selectByPrimaryKey(category.getPid());
        }
        categoryExample.setOrderByClause("update_Time desc");
        List<Category> categories = categoryMapper.selectByExample(categoryExample);
        if (categories.size() != 0) {
            if (category.getPid() == 0) {
                wxCategoryInfoVo.setCurrentCategory(categories.get(0));
            } else {
                wxCategoryInfoVo.setCurrentCategory(category);
            }
        }
        wxCategoryInfoVo.setBrotherCategory(categories);
        wxCategoryInfoVo.setParentCategory(parent);
        return wxCategoryInfoVo;
    }

    @Override
    public WxGoodsListVo goodsList(GoodsListBaseParam goodsListBaseParam) {
        PageHelper.startPage(goodsListBaseParam.getPage(), goodsListBaseParam.getLimit());
        List<WxHomeListVo.WxHomeGoodsVo> wxHomeGoodsVos = null;
        wxHomeGoodsVos = goodsMapper.queryWxHomeGoodsList(goodsListBaseParam);
       // 判断是否已经登录
        if (goodsListBaseParam.getKeyword() != null) {
            Subject subject = SecurityUtils.getSubject();
            if (subject != null) {
                if (subject.isAuthenticated()) {
                    User user = (User) subject.getPrincipals().getPrimaryPrincipal();
                    int count = searchHistoryMapper.queryHistoryByUserIdAndkeyword(user.getId(), goodsListBaseParam.getKeyword());
                    SearchHistory searchHistory = new SearchHistory();
                    searchHistory.setUserId(user.getId());
                    searchHistory.setKeyword(goodsListBaseParam.getKeyword());
                    searchHistory.setUpdateTime(new Date());
                    if (count == 0) {
                        searchHistory.setAddTime(new Date());
                        searchHistory.setDeleted(false);
                        searchHistory.setFrom(user.getNickname());
                        searchHistoryMapper.insert(searchHistory);
                    } else {
                        searchHistoryMapper.updateByDemo(searchHistory);
                    }
                }
            }
        }
        PageInfo pageInfo = new PageInfo(wxHomeGoodsVos);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        WxGoodsListVo data = WxGoodsListVo.data(((int) total), pages, goodsListBaseParam.getLimit(), goodsListBaseParam.getPage(), wxHomeGoodsVos);
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.createCriteria().andDeletedEqualTo(false);
        List<Category> categories = categoryMapper.selectByExample(categoryExample);
        data.setFilterCategoryList(categories);
        return data;
    }

    @Override
    public int countGoods() {
        GoodsExample goodsExample = new GoodsExample();
        goodsExample.createCriteria().andDeletedEqualTo(false);
        int count = (int) goodsMapper.countByExample(goodsExample);
        return count;
    }

    @Override
    public WxGoodsDetailVo goodsDetail(Integer id) {
        Goods goods = goodsMapper.selectByPrimaryKey(id);
        WxGoodsDetailVo wxGoodsDetailVo = new WxGoodsDetailVo();
        // List<GoodsAttribute> attribute;
        List<GoodsAttribute> attribute = goodsAttributeMapper.queryGoodsAttribute(id);
        wxGoodsDetailVo.setAttribute(attribute);
        //    Brand brand;
        Brand brand = brandMapper.selectByPrimaryKey(goods.getBrandId());
        wxGoodsDetailVo.setBrand(brand);
        List<Comment> comments = commentMapper.queryCommentInfoByGoodsId(id);
        ArrayList<WxCommentVo> wxCommentVos = new ArrayList<>();
        WxGoodsDetailVo.GoodsCommentBean goodsCommentBean = new WxGoodsDetailVo.GoodsCommentBean();
        goodsCommentBean.setCount(comments.size());
        for (Comment comment : comments) {
            User user = userMapper.selectByPrimaryKey(comment.getUserId());
            WxCommentVo wxCommentVo = new WxCommentVo(comment.getId(), comment.getContent(), comment.getAdminContent(), comment.getPicUrls(), comment.getAddTime(), user.getAvatar(), user.getNickname());
            wxCommentVos.add(wxCommentVo);
        }
        if (wxCommentVos.size() < 5) {
            goodsCommentBean.setData(wxCommentVos);
        } else {
            List<WxCommentVo> wxCommentVos1 = wxCommentVos.subList(0, 4);
            goodsCommentBean.setData(wxCommentVos1);
        }
        wxGoodsDetailVo.setComment(goodsCommentBean);
        //    List<Groupon> groupon;
        List<WxGoodsDetailVo.GrouponDetailBean> groupon = grouponRulesMapper.queryGrouponDetailInfo(id);
        wxGoodsDetailVo.setGroupon(groupon);
        //    List<Goods> info;
        Goods info = goodsMapper.selectByPrimaryKey(id);
        wxGoodsDetailVo.setInfo(info);
        //    List<Issue> issue;
        IssueExample issueExample = new IssueExample();
        issueExample.createCriteria().andDeletedEqualTo(false);
        List<Issue> issue = issueMapper.selectByExample(issueExample);
        wxGoodsDetailVo.setIssue(issue);
        //    List<GoodsProduct> productList;
        GoodsProductExample goodsProductExample = new GoodsProductExample();
        GoodsProductExample.Criteria criteria1 = goodsProductExample.createCriteria();
        criteria1.andDeletedEqualTo(false);
        criteria1.andGoodsIdEqualTo(id);
        List<GoodsProduct> productList = goodsProductMapper.selectByExample(goodsProductExample);
        wxGoodsDetailVo.setProductList(productList);
        //    Boolean share;
        wxGoodsDetailVo.setShare(true);
        //    List<GoodsSpecificationsBean> specificationList;
        List<WxGoodsDetailVo.GoodsSpecificationsBean> specificationList = goodsSpecificationMapper.querySpecNameByGoodsId(id);
        for (WxGoodsDetailVo.GoodsSpecificationsBean goodsSpecificationsBean : specificationList) {
            List<GoodsSpecification> valueList = goodsSpecificationMapper.querySpecInfoByGoodsIdAndSpec(id, goodsSpecificationsBean.getName());
            goodsSpecificationsBean.setValueList(valueList);
        }
        wxGoodsDetailVo.setSpecificationList(specificationList);
        //    Integer userHasCollect;
        CollectExample collectExample = new CollectExample();
        CollectExample.Criteria criteria2 = collectExample.createCriteria();
        criteria2.andDeletedEqualTo(false);
        criteria2.andValueIdEqualTo(id);
        List<Collect> collects = collectMapper.selectByExample(collectExample);
        wxGoodsDetailVo.setUserHasCollect(collects.size());
        wxGoodsDetailVo.setShareUrl(goods.getShareUrl());

        Subject subject = SecurityUtils.getSubject();
        if (subject != null) {
            if (subject.getPrincipals() != null) {
                if (subject.getPrincipals().getPrimaryPrincipal() != null) {
                    User user = (User) subject.getPrincipals().getPrimaryPrincipal();
                    int count = footprintMapper.queryFootPrintByUserIdAndGoodsId(user.getId(),id);
                    Footprint footprint = new Footprint();
                    footprint.setUserId(user.getId());
                    footprint.setUpdateTime(new Date());
                    footprint.setGoodsId(id);
                    if (count == 0) {
                        footprint.setAddTime(new Date());
                        footprint.setDeleted(false);
                        footprintMapper.insert(footprint);
                    }else{
                        footprintMapper.updateByDemo(footprint);
                    }
                }
            }
        }
        return wxGoodsDetailVo;
    }

    @Override
    public WxGoodsRelatedData goodsRelated(Integer id) {
        Goods goods = goodsMapper.selectByPrimaryKey(id);
        Integer categoryId = goods.getCategoryId();
        PageHelper.startPage(CONSTANT.getPageDefault(), 6);
        List<WxHomeListVo.WxHomeGoodsVo> wxHomeGoodsVos = goodsMapper.queryWxHomeFloorGoodsBycategoryId(categoryId);
        PageInfo pageInfo = new PageInfo(wxHomeGoodsVos);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        WxGoodsRelatedData data = WxGoodsRelatedData.data(((int) total), pages, 6, CONSTANT.getPageDefault(), wxHomeGoodsVos);
        return data;
    }

    @Override
    public WxCommentData commentList(WxBaseParam wxBaseParam, Integer valueId, Integer type) {
        List<Comment> list = null;
        if (wxBaseParam.getShowType() == 0) {
            list = commentMapper.queryCommentData(valueId, type);
        } else {
            list = commentMapper.queryCommentDataNoPic(valueId, type);
        }
        ArrayList<WxCommentData.WxCommentDataBean> commentList = new ArrayList<>();
        for (Comment comment : list) {
            User user = userMapper.selectByPrimaryKey(comment.getUserId());
            WxCommentData.WxCommentDataBean.WxCommentUserBean wxCommentUserBean = new WxCommentData.WxCommentDataBean.WxCommentUserBean(user.getAvatar(), user.getNickname());
            WxCommentData.WxCommentDataBean wxCommentDataBean = new WxCommentData.WxCommentDataBean(comment.getContent(), comment.getAdminContent(), comment.getPicUrls(), comment.getAddTime(), wxCommentUserBean);
            commentList.add(wxCommentDataBean);
        }
        PageHelper.startPage(wxBaseParam.getPage(), wxBaseParam.getLimit());
        PageInfo pageInfo = new PageInfo(commentList);
        long total = pageInfo.getTotal();
        int pages = pageInfo.getPages();
        WxCommentData data = WxCommentData.data(((int) total), pages, wxBaseParam.getLimit(), wxBaseParam.getPage(), commentList);
        return data;
    }

    @Override
    public WxCommentCountData commentCount(Integer valueId, Integer type) {
        int allCount = commentMapper.queryCountComment(valueId, type, null);
        int hasPicCount = commentMapper.queryCountComment(valueId, type, true);
        WxCommentCountData wxCommentCountData = new WxCommentCountData();
        wxCommentCountData.setAllCount(allCount);
        wxCommentCountData.setHasPicCount(hasPicCount);
        return wxCommentCountData;
    }
}
