package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxCollectAddordeleteBo;
import com.cskaoyan.bean.vo.WxCollectListData;

public interface WxCollectService {
    WxCollectListData collectList(Integer type, Integer page, Integer limit);

    void addordelete(WxCollectAddordeleteBo wxCollectAddordeleteBo);
}
