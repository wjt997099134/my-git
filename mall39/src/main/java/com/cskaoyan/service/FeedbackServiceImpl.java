package com.cskaoyan.service;

import com.cskaoyan.bean.bo.WxFeedbackBO;
import com.cskaoyan.bean.po.Feedback;
import com.cskaoyan.mapper.FeedbackMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * @author karlfu
 * @since 2022/06/07 17:34
 */

@Service
public class FeedbackServiceImpl implements FeedbackService {
    @Autowired
    FeedbackMapper feedbackMapper;

    @Override
    public void feedbackSubmit(WxFeedbackBO wxFeedbackBO, Integer userId, String username) {

        Feedback feedback = new Feedback();

        feedback.setUserId(userId);
        feedback.setUsername(username);
        // 这个状态不知道是什么 看到老师网站都是1 就设置1吧
        feedback.setStatus(1);

        feedback.setMobile(wxFeedbackBO.getMobile());
        feedback.setFeedType(wxFeedbackBO.getFeedType());
        feedback.setContent(wxFeedbackBO.getContent());
        feedback.setPicUrls(wxFeedbackBO.getPicUrls());
        feedback.setHasPicture(wxFeedbackBO.getHasPicture());
        feedback.setAddTime(new Date());
        feedback.setUpdateTime(new Date());

        feedbackMapper.insertSelective(feedback);

    }
}
