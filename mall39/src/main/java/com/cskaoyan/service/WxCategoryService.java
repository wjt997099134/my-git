package com.cskaoyan.service;

import com.cskaoyan.bean.vo.spread.WxCategoryData;
import com.cskaoyan.bean.vo.spread.WxCurrentCategoryData;

public interface WxCategoryService {
    WxCategoryData queryCategories();

    WxCurrentCategoryData queryCurrentCategories(Integer id);
}
