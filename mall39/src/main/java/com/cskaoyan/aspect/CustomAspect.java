package com.cskaoyan.aspect;

import com.cskaoyan.bean.po.Log;
import com.cskaoyan.bean.vo.BaseRespVo;
import com.cskaoyan.mapper.LogMapper;
import com.cskaoyan.util.ConstantProperties;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.Map;

/**
 * 系统管理
 * 操作日志模块
 * 切面类：用于向market_log表中插入操作信息
 *
 * @author WuHuaguo
 * @since 2022/06/06 17:01
 */

@Configuration
@Aspect
public class CustomAspect {

    @Autowired
    ConstantProperties CONSTANT;

    @Autowired
    LogMapper logMapper;

    @Pointcut("execution(* com.cskaoyan.controller.AuthController.login(..))")
    public void login() {
    }

    @Pointcut("execution(* com.cskaoyan.controller.AuthController.logout())")
    public void logout() {
    }

    @Pointcut("execution(* com.cskaoyan.controller.system.SystemAdminController.create(..))")
    public void adminCreate() {
    }

    @Pointcut("execution(* com.cskaoyan.controller.system.SystemAdminController.update(..))")
    public void adminUpdate() {
    }

    @Pointcut("execution(* com.cskaoyan.controller.system.SystemAdminController.delete(..))")
    public void adminDelete() {
    }


    @AfterReturning(value = "login()", returning = "result")
    public void afterLogin(JoinPoint joinPoint, Object result) {
        // 获取发请求的ip地址
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        String ip = request.getRemoteAddr();

        // session用于存储用户登录的用户名、ip地址
        HttpSession session = request.getSession();

        // 获取参数username
        Object[] args = joinPoint.getArgs();
        Map map = (Map) args[0];

        String admin = (String) map.get("username");

        session.setAttribute("ip", ip);
        session.setAttribute("admin", admin);

        Log log = new Log();
        Date date = new Date();
        log.setAdmin(admin);
        log.setIp(ip);
        log.setType(CONSTANT.getLogLoginType());
        log.setAction("登录");

        BaseRespVo loginBaseResp = (BaseRespVo) result;
        if (loginBaseResp.getData() == null) {
            log.setStatus(false);
        } else {
            // 登录成功
            log.setStatus(true);
        }

        log.setAddTime(date);
        log.setUpdateTime(date);

        logMapper.insertSelective(log);
    }


    @After("logout()")
    public void afterLogout() {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        HttpSession session = request.getSession();

        String admin = (String) session.getAttribute("admin");
        String ip = (String) session.getAttribute("ip");

        Log log = new Log();
        Date date = new Date();
        log.setAdmin(admin);
        log.setIp(ip);
        log.setType(CONSTANT.getLogLoginType());
        log.setAction("退出");
        log.setStatus(true);
        log.setAddTime(date);
        log.setUpdateTime(date);

        logMapper.insertSelective(log);

        session.invalidate();
    }


    @AfterReturning(value = "adminCreate()", returning = "result")
    public void afterReturningAdminCreate(BaseRespVo result) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        HttpSession session = request.getSession();

        String admin = (String) session.getAttribute("admin");
        String ip = (String) session.getAttribute("ip");

        Log log = new Log();
        Date date = new Date();
        log.setAdmin(admin);
        log.setIp(ip);
        log.setType(CONSTANT.getLogLoginType());
        log.setAction("添加管理员");
        log.setAddTime(date);
        log.setUpdateTime(date);

        if (result.getErrno() != 0) {
            // 操作失败
            log.setStatus(false);
        } else {
            // 操作成功
            log.setStatus(true);
        }

        logMapper.insertSelective(log);
    }

    @AfterReturning(value = "adminUpdate()", returning = "result")
    public void afterReturningAdminUpdate(BaseRespVo result) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        HttpSession session = request.getSession();

        String admin = (String) session.getAttribute("admin");
        String ip = (String) session.getAttribute("ip");

        Log log = new Log();
        Date date = new Date();
        log.setAdmin(admin);
        log.setIp(ip);
        log.setType(CONSTANT.getLogLoginType());
        log.setAction("编辑管理员");
        log.setAddTime(date);
        log.setUpdateTime(date);

        if (result.getErrno() != 0) {
            // 操作失败
            log.setStatus(false);
        } else {
            // 操作成功
            log.setStatus(true);
        }

        logMapper.insertSelective(log);
    }

    @AfterReturning(value = "adminDelete()", returning = "result")
    public void afterReturningAdminDelete(BaseRespVo result) {
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
        HttpSession session = request.getSession();

        String admin = (String) session.getAttribute("admin");
        String ip = (String) session.getAttribute("ip");

        Log log = new Log();
        Date date = new Date();
        log.setAdmin(admin);
        log.setIp(ip);
        log.setType(CONSTANT.getLogLoginType());
        log.setAction("删除管理员");
        log.setAddTime(date);
        log.setUpdateTime(date);
        log.setStatus(true);

        logMapper.insertSelective(log);
    }
}
